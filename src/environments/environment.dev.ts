export const environment = {
  production: true,
  apiUrl: 'http://husb.dev.services.getplug.io',
  prisma: {
    endpoint: 'http://192.81.211.19/api',
    websocket: 'ws://192.81.211.19/api'
  },
  hmr: false,
  platforms: {
    appStore: '',
    playStore: 'https://play.google.com/store/apps/details?id=com.plugged.getplug',
    facebook: 'https://www.facebook.com/PLUGGEDServices',
    twitter: 'https://twitter.com/PLUGGEDServices'
  },
};
