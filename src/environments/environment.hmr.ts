export const environment = {
  production: false,
  apiUrl: 'http://hu.lvh.me:2020',
  prisma: {
    endpoint: 'http://localhost:4000',
    websocket: 'ws://localhost:4000'
  },
  hmr: true,
  platforms: {
    appStore: '',
    playStore: 'https://play.google.com/store/apps/details?id=com.plugged.getplug',
    facebook: 'https://www.facebook.com/PLUGGEDServices',
    twitter: 'https://twitter.com/PLUGGEDServices'
  },
};