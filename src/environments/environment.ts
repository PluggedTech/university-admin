// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://hu.lvh.me:2020',
  prisma: {
    endpoint: 'http://localhost:4000',
    websocket: 'ws://localhost:4000'
  },
  hmr: false,
  platforms: {
    appStore: '',
    playStore: 'https://play.google.com/store/apps/details?id=com.plugged.getplug',
    facebook: 'https://www.facebook.com/PLUGGEDServices',
    twitter: 'https://twitter.com/PLUGGEDServices',
  },
  firebaseConfig: {
    apiKey: "AIzaSyB9KCoyvJyGDrC44hD82jJp5Zg1586_Fzk",
    authDomain: "getplug-705d0.firebaseapp.com",
    databaseURL: "https://getplug-705d0.firebaseio.com",
    projectId: "getplug-705d0",
    storageBucket: "getplug-705d0.appspot.com",
    messagingSenderId: "901928295981",
    appId: "1:901928295981:web:9dd01310af5486311e62a4",
    measurementId: "G-DJQEL981F3",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
