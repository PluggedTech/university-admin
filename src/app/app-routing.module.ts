import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '@app/not-found/not-found.component';
import { AuthGuard } from '@guards/auth.guard';
import { ActivateComponent } from './activate/activate.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { EventApprovalComponent } from './event-approval/event-approval.component';
import { LegalPrivacyPolicyComponent } from './landing/legal-privacy-policy/legal-privacy-policy.component';
import { LegalTermsOfUseComponent } from './landing/legal-terms-of-use/legal-terms-of-use.component';
import { HelpComponent } from './help/help.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'welcome', pathMatch: 'full' },
  { path: 'terms', component: LegalTermsOfUseComponent },
  { path: 'privacy', component: LegalPrivacyPolicyComponent },
  {
    path: '',
    canActivate: [ AuthGuard ],
    loadChildren: () => import('@main/main.module').then(m => m.MainModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('@app/landing/landing.module').then(m => m.LandingModule)
  },
  { path: 'help', component: HelpComponent },
  { path: 'activate', component: ActivateComponent },
  { path: 'reset-password', component: ResetPasswordComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'event-approval/:id/:type', component: EventApprovalComponent  },
  { path: 'login', redirectTo: 'welcome', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes, {
    preloadingStrategy: PreloadAllModules,
    anchorScrolling: 'enabled',
    scrollPositionRestoration: 'enabled'
  }) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
