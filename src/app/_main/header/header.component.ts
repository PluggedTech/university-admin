import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.scss' ],
})
export class HeaderComponent implements OnInit {

  sidebarToggleChecked = false;
  mobileToggleOpen = false;

  /**
   * @description Emits the post-state of the sidebar-toggle switch.
   * @emits 'OPEN' || 'CLOSE'
   * */
    // tslint:disable-next-line:no-output-rename
  @Output('sidebarStateChange') change = new EventEmitter<string>();

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  logout(): void {
    this.authService.logout();
  }

  onChange(): void {
    const state = (this.sidebarToggleChecked) ? 'OPEN' : 'CLOSE';
    this.change.emit(state);
  }

  onClick(): void {
    this.mobileToggleOpen = !this.mobileToggleOpen;
  }

  setSidebarToggleOpen(): void {
    this.sidebarToggleChecked = true;
  }

  setSidebarToggleClose(): void {
    this.sidebarToggleChecked = false;
  }
}
