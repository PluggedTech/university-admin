import { Component, OnInit } from '@angular/core';
import { SIDEBAR_MENU_ITEMS } from '@main/sidebar/sidebar-items';
import { AuthService } from '@services/auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface Profile {
  id: number;
  firstName: string;
  lastName: string;
  profilePicture: string;
}

const DEFAULT_PROFILE_PICTURE = 'assets/images/user/user.png';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: [ './sidebar.component.scss' ]
})
export class SidebarComponent implements OnInit {

  menuItems = SIDEBAR_MENU_ITEMS;
  profile$: Observable<Profile>;

  constructor(
    private authService: AuthService
  ) {
    this.profile$ = this.authService.state$
      .pipe(
        map(({ user }) => {
          if (!user) { return; }

          const { user: { firstName, lastName, id } } = user;

          let { profilePicture } = user;
          if ((profilePicture || '').trim() === '') {
            profilePicture = DEFAULT_PROFILE_PICTURE;
          }

          return <Profile>{
            id,
            firstName,
            lastName,
            profilePicture
          };
        })
      );
  }

  ngOnInit(): void {}
}
