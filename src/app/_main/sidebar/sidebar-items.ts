export interface SidebarMenu {
  path?: string;
  title?: string;
  icon?: string;
  type?: string;
  headTitle?: string;
  badgeType?: string;
  badgeValue?: string;
  children?: SidebarMenu[];
}

export const SIDEBAR_MENU_ITEMS: SidebarMenu[] = [
  { headTitle: 'General' },
  { path: '/e/dashboard', title: 'Dashboard', icon: 'icon-desktop', type: 'link' },
  { path: '/e/contacts', title: 'Contacts', icon: 'icon-agenda', type: 'link' },
  { headTitle: 'OPPORTUNITIES' },
  { path: '/e/jobs', title: 'Jobs', icon: 'icon-briefcase', type: 'link' },
  { path: '/e/events', title: 'Events', icon: 'icon-calendar', type: 'link' },
  { path: '/e/interviews', title: 'Interviews', icon: 'icon-comments', type: 'link' },
  { headTitle: 'ACCOUNT' },
  { path: '/e/account/settings', title: 'Settings', icon: 'icon-settings', type: 'link' },
];
