import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HeaderComponent } from '@main/header/header.component';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ChatSidebarComponent } from './chat-sidebar/chat-sidebar.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    SidebarComponent,
    ChatSidebarComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    // Others
    FontAwesomeModule,
    FormsModule
  ]
})
export class MainModule {}
