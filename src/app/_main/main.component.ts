import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { HeaderComponent } from '@main/header/header.component';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { MessageNotificationService } from '@app/_services/message-notification.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: [ './main.component.scss' ]
})
export class MainComponent implements OnInit, AfterViewInit {

  @ViewChild(HeaderComponent) header: HeaderComponent;
  @ViewChild('pageBodyWrapper', { read: ElementRef }) bodyWrapper: ElementRef;
  @ViewChild('pageSidebar', { read: ElementRef }) sidebar: ElementRef;

  unreadMessages : number = 0;

  constructor(
    private renderer: Renderer2,
    private messageNotification : MessageNotificationService
  ) { }

  ngOnInit(): void {
    // (1) Tablets/Phones: on load, close the sidebar by default
    // (2) All: close or open the sidebar, by default, if there is room for it
    fromEvent(window, 'resize')
      .pipe(debounceTime(500))
      .subscribe(() => (window.innerWidth <= 991) ? this.closeSidebar(true) : this.openSidebar(true));

    // initial call to ensure default conditions are met
    window.dispatchEvent(new Event('resize'));

    this.messageNotification._unreadMessages
    .subscribe(res => {
      this.unreadMessages = res.total;
    }, err => {
      console.log(err)
    })
  }

  ngAfterViewInit(): void {
  }

  /**
   * Binds to {@link HeaderComponent.change} event to open or close the page-sidebar accordingly.
   * */
  onSidebarToggle(state) {
    if (state === 'OPEN') {
      this.openSidebar();
    } else if (state === 'CLOSE') {
      this.closeSidebar();
    }
  }

  /**
   * Cause the page-sidebar to close.
   *
   * @param changeSwitch If `true`, also sets sidebar-toggle switch (see {@link header}) to its closed state.
   * */
  private closeSidebar(changeSwitch = false) {
    if (changeSwitch) { this.header.setSidebarToggleClose(); }
    this.renderer.removeClass(this.sidebar.nativeElement, 'page-sidebar-open');
    this.renderer.addClass(this.bodyWrapper.nativeElement, 'sidebar-close');
  }

  /**
   * Cause the page-sidebar to open.
   *
   * @param changeSwitch If `true`, also sets sidebar-toggle switch (see {@link header}) to its open state.
   * */
  private openSidebar(changeSwitch = false) {
    if (changeSwitch) { this.header.setSidebarToggleOpen(); }
    this.renderer.addClass(this.sidebar.nativeElement, 'page-sidebar-open');
    this.renderer.removeClass(this.bodyWrapper.nativeElement, 'sidebar-close');
  }
}
