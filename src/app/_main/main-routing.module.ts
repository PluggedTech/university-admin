import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from '@main/main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', redirectTo: 'e/dashboard', pathMatch: 'full' },
      { path: 'e', loadChildren: () => import('@dashboards/employer/employer.module').then(m => m.EmployerModule) },
      { path: 'company', loadChildren: () => import('@company/company.module').then(m => m.CompanyModule) }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class MainRoutingModule {}
