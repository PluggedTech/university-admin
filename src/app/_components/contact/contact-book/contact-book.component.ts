import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Contact, ContactService as ContactAPI, CompanyService } from '@api/generated';
import { ContactHelper } from '@components/contact/contact-helper';
import { ContactService } from '@store/services/entities/contact.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { switchMap, take, takeUntil, tap, map } from 'rxjs/operators';

enum State {
  'CREATE' = '[Contact] Create Contact',
  'READ' = '[Contact] Read Contact',
  'UPDATE' = '[Contact] Update Contact',
}

@Component({
  selector: 'app-contact-book',
  templateUrl: './contact-book.component.html',
  styleUrls: [ './contact-book.component.scss' ]
})
export class ContactBookComponent implements OnInit, OnDestroy {

  formGroup: FormGroup;
  contacts$: Observable<Contact[]>;

  fullName = ContactHelper.fullName;
  state = State;

  destroy$ = new Subject<void>();
  disabled$ = new BehaviorSubject<boolean>(true);
  selectedContact$ = new Subject<Contact>();
  fullNameAbbreviation$: Observable<string>;
  currentState$ = new BehaviorSubject<State>(State.READ);
  private storeSelectedContact: Contact;

  private _contactList$ = new BehaviorSubject<any>('');
  get contactList$(): Observable<any> { return this._contactList$.asObservable(); }

  constructor(
    private fb: FormBuilder,
    private contactService: ContactService,
    private contactAPI : ContactAPI,
    private companyService : CompanyService
  ) {
    this.formGroup = fb.group({
      firstName: '',
      lastName: '',
      title: '',
      phoneNumber: '',
      email: '',
      companyId: ''
    });

    const auth = JSON.parse(localStorage.getItem('auth'))
    this.contacts$ = this.contactAPI.apiContactQueryPost({ companyId : Number(auth.user.company.id) })
    .pipe(
      map(contacts => {
        return contacts.contactList.map(contact => {
          return contact
        }) 
      })
    )

    //this.contacts$ = this.contactService.entities$;
  }

  // get currentState() { return this._currentState$.asObservable(); }

  get fc() { return this.formGroup.controls; }

  ngOnInit(): void {
    //this.contactService.getAll();
    const auth = JSON.parse(localStorage.getItem('auth'))
    this.contacts$ = this.contactAPI.apiContactQueryPost({ companyId : Number(auth.user.company.id) })
    .pipe(
      map(contacts => {
        return contacts.contactList.map(contact => {
          return contact
        }) 
      })
    )

    // used to store the key of the last known valid field
    let freezeKey, freezeModifiedKey;
    this.formGroup.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        switchMap(object => {
          return this.currentState$.pipe(
            tap(state => {
              /*
              * Ensure that the the `formGroup` is valid for user submission.
              *
              * Case: (currentState === State.CREATE):
              * Require at least on form field to contain a string value with
              * a length greater than zero.
              *
              * Case: (currentState === State.UPDATE):
              * Require at least on form field be modified.
              * */
              if (state !== State.CREATE && state !== State.UPDATE) { return; }

              // short-check: skip full-check if field is still valid
              if (state === State.UPDATE && freezeModifiedKey) {
                const value = (object[ freezeModifiedKey ] || '').trim();
                const comparator = this.storeSelectedContact[ freezeModifiedKey ].trim();
                if (state === State.UPDATE && value !== comparator && value.length > 0) { return; }
                freezeModifiedKey = null;
              }

              if (state === State.CREATE && freezeKey) {
                const value = (object[ freezeKey ] || '').trim();
                if (value.length > 0) { return; }
                freezeKey = null;
              }

              // full-check:
              let valid = false;
              let modified = false;
              for (const key in object) {
                if (object.hasOwnProperty(key)) {
                  // hop over null values -- the user has not interacted with that field
                  if (object[ key ] === null) { continue; }

                  const value: string = object[ key ].trim();

                  // cond: `state === State.CREATE`
                  if (state === State.CREATE) {
                    if (value.length > 0) {
                      valid = true;
                      freezeKey = key;
                      break;
                    }
                  }

                  // cond: `state === State.UPDATE`
                  if (state === State.UPDATE) {
                    const comparator = this.storeSelectedContact[ key ].trim();
                    if (value !== comparator) {
                      modified = true;
                      freezeModifiedKey = key;
                    }

                    if (value.length > 0) {
                      valid = true;
                      freezeKey = key;
                    }
                  }
                }
              }

              const disable = (state === State.UPDATE) ? !(valid && modified) : !valid;
              this.disabled$.next(disable);
            })
          );
        })
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onClickCreateContact() {
    this.formGroup.reset();
    this.currentState$.next(State.CREATE);
  }

  onClickReadContact(contact: Contact) {
    this.storeSelectedContact = contact;
    this.selectedContact$.next({ ...contact });
    this.currentState$.next(State.READ);
  }

  onClickUpdateContact(contact: Contact) {
    this.formGroup.patchValue(contact);
    this.currentState$.next(State.UPDATE);
  }

  onClickDeleteContact(contact: Contact) {
    this.contactService
      .delete(contact)
      .pipe(take(1))
      .subscribe(() => this.clearSelectedContact());
  }

  onClickSubmit(currentState: State): void {
    if (currentState !== State.CREATE && currentState !== State.UPDATE) {
      throw new Error('[ContactBookComponent] Submit Error - Unknown state!');
    }

    const auth = JSON.parse(localStorage.getItem('auth'))
    this.formGroup.controls['companyId'].setValue(auth.user.company.id)

    const contact: Contact = this.formGroup.getRawValue();

    if (currentState === State.CREATE) {
      // api does not allow null values
      for (const key in contact) {
        if (contact[ key ] === null) {
          contact[ key ] = '';
        }
      }

      this.contactService
        .add(contact)
        .pipe(take(1))
        .subscribe(res => this.onClickReadContact(res));
    }

    if (currentState === State.UPDATE) {
      const entity = {
        id: this.storeSelectedContact.id,
        ...contact
      };

      this.contactService
        .update(entity)
        .pipe(take(1))
        .subscribe(res => this.onClickReadContact(res));
    }

  }

  getSubmitButtonText(state: State): string {
    if (state === State.CREATE) { return 'Create'; }
    if (state === State.UPDATE) { return 'Save'; }
  }

  private clearSelectedContact(): void {
    this.storeSelectedContact = null;
    this.selectedContact$.next(null);
  }
}
