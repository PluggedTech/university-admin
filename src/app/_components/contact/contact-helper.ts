import { Contact } from '@api/generated';

export const ContactHelper = {
  fullName: (contact: Contact): string => {
    return contact.firstName + ' ' + contact.lastName;
  }
};
