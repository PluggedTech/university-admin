import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Contact } from '@api/generated';
import { ContactDataService, ContactService } from '@store/services/entities/contact.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-contact-card',
  templateUrl: './contact-card.component.html',
  styleUrls: [ './contact-card.component.scss' ]
})
export class ContactCardComponent implements OnInit, OnDestroy {

  formGroup: FormGroup;

  destroy$ = new Subject<void>();
  disabled$ = new BehaviorSubject<boolean>(true);

  constructor(
    public dialogRef: MatDialogRef<ContactCardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private contactService: ContactService,
    private contactDataService: ContactDataService
  ) {}

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      firstName: '',
      lastName: '',
      title: '',
      email: '',
      phoneNumber: '',
      companyId : ''
    });

    let lastPassKey;
    this.formGroup.valueChanges
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(object => {
        if (lastPassKey && object[ lastPassKey ].trim().length > 0) {
          this.disabled$.next(false);
          return;
        } else {
          this.disabled$.next(true);
        }

        for (const key in object) {
          if (object.hasOwnProperty(key)) {
            if (object[ key ].trim().length > 0) {
              this.disabled$.next(false);
              lastPassKey = key;
              break;
            }

            this.disabled$.next(true);
          }
        }
      });
  }

  onSubmit(): void {
    const auth = JSON.parse(localStorage.getItem('auth'))
    this.formGroup.controls['companyId'].setValue(auth.user.company.id)

    const contact: Contact = this.formGroup.getRawValue();

    this.contactService
      .add(contact)
      .pipe(take(1))
      .subscribe(res => this.dialogRef.close(res));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
