import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ContactCardComponent } from '@components/contact/contact-card/contact-card.component';
import { ContactFieldComponent } from '@components/contact/contact-field/contact-field.component';
import { ContentModule } from '@content/content.module';
import { DirectivesModule } from '@directives/directives.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PipesModule } from '@pipes/pipes.module';
import { NgTruncatePipeModule } from 'angular-pipes';
import { ContactBookComponent } from './contact-book/contact-book.component';
import { ContactStubComponent } from './contact-stub/contact-stub.component';

@NgModule({
  declarations: [
    ContactCardComponent,
    ContactFieldComponent,
    ContactBookComponent,
    ContactStubComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    NgTruncatePipeModule,
    PipesModule,
    DirectivesModule,
    ContentModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
  ],
  exports: [
    ContactCardComponent,
    ContactFieldComponent,
    ContactBookComponent,
    ContactStubComponent,
  ]
})
export class ContactModule {}
