import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Contact } from '@api/generated';
import { ContactCardComponent } from '@components/contact/contact-card/contact-card.component';
import { ContactHelper } from '@components/contact/contact-helper';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

enum State {
  'EMPTY' = '[Contact] Empty',
  'FILLED' = '[Contact] Filled'
}

@Component({
  selector: 'app-contact-field',
  templateUrl: './contact-field.component.html',
  styleUrls: [ './contact-field.component.scss' ],
})
export class ContactFieldComponent implements OnInit {

  @Input() label: string;
  @Input() placeholder: string;
  @Input() contacts$: Observable<Contact[]>;

  @Output() selectedContact = new EventEmitter<Contact>();

  @ViewChild(MatAutocompleteTrigger) trigger: MatAutocompleteTrigger;
  @ViewChild('inputElement', { read: ElementRef }) input: ElementRef;

  filteredContacts: Observable<Contact[]>;

  fullName = ContactHelper.fullName;
  control = new FormControl();

  private storeState: State = State.EMPTY;

  constructor(
    public dialog: MatDialog
  ) {}

  private _state$ = new BehaviorSubject<State>(this.storeState);

  get state$(): Observable<State> { return this._state$.asObservable(); }

  private set disabled(value: boolean) { this.input.nativeElement.disabled = value; }

  getIcon(): IconProp {
    const state = this.storeState;
    if (state === State.EMPTY) { return <IconProp>[ 'fas', 'address-book' ]; }
    if (state === State.FILLED) { return <IconProp>[ 'fas', 'minus-square' ]; }
  }

  ngOnInit(): void {
    this.filteredContacts = this.control.valueChanges.pipe(
      startWith(''),
      switchMap(search => {
        return this.contacts$.pipe(
          map(contacts => contacts.filter(contact => {
              const firstName = contact.firstName.toLowerCase();
              const lastName = contact.lastName.toLowerCase();

              if (firstName.includes(search) || lastName.includes(search)) {
                return contact;
              }
            })
          )
        );
      })
    );
  }

  onIconClick(event: Event, state: State) {
    if (state === State.FILLED) {
      this.clear();
    }

    if (state === State.EMPTY) {
      this.createNewContact();
    }
  }

  onOptionSelected(event: MatAutocompleteSelectedEvent) {
    const contact: Contact = event.option.value;
    if (!contact) { return; }
    this.fill(contact);
  }

  createNewContact(): void {
    this.control.setValue('');

    const config: MatDialogConfig = {
      width: (window.innerWidth > 480) ? '350px' : `${window.innerWidth * .90}px`,
      data: {}
    };

    const dialogRef = this.dialog.open(ContactCardComponent, config);

    dialogRef.afterClosed()
      .subscribe(res => {
        this.fill(res);
      });
  }

  clear(): void {
    this.control.setValue('');
    this.setState(State.EMPTY);
    this.disabled = false;
  }

  fill(contact: Contact): void {
    this.control.setValue(this.fullName(contact));
    this.selectedContact.emit(contact);
    this.setState(State.FILLED);
    this.trigger.closePanel();
    this.disabled = true;
  }

  private setState(state: State): void {
    this.storeState = state;
    this._state$.next(state);
  }
}
