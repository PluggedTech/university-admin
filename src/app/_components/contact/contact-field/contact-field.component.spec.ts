import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactFieldComponent } from './contact-field.component';

describe('ContactComponent', () => {
  let component: ContactFieldComponent;
  let fixture: ComponentFixture<ContactFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
