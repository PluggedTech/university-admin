import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactStubComponent } from './contact-stub.component';

describe('ContactStubComponent', () => {
  let component: ContactStubComponent;
  let fixture: ComponentFixture<ContactStubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactStubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactStubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
