import { Component, Input, OnInit } from '@angular/core';
import { Contact } from '@api/generated';
import { ContactHelper } from '@components/contact/contact-helper';

@Component({
  selector: 'app-contact-stub',
  templateUrl: './contact-stub.component.html',
  styleUrls: [ './contact-stub.component.scss' ]
})
export class ContactStubComponent implements OnInit {

  @Input() contact: Contact;

  fullName = ContactHelper.fullName;

  constructor() { }

  ngOnInit(): void {
  }

}
