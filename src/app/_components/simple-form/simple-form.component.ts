import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { SimpleFormControls } from '@components/simple-form/simple-form-controls.abstract';

@Component({
  selector: 'app-simple-form',
  templateUrl: './simple-form.component.html',
  styleUrls: [ './simple-form.component.scss' ]
})
export class SimpleFormComponent implements SimpleFormControls, OnInit {

  @Input() headerTitle: string | null = null;
  @Input() cancelButtonText: string | null = null;
  @Input() submitButtonText: string | null = null;
  @Input() showDeleteButton = false;

  @Output() headerIconClicked = new EventEmitter<void>();
  @Output() cancelButtonClicked = new EventEmitter<void>();
  @Output() submitButtonClicked = new EventEmitter<void>();
  @Output() deleteButtonClicked = new EventEmitter<void>();

  @ViewChild('submitBtn') submitBtn;
  @ViewChild('cancelBtn') cancelBtn;
  @ViewChild('deleteBtn') deleteBtn;


  constructor() { }

  ngOnInit(): void {
  }

  onHeaderIconClick(): void { this.headerIconClicked.emit(); }

  onCancelButtonClick(): void { this.cancelButtonClicked.emit(this.cancelBtn); }

  onSubmitButtonClick(): void { this.submitButtonClicked.emit(this.submitBtn); }

  onDeleteButtonClick(): void { this.deleteButtonClicked.emit(this.deleteBtn); }

}
