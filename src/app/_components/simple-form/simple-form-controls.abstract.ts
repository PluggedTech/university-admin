/**
 * Input properties and event callbacks of SimpleFormComponent.
 * @see {@link SimpleFormComponent}
 * */
export declare interface SimpleFormControls {
  /** The text to be displayed in the panel's header. */
  headerTitle?: string;

  /**
   * The text to be displayed in the cancel button.
   * @note The cancel button will only display if this value is defined and not null.
   * */
  cancelButtonText?: string;

  /**
   * The text to be displayed in the submit button.
   * @note The submit button will only display if this value is defined and not null.
   * */
  submitButtonText?: string;

  /**
   * If `true`, the delete button will be displayed.
   * @note The delete button is an icon that is hidden by default.
   * */
  showDeleteButton?: boolean;

  /**
   * The callback function for {@link SimpleFormComponent.headerIconClicked (headerIconClicked)} event.
   *
   * @example
   * ```html
   * <app-simple-form  (headerIconClicked)="onHeaderIconClick()"></app-simple-form>
   * ```
   * */
  onHeaderIconClick?(): void;

  /**
   * The callback function for {@link SimpleFormComponent.cancelButtonClicked (cancelButtonClicked)} event.
   *
   * @example
   * ```html
   * <app-simple-form  (cancelButtonClicked)="onCancelButtonClick()"></app-simple-form>
   * ```
   * */
  onCancelButtonClick?(btn): void;

  /**
   * The callback function for {@link SimpleFormComponent.submitButtonClicked (submitButtonClicked)} event.
   *
   * @example
   * ```html
   * <app-simple-form  (submitButtonClicked)="onSubmitButtonClick()"></app-simple-form>
   * ```
   * */
  onSubmitButtonClick?(btn): void;

  /**
   * The callback function for {@link SimpleFormComponent.deleteButtonClicked (deleteButtonClicked)} event.
   *
   * @example
   * ```html
   * <app-simple-form  (deleteButtonClicked)="onDeleteButtonClick()"></app-simple-form>
   * ```
   * */
  onDeleteButtonClick?(btn): void;
}
