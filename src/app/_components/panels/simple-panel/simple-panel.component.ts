import { Component, Input, OnInit } from '@angular/core';

interface Icon {
  title?: string;
  routerLink?: string;
  faIcon: any;
}

export interface SimplePanelOptions {
  leftIcon?: Icon;
  rightIcon?: Icon;
}

@Component({
  selector: 'app-simple-panel',
  templateUrl: './simple-panel.component.html',
  styleUrls: [ './simple-panel.component.scss' ]
})
export class SimplePanelComponent implements OnInit {
  @Input() title: string;
  @Input() options: SimplePanelOptions = {};

  constructor() { }

  ngOnInit() {
  }

}
