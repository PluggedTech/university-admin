import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SimpleFormComponent } from './simple-form/simple-form.component';

@NgModule({
  declarations: [
    SimpleFormComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    SimpleFormComponent
  ]
})
export class ComponentsModule {}
