import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'content-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: [ './page-header.component.scss' ]
})

export class PageHeaderComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() breadcrumbs: string[] | boolean = [];

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    if (this.breadcrumbs === true) {
      this.breadcrumbs = this.activatedRoute.snapshot.url.join().split(',');
    }
  }

}
