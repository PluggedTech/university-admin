import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StickyPositionDirective } from '@directives/sticky-position.directive';
import { ToggleFullscreenDirective } from '@directives/toggle-fullscreen.directive';
import { MatMonthYearDatePickerDirective } from './mat-month-year-date-picker.directive';

@NgModule({
  declarations: [
    MatMonthYearDatePickerDirective,
    ToggleFullscreenDirective,
    StickyPositionDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MatMonthYearDatePickerDirective,
    ToggleFullscreenDirective,
    StickyPositionDirective,
  ]
})
export class DirectivesModule {}
