import { AfterViewInit, Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appStickyPosition]'
})
export class StickyPositionDirective implements AfterViewInit {

  @Input('appStickyPosition') offsetSelectors: string | string[];

  constructor(
    private el: ElementRef,
    private render: Renderer2
  ) {}

  ngAfterViewInit(): void {
    const selectors = (!Array.isArray(this.offsetSelectors))
      ? this.offsetSelectors
      : this.offsetSelectors.join(' ');

    const elms = document.querySelectorAll(selectors);

    let height = 15;
    elms.forEach((elm, i) => {
      height += elms.item(i).clientHeight;
    });

    const el = this.el.nativeElement;
    this.render.setStyle(el, 'position', 'sticky');
    this.render.setStyle(el, 'top', `${height}px`);
  }

}
