import { AfterViewInit, Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';

const CUSTOM_DATE_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

// Panel classname for custom styling for <mat-datepicker>
//
// See: src/assets/app.scss
// Ref: https://stackoverflow.com/questions/57261324/how-to-hide-top-button-in-mat-calendar
//
const PANEL_CLASS_NAME = 'mat-datepicker-year-month';

@Directive({
  selector: '[appMatMonthYearDatePicker]',
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS }
  ]
})
export class MatMonthYearDatePickerDirective<D> implements AfterViewInit, OnDestroy, OnInit {

  @Input('appMatMonthYearDatePicker') matDatepicker: MatDatepicker<D>;

  constructor() {}

  ngOnInit(): void {
    // Close matDatepicker when month has been selected.
    this.matDatepicker.monthSelected.subscribe(() => {
      this.matDatepicker.close();
    });
  }

  ngAfterViewInit(): void {
    // Add custom PANEL_CLASS_NAME to matDatepicker.panelClass if it does not already exist.
    const panelClass = this.matDatepicker.panelClass;
    if (typeof panelClass === 'undefined') {
      this.matDatepicker.panelClass = PANEL_CLASS_NAME;
    } else {
      const regex = new RegExp(`(${PANEL_CLASS_NAME})\b`);
      if (typeof panelClass === 'string') {
        if (panelClass.search(regex) === -1) {
          this.matDatepicker.panelClass = `${PANEL_CLASS_NAME} ${panelClass}`;
        }
      } else {
        if (panelClass.join(' ').search(regex) === -1) {
          this.matDatepicker.panelClass = [ PANEL_CLASS_NAME ].concat(panelClass);
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.matDatepicker.monthSelected.unsubscribe();
  }

}
