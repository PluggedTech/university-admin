import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaterialsModule } from '@app/ngx-materials.module';
import { LoginComponent } from '@auth/login/login.component';
import { EmployerSignupComponent } from '@auth/signup/employer/employer.signup.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    LoginComponent,
    EmployerSignupComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    NgxMaterialsModule,
    ReactiveFormsModule,
  ]
})
export class AuthModule {}
