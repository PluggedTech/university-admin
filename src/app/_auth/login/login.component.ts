import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '@api/generated';
import { ForgotPasswordComponent } from '@app/forgot-password/forgot-password.component';
import { AuthService } from '@services/auth.service';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { MatError } from '@angular/material/form-field';

@Component({
  selector: 'app-account-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ]
})
export class LoginComponent implements OnInit, OnDestroy {

  user: User = {};
  getState: Observable<any>;
  errorMessage: string | null;

  returnUrl = this.route.snapshot.queryParams[ 'returnUrl' ] || '/';
  loginForm = this.formBuilder.group({
    username: [ '', [ Validators.required ] ],
    password: [ '', [ Validators.required ] ],
  });

  private unsubscribe$ = new Subject<void>();

  @ViewChild('loginBtn') loginBtn;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<LoginComponent>
  ) {}

  get fc() { return this.loginForm.controls; }

  ngOnInit(): void {
    //this.authService.state$
    //  .pipe(
    //    takeUntil(this.unsubscribe$),
    //    tap(({ errorMessage }) => {
    //      console.log(errorMessage)
    //      if (errorMessage) {
    //        return this.snackBar.open(errorMessage, 'Error', { duration: 2000 });
    //      }
    //    })
    //  )
    //  .subscribe();
  }

  login() {
    if (this.loginForm.invalid) { return; }
    this.loginBtn.nativeElement.disabled = true;
    const { username, password } = this.loginForm.getRawValue();
    const payload = { email: username, password };
    this.authService.login(payload)
    .subscribe(res => {
      this.loginBtn.nativeElement.disabled = false;
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  openDialog(dialog: 'FORGOT' | 'SIGNUP') {
    this.dialogRef.close();

    const config: MatDialogConfig = {
      panelClass: 'mat-dialog-auth-panel',
      backdropClass: 'mat-dialog-auth-backdrop',
    };

    let dialogRef: MatDialogRef<any>;
    if (dialog === 'FORGOT') {
      dialogRef = this.dialog.open(ForgotPasswordComponent, { ...config, id: 'auth-dialog-forgot' });
    }

    if (dialogRef) {
      // store `dialogRef.id` so that we may close the dialog on successful `SIGNIN` or `SIGNUP`
      // Note: We close the dialog globally via ** src/app/_guards/auth.guard.ts **
      localStorage.setItem('authDialogID', dialogRef.id);
      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(() => localStorage.removeItem('authDialogID'));
    }
  }
}
