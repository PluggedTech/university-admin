import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Company, SocialMedia, User } from '@api/generated';
import { AuthService } from '@services/auth.service';
import { CompanyService } from '@store/services/entities/company.service';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-account-signup',
  templateUrl: './employer.signup.component.html',
  styleUrls: [ './employer.signup.component.scss' ],
})
export class EmployerSignupComponent implements OnInit, OnDestroy {
  @ViewChild('vcSignupForm', { read: ViewContainerRef, static: true }) container: ViewContainerRef;
  @ViewChild('tplEmployerForm', { read: TemplateRef, static: true }) tplEmployerForm: TemplateRef<any>;
  @ViewChild('tplCompanyForm', { read: TemplateRef, static: true }) tplCompanyForm: TemplateRef<any>;
  @ViewChild('tplUrlsForm', { read: TemplateRef, static: true }) tplUrlsForm: TemplateRef<any>;

  user: User = {};
  errorMessage: string | null;
  termsAccepted = false;

  signupForm: {
    employer: FormGroup,
    employerUrls: FormGroup,
  };

  formIndex = 0;
  formButtonText = 'Next';
  formSubmitted = false;

  companies: Observable<Company[]>;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private companyService: CompanyService,
    private dialogRef : MatDialogRef<EmployerSignupComponent>,
    private dialog : MatDialog
  ) {
    this.companies = this.companyService.entities$;
  }

  get fc() {
    return {
      ...this.signupForm.employer.controls,
      ...this.signupForm.employerUrls.controls,
    };
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit() {
    this.companyService.getAll();

    // this.store
    //   .pipe(
    //     takeUntil(this.unsubscribe$),
    //     select(selectAuthState)
    //   )
    //   .subscribe((state: AuthState) => {
    //     const { errorMessage } = state;
    //     if (errorMessage) {
    //       this.snackBar.open(errorMessage, 'Error', { duration: 2000 });
    //     }
    //   });

    this.authService.state$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe();

    const formUrls: SocialMedia = {
      websiteUrl: '',
      linkedinUrl: '',
      twitterUrl: '',
      facebookUrl: '',
    };

    this.signupForm = {
      employer: this.fb.group({
        firstName: [ '', Validators.required ],
        lastName: [ '', Validators.required ],
        jobTitle: [ '', Validators.required ],
        gender: [ '', Validators.required ],
        email: [ '', [ Validators.required, Validators.email ] ],
        phoneNumber: [ '', Validators.required ],
        password: [ '', Validators.required ],
        confirmPassword: [ '', Validators.required ],
      }),
      employerUrls: this.fb.group({ ...formUrls }),
    };

    this.signupForm.employerUrls.valueChanges.subscribe(value => {
      for (const key in value) {
        if (value[ key ].length > 0) {
          this.formButtonText = 'Next';
          return;
        } else {
          this.formButtonText = 'Skip';
        }
      }
    });

    this.next();
  }

  next() {
    if (this.formIndex === 0) {
      this.container.clear();
      this.container.createEmbeddedView(this.tplEmployerForm);
      this.formButtonText = 'Next';
      this.formIndex++;
      return;
    }

    const sf = this.signupForm;

    if (this.formIndex === 1 && sf.employer.valid) {
      this.container.clear();
      this.container.createEmbeddedView(this.tplUrlsForm, {
        formGroup: sf.employerUrls,
        formHeader: 'Employer Network'
      });
      this.formButtonText = 'Register';
      this.formIndex++;
      return;
    }

    if (this.formIndex > 1 && sf.employerUrls.valid) {
      if (!this.termsAccepted) { return; }
      this.formSubmitted = true;
      this.register();
    }
  }

  previous() {
    this.formIndex = this.formIndex - 2;
    this.next();
  }

  private register() {
    const recruiter = this.signupForm.employer.getRawValue();

    const user = {
      firstName: recruiter.firstName,
      lastName: recruiter.lastName,
      gender: recruiter.gender,
      email: recruiter.email,
      phone: recruiter.phoneNumber,
      password: recruiter.password,
      type: User.TypeEnum.RECRUITER,
      profilePic: null, // todo: to be moved to recruiterProfile
    };

    const recruiterProfile = {
      title: recruiter.jobTitle,
      socialMedia: {
        ...this.signupForm.employerUrls.getRawValue(),
      },
      user,
    };

    this.authService.registerUser(recruiterProfile, this.signupForm);
  }
}
