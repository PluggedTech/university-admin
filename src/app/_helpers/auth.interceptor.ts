import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QBHelper } from '@app/qbHelper';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private qbHelper : QBHelper) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(map(event => {
      if(event instanceof HttpResponse){
        const auth = JSON.parse(localStorage.getItem('auth'))

        if(auth.isAuthenticated)
          this.qbHelper.connectChat(auth.user.qbCredential)
      }
      return event;
    }));
  }
}
