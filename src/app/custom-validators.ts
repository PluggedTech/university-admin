import { FormControl } from '@angular/forms';

export class CustomValidators {
  static validateRequired(c: FormControl) {
    return (c.value.length === 0) ? { required: true } : null;
  }
}
