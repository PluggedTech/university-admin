import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { DEFAULT_CURRENCY_CODE, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiModule, Configuration } from '@api/generated';
import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { AuthModule } from '@auth/auth.module';
import { JwtModule } from '@auth0/angular-jwt';
import { environment } from '@environments/environment';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { ErrorInterceptor } from '@helpers/error.interceptor';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers } from '@store/app.states';
import { AuthEffects } from '@store/effects/auth.effects';
import { EntityStoreModule } from '@store/entity-store.module';
import { CustomRouteSerializer } from '@store/reducers/router/custom-route-serializer';
import { ApolloModule } from 'apollo-angular';
import { HttpLinkModule } from 'apollo-angular-link-http';
import { NotFoundComponent } from './not-found/not-found.component';
import { NgxMaterialsModule } from '@app/ngx-materials.module';
import { QBHelper } from './qbHelper';
import { ActivateComponent } from './activate/activate.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { EventApprovalComponent } from './event-approval/event-approval.component';
import { AuthInterceptor } from './_helpers/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    ActivateComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    EventApprovalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    // Others
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          const auth = JSON.parse(localStorage.getItem('auth'));
          if (auth && auth.token) {
            return auth.token;
          }
        },
        allowedDomains: [ 'hu.lvh.me:2020', 'husb.dev.services.getplug.io', 'husb.services.getplug.io', ],
      }
    }),
    ApiModule.forRoot(() => new Configuration({
      basePath: environment.apiUrl,
    })),
    ApolloModule,
    AuthModule,
    FontAwesomeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpLinkModule,
    NgxMaterialsModule,
    StoreModule.forRoot(reducers, {}),
    StoreDevtoolsModule.instrument({ logOnly: environment.production }),
    EffectsModule.forRoot([ AuthEffects ]),
    StoreRouterConnectingModule.forRoot({ serializer: CustomRouteSerializer }),
    EntityStoreModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'USD' },
    QBHelper
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far, fab);
  }
}
