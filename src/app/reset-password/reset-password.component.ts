import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  changePasswordForm : FormGroup
  formStatus  = { success: false, message : ''}
  secret = ''
  user : any;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private router : ActivatedRoute,
    private route : Router
  ) { }

  async ngOnInit() {
    this.secret = this.router.snapshot.queryParamMap.get('s') 

    this.changePasswordForm = this.fb.group({
      password: [ '', Validators.required ],
      confirmPassword: [ '', Validators.required ]
    })

    try {
     const validate = await this.http.post<any>(`${environment.apiUrl}/auth/verify-reset`, { s : this.secret }).toPromise();  
     this.user = validate;
    } catch (error) {
     this.formStatus  = { success: false, message : error}
     setTimeout(() => {this.route.navigateByUrl('/')}, 8000)
    }
  }

  async submit(){
    const data = this.changePasswordForm.value;
    if(data.password === data.confirmPassword){
      try {
       const result = await this.http.post<any>(`${environment.apiUrl}/auth/change-password`, 
       { 
         userId : this.user.userId, 
         secret : this.user.secret,
         password: data.password
       }).toPromise();  
       this.formStatus  = { success: true, message : 'Successfully Changed Password'}
      } catch (error) {
       this.formStatus  = { success: false, message : error}
      }
    }else{
      this.formStatus = { success : false, message : 'Password do not match'}
    }

    this.changePasswordForm.reset();
    setTimeout(() => {this.route.navigateByUrl('/')}, 8000)
  }

}
