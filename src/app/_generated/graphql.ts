import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
};



export type AuthPayload = {
   __typename?: 'AuthPayload';
  token: Scalars['String'];
  user: User;
};

export type Company = {
   __typename?: 'Company';
  id: Scalars['ID'];
  name: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  totalNumberOfEmployees: Scalars['Int'];
  websiteUrl?: Maybe<Scalars['String']>;
  linkedinUrl?: Maybe<Scalars['String']>;
  twitterUrl?: Maybe<Scalars['String']>;
  facebookUrl?: Maybe<Scalars['String']>;
  user: User;
  field: Field;
  startup?: Maybe<Startup>;
  employer?: Maybe<Employer>;
};


export type Employer = {
   __typename?: 'Employer';
  id: Scalars['ID'];
  phoneNumber: Scalars['String'];
  title: Scalars['String'];
  bannerImage?: Maybe<File>;
  profileImage?: Maybe<File>;
  biography?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
  linkedinUrl?: Maybe<Scalars['String']>;
  twitterUrl?: Maybe<Scalars['String']>;
  facebookUrl?: Maybe<Scalars['String']>;
  isVerified: Scalars['Boolean'];
  status?: Maybe<Scalars['String']>;
  company: Company;
};

export enum EmploymentType {
  FullTime = 'FULL_TIME',
  PartTime = 'PART_TIME',
  Temporary = 'TEMPORARY',
  Seasonal = 'SEASONAL',
  Freelance = 'FREELANCE',
  Consultant = 'CONSULTANT',
  Intern = 'INTERN'
}

export type Faq = {
   __typename?: 'Faq';
  id: Scalars['ID'];
  question: Scalars['String'];
  answer: Array<Scalars['String']>;
};

export type Field = {
   __typename?: 'Field';
  id: Scalars['ID'];
  industry: Industry;
  sector?: Maybe<Sector>;
  company: Company;
};

export type File = {
   __typename?: 'File';
  id: Scalars['ID'];
  path: Scalars['String'];
  filename: Scalars['String'];
  mimetype: Scalars['String'];
  encoding: Scalars['String'];
};

export type ICompanyCreate = {
  name: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  totalNumberOfEmployees: Scalars['Int'];
  websiteUrl?: Maybe<Scalars['String']>;
  linkedinUrl?: Maybe<Scalars['String']>;
  twitterUrl?: Maybe<Scalars['String']>;
  facebookUrl?: Maybe<Scalars['String']>;
  field: IFieldCreate;
  startup?: Maybe<IStartupCreate>;
};

export type ICompanyUpdate = {
  name?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  totalNumberOfEmployees?: Maybe<Scalars['Int']>;
  websiteUrl?: Maybe<Scalars['String']>;
  linkedinUrl?: Maybe<Scalars['String']>;
  twitterUrl?: Maybe<Scalars['String']>;
  facebookUrl?: Maybe<Scalars['String']>;
  field?: Maybe<IFieldCreate>;
  startup?: Maybe<IStartupCreate>;
};

export type IEmployerCreate = {
  phoneNumber: Scalars['String'];
  title: Scalars['String'];
  biography?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
  linkedinUrl?: Maybe<Scalars['String']>;
  twitterUrl?: Maybe<Scalars['String']>;
  facebookUrl?: Maybe<Scalars['String']>;
};

export type IEmployerSignup = {
  user: IUserCreate;
  company: ICompanyCreate;
  employer: IEmployerCreate;
};

export type IEmployerUpdate = {
  phoneNumber?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  biography?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
  linkedinUrl?: Maybe<Scalars['String']>;
  twitterUrl?: Maybe<Scalars['String']>;
  facebookUrl?: Maybe<Scalars['String']>;
};

export type IFieldCreate = {
  industryId: Scalars['String'];
  sectorId?: Maybe<Scalars['String']>;
};

export type IJobCreate = {
  description: Scalars['String'];
  employmentType: EmploymentType;
  experienceRequired: Scalars['Int'];
  industryId: Scalars['String'];
  location: Scalars['String'];
  salaryRange: Scalars['String'];
  skillsRequired: Scalars['String'];
  title: Scalars['String'];
};

export type IJobUpdate = {
  description?: Maybe<Scalars['String']>;
  employmentType?: Maybe<EmploymentType>;
  experienceRequired?: Maybe<Scalars['Int']>;
  industryId?: Maybe<Scalars['String']>;
  location?: Maybe<Scalars['String']>;
  salaryRange?: Maybe<Scalars['String']>;
  skillsRequired?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};

export type Industry = {
   __typename?: 'Industry';
  id: Scalars['ID'];
  name: Scalars['String'];
  sectors?: Maybe<Array<Maybe<Sector>>>;
};

export type IStartupCreate = {
  totalFunding?: Maybe<Scalars['Float']>;
  lastFundingDate?: Maybe<Scalars['DateTime']>;
  lastFundingRound?: Maybe<Scalars['DateTime']>;
};

export type IUserCreate = {
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
};

export type IUserUpdate = {
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};

export type Job = {
   __typename?: 'Job';
  id: Scalars['ID'];
  description: Scalars['String'];
  employmentType: EmploymentType;
  experienceRequired: Scalars['Int'];
  industry: Industry;
  location: Scalars['String'];
  salaryRange: Scalars['String'];
  skillsRequired: Scalars['String'];
  title: Scalars['String'];
  status?: Maybe<Status>;
  company: Company;
  createdAt?: Maybe<Scalars['DateTime']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type Mutation = {
   __typename?: 'Mutation';
  changePassword: Scalars['Boolean'];
  employerSignup: AuthPayload;
  login: AuthPayload;
  signup: AuthPayload;
  createCompany: Company;
  createEmployer: Employer;
  createJob: Job;
  updateCompany: Company;
  updateEmployer: Employer;
  updateJob: Job;
  updateUser: User;
  deleteJob: Job;
};


export type MutationChangePasswordArgs = {
  oldPassword: Scalars['String'];
  newPassword: Scalars['String'];
};


export type MutationEmployerSignupArgs = {
  user: IUserCreate;
  company: ICompanyCreate;
  employer: IEmployerCreate;
};


export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationSignupArgs = {
  params?: Maybe<IUserCreate>;
};


export type MutationCreateCompanyArgs = {
  params: ICompanyCreate;
};


export type MutationCreateEmployerArgs = {
  params: IEmployerCreate;
};


export type MutationCreateJobArgs = {
  params: IJobCreate;
};


export type MutationUpdateCompanyArgs = {
  params: ICompanyUpdate;
};


export type MutationUpdateEmployerArgs = {
  params: IEmployerUpdate;
};


export type MutationUpdateJobArgs = {
  id: Scalars['String'];
  params: IJobUpdate;
};


export type MutationUpdateUserArgs = {
  params: IUserUpdate;
};


export type MutationDeleteJobArgs = {
  id: Scalars['String'];
};

export enum MutationType {
  Created = 'CREATED',
  Updated = 'UPDATED',
  Deleted = 'DELETED'
}

export type NewJobPayload = {
   __typename?: 'NewJobPayload';
  mutation: MutationType;
  node?: Maybe<Job>;
  previousValues?: Maybe<Job>;
};

export type Query = {
   __typename?: 'Query';
  company: Company;
  employer: Employer;
  faqs: Array<Faq>;
  industries: Array<Industry>;
  jobs: Array<Job>;
  sectors: Array<Sector>;
  user: User;
};


export type QueryJobsArgs = {
  filter?: Maybe<Scalars['String']>;
};

export type Sector = {
   __typename?: 'Sector';
  id: Scalars['ID'];
  name: Scalars['String'];
  industry: Industry;
};

export type Startup = {
   __typename?: 'Startup';
  id?: Maybe<Scalars['ID']>;
  totalFunding: Scalars['Float'];
  lastFundingDate: Scalars['DateTime'];
  lastFundingRound: Scalars['DateTime'];
  company: Company;
};

export enum Status {
  Pending = 'PENDING',
  Approved = 'APPROVED',
  Rejected = 'REJECTED'
}

export type Subscription = {
   __typename?: 'Subscription';
  newJob: NewJobPayload;
};

export type User = {
   __typename?: 'User';
  id: Scalars['ID'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  email: Scalars['String'];
  company?: Maybe<Company>;
};

/**
 * One possible value for a given Enum. Enum values are unique values, not a
 * placeholder for a string or numeric value. However an Enum value is returned in
 * a JSON response as a string.
 */
export type __EnumValue = {
   __typename?: '__EnumValue';
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  isDeprecated: Scalars['Boolean'];
  deprecationReason?: Maybe<Scalars['String']>;
};

/**
 * Object and Interface types are described by a list of Fields, each of which has
 * a name, potentially a list of arguments, and a return type.
 */
export type __Field = {
   __typename?: '__Field';
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  args: Array<__InputValue>;
  type: __Type;
  isDeprecated: Scalars['Boolean'];
  deprecationReason?: Maybe<Scalars['String']>;
};

/**
 * Arguments provided to Fields or Directives and the input fields of an
 * InputObject are represented as Input Values which describe their type and
 * optionally a default value.
 */
export type __InputValue = {
   __typename?: '__InputValue';
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  type: __Type;
  /** A GraphQL-formatted string representing the default value for this input value. */
  defaultValue?: Maybe<Scalars['String']>;
};

/**
 * The fundamental unit of any GraphQL Schema is the type. There are many kinds of
 * types in GraphQL as represented by the `__TypeKind` enum.
 * 
 * Depending on the kind of a type, certain fields describe information about that
 * type. Scalar types provide no information beyond a name and description, while
 * Enum types provide their values. Object and Interface types provide the fields
 * they describe. Abstract types, Union and Interface, provide the Object types
 * possible at runtime. List and NonNull types compose other types.
 */
export type __Type = {
   __typename?: '__Type';
  kind: __TypeKind;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  fields?: Maybe<Array<__Field>>;
  interfaces?: Maybe<Array<__Type>>;
  possibleTypes?: Maybe<Array<__Type>>;
  enumValues?: Maybe<Array<__EnumValue>>;
  inputFields?: Maybe<Array<__InputValue>>;
  ofType?: Maybe<__Type>;
};


/**
 * The fundamental unit of any GraphQL Schema is the type. There are many kinds of
 * types in GraphQL as represented by the `__TypeKind` enum.
 * 
 * Depending on the kind of a type, certain fields describe information about that
 * type. Scalar types provide no information beyond a name and description, while
 * Enum types provide their values. Object and Interface types provide the fields
 * they describe. Abstract types, Union and Interface, provide the Object types
 * possible at runtime. List and NonNull types compose other types.
 */
export type __TypeFieldsArgs = {
  includeDeprecated?: Maybe<Scalars['Boolean']>;
};


/**
 * The fundamental unit of any GraphQL Schema is the type. There are many kinds of
 * types in GraphQL as represented by the `__TypeKind` enum.
 * 
 * Depending on the kind of a type, certain fields describe information about that
 * type. Scalar types provide no information beyond a name and description, while
 * Enum types provide their values. Object and Interface types provide the fields
 * they describe. Abstract types, Union and Interface, provide the Object types
 * possible at runtime. List and NonNull types compose other types.
 */
export type __TypeEnumValuesArgs = {
  includeDeprecated?: Maybe<Scalars['Boolean']>;
};

/** An enum describing what kind of type a given `__Type` is. */
export enum __TypeKind {
  /** Indicates this type is a scalar. */
  Scalar = 'SCALAR',
  /** Indicates this type is an object. `fields` and `interfaces` are valid fields. */
  Object = 'OBJECT',
  /** Indicates this type is an interface. `fields` and `possibleTypes` are valid fields. */
  Interface = 'INTERFACE',
  /** Indicates this type is a union. `possibleTypes` is a valid field. */
  Union = 'UNION',
  /** Indicates this type is an enum. `enumValues` is a valid field. */
  Enum = 'ENUM',
  /** Indicates this type is an input object. `inputFields` is a valid field. */
  InputObject = 'INPUT_OBJECT',
  /** Indicates this type is a list. `ofType` is a valid field. */
  List = 'LIST',
  /** Indicates this type is a non-null. `ofType` is a valid field. */
  NonNull = 'NON_NULL'
}

export type EmployerSignupMutationVariables = {
  user: IUserCreate;
  company: ICompanyCreate;
  employer: IEmployerCreate;
};


export type EmployerSignupMutation = (
  { __typename?: 'Mutation' }
  & { employerSignup: (
    { __typename?: 'AuthPayload' }
    & Pick<AuthPayload, 'token'>
    & { user: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'firstName' | 'lastName' | 'email'>
    ) }
  ) }
);

export type LoginMutationVariables = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login: (
    { __typename?: 'AuthPayload' }
    & Pick<AuthPayload, 'token'>
    & { user: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'firstName' | 'lastName' | 'email'>
    ) }
  ) }
);

export type ChangePasswordMutationVariables = {
  oldPassword: Scalars['String'];
  newPassword: Scalars['String'];
};


export type ChangePasswordMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'changePassword'>
);

export type UpdateCompanyMutationVariables = {
  params: ICompanyUpdate;
};


export type UpdateCompanyMutation = (
  { __typename?: 'Mutation' }
  & { updateCompany: (
    { __typename?: 'Company' }
    & Pick<Company, 'id' | 'name' | 'email' | 'phoneNumber' | 'description' | 'totalNumberOfEmployees' | 'websiteUrl' | 'linkedinUrl' | 'twitterUrl' | 'facebookUrl'>
    & { field: (
      { __typename?: 'Field' }
      & { industry: (
        { __typename?: 'Industry' }
        & Pick<Industry, 'id'>
      ), sector?: Maybe<(
        { __typename?: 'Sector' }
        & Pick<Sector, 'id'>
      )> }
    ), startup?: Maybe<(
      { __typename?: 'Startup' }
      & Pick<Startup, 'id' | 'totalFunding' | 'lastFundingDate' | 'lastFundingRound'>
    )>, employer?: Maybe<(
      { __typename?: 'Employer' }
      & Pick<Employer, 'id' | 'phoneNumber' | 'title' | 'biography' | 'websiteUrl' | 'linkedinUrl' | 'twitterUrl' | 'facebookUrl' | 'isVerified'>
    )> }
  ) }
);

export type UpdateEmployerMutationVariables = {
  params: IEmployerUpdate;
};


export type UpdateEmployerMutation = (
  { __typename?: 'Mutation' }
  & { updateEmployer: (
    { __typename?: 'Employer' }
    & Pick<Employer, 'id' | 'phoneNumber' | 'title' | 'biography' | 'websiteUrl' | 'linkedinUrl' | 'twitterUrl' | 'facebookUrl' | 'isVerified' | 'status'>
    & { bannerImage?: Maybe<(
      { __typename?: 'File' }
      & Pick<File, 'id'>
    )>, profileImage?: Maybe<(
      { __typename?: 'File' }
      & Pick<File, 'id'>
    )> }
  ) }
);

export type UpdateUserMutationVariables = {
  params: IUserUpdate;
};


export type UpdateUserMutation = (
  { __typename?: 'Mutation' }
  & { updateUser: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'firstName' | 'lastName' | 'email'>
  ) }
);

export type CreateJobMutationVariables = {
  params: IJobCreate;
};


export type CreateJobMutation = (
  { __typename?: 'Mutation' }
  & { createJob: (
    { __typename?: 'Job' }
    & Pick<Job, 'id' | 'description' | 'employmentType' | 'experienceRequired' | 'location' | 'salaryRange' | 'skillsRequired' | 'title' | 'status' | 'createdAt' | 'updatedAt'>
    & { industry: (
      { __typename?: 'Industry' }
      & Pick<Industry, 'id' | 'name'>
    ) }
  ) }
);

export type UpdateJobMutationVariables = {
  id: Scalars['String'];
  params: IJobUpdate;
};


export type UpdateJobMutation = (
  { __typename?: 'Mutation' }
  & { updateJob: (
    { __typename?: 'Job' }
    & Pick<Job, 'id' | 'description' | 'employmentType' | 'experienceRequired' | 'location' | 'salaryRange' | 'skillsRequired' | 'title' | 'status' | 'createdAt' | 'updatedAt'>
    & { industry: (
      { __typename?: 'Industry' }
      & Pick<Industry, 'id' | 'name'>
    ) }
  ) }
);

export type DeleteJobMutationVariables = {
  id: Scalars['String'];
};


export type DeleteJobMutation = (
  { __typename?: 'Mutation' }
  & { deleteJob: (
    { __typename?: 'Job' }
    & Pick<Job, 'id'>
  ) }
);

export type GetEnumValuesQueryVariables = {
  name: Scalars['String'];
};


export type GetEnumValuesQuery = (
  { __typename?: 'Query' }
  & { __type?: Maybe<(
    { __typename?: '__Type' }
    & { enumValues?: Maybe<Array<(
      { __typename?: '__EnumValue' }
      & Pick<__EnumValue, 'name'>
    )>> }
  )> }
);

export type GetFaqsQueryVariables = {};


export type GetFaqsQuery = (
  { __typename?: 'Query' }
  & { faqs: Array<(
    { __typename?: 'Faq' }
    & Pick<Faq, 'id' | 'question' | 'answer'>
  )> }
);

export type GetIndustriesQueryVariables = {};


export type GetIndustriesQuery = (
  { __typename?: 'Query' }
  & { industries: Array<(
    { __typename?: 'Industry' }
    & Pick<Industry, 'id' | 'name'>
    & { sectors?: Maybe<Array<Maybe<(
      { __typename?: 'Sector' }
      & Pick<Sector, 'id' | 'name'>
    )>>> }
  )> }
);

export type GetUserQueryVariables = {};


export type GetUserQuery = (
  { __typename?: 'Query' }
  & { user: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'firstName' | 'lastName' | 'email'>
  ) }
);

export type GetUserCompanyQueryVariables = {};


export type GetUserCompanyQuery = (
  { __typename?: 'Query' }
  & { company: (
    { __typename?: 'Company' }
    & Pick<Company, 'id' | 'name' | 'email' | 'phoneNumber' | 'description' | 'totalNumberOfEmployees' | 'websiteUrl' | 'linkedinUrl' | 'twitterUrl' | 'facebookUrl'>
    & { field: (
      { __typename?: 'Field' }
      & { industry: (
        { __typename?: 'Industry' }
        & Pick<Industry, 'id'>
      ), sector?: Maybe<(
        { __typename?: 'Sector' }
        & Pick<Sector, 'id'>
      )> }
    ), startup?: Maybe<(
      { __typename?: 'Startup' }
      & Pick<Startup, 'id' | 'totalFunding' | 'lastFundingDate' | 'lastFundingRound'>
    )>, employer?: Maybe<(
      { __typename?: 'Employer' }
      & Pick<Employer, 'id' | 'phoneNumber' | 'title' | 'biography' | 'websiteUrl' | 'linkedinUrl' | 'twitterUrl' | 'facebookUrl' | 'isVerified'>
    )> }
  ) }
);

export type GetJobPostingsQueryVariables = {
  filter?: Maybe<Scalars['String']>;
};


export type GetJobPostingsQuery = (
  { __typename?: 'Query' }
  & { jobs: Array<(
    { __typename?: 'Job' }
    & Pick<Job, 'id' | 'description' | 'employmentType' | 'experienceRequired' | 'location' | 'salaryRange' | 'skillsRequired' | 'title' | 'status' | 'createdAt' | 'updatedAt'>
    & { industry: (
      { __typename?: 'Industry' }
      & Pick<Industry, 'id' | 'name'>
    ) }
  )> }
);

export type GetNewJobPostingsSubscriptionVariables = {};


export type GetNewJobPostingsSubscription = (
  { __typename?: 'Subscription' }
  & { newJob: (
    { __typename?: 'NewJobPayload' }
    & Pick<NewJobPayload, 'mutation'>
    & { node?: Maybe<(
      { __typename?: 'Job' }
      & Pick<Job, 'id' | 'description' | 'employmentType' | 'experienceRequired' | 'location' | 'salaryRange' | 'skillsRequired' | 'title' | 'status' | 'createdAt' | 'updatedAt'>
      & { industry: (
        { __typename?: 'Industry' }
        & Pick<Industry, 'id' | 'name'>
      ) }
    )>, previousValues?: Maybe<(
      { __typename?: 'Job' }
      & Pick<Job, 'id'>
    )> }
  ) }
);

export const EmployerSignupDocument = gql`
    mutation employerSignup($user: IUserCreate!, $company: ICompanyCreate!, $employer: IEmployerCreate!) {
  employerSignup(user: $user, company: $company, employer: $employer) {
    token
    user {
      id
      firstName
      lastName
      email
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class EmployerSignupGQL extends Apollo.Mutation<EmployerSignupMutation, EmployerSignupMutationVariables> {
    document = EmployerSignupDocument;
    
  }
export const LoginDocument = gql`
    mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    token
    user {
      id
      firstName
      lastName
      email
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LoginGQL extends Apollo.Mutation<LoginMutation, LoginMutationVariables> {
    document = LoginDocument;
    
  }
export const ChangePasswordDocument = gql`
    mutation changePassword($oldPassword: String!, $newPassword: String!) {
  changePassword(oldPassword: $oldPassword, newPassword: $newPassword)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ChangePasswordGQL extends Apollo.Mutation<ChangePasswordMutation, ChangePasswordMutationVariables> {
    document = ChangePasswordDocument;
    
  }
export const UpdateCompanyDocument = gql`
    mutation updateCompany($params: ICompanyUpdate!) {
  updateCompany(params: $params) {
    id
    name
    email
    phoneNumber
    description
    totalNumberOfEmployees
    websiteUrl
    linkedinUrl
    twitterUrl
    facebookUrl
    field {
      industry {
        id
      }
      sector {
        id
      }
    }
    startup {
      id
      totalFunding
      lastFundingDate
      lastFundingRound
    }
    employer {
      id
      phoneNumber
      title
      biography
      websiteUrl
      linkedinUrl
      twitterUrl
      facebookUrl
      isVerified
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateCompanyGQL extends Apollo.Mutation<UpdateCompanyMutation, UpdateCompanyMutationVariables> {
    document = UpdateCompanyDocument;
    
  }
export const UpdateEmployerDocument = gql`
    mutation updateEmployer($params: IEmployerUpdate!) {
  updateEmployer(params: $params) {
    id
    phoneNumber
    title
    bannerImage {
      id
    }
    profileImage {
      id
    }
    biography
    websiteUrl
    linkedinUrl
    twitterUrl
    facebookUrl
    isVerified
    status
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateEmployerGQL extends Apollo.Mutation<UpdateEmployerMutation, UpdateEmployerMutationVariables> {
    document = UpdateEmployerDocument;
    
  }
export const UpdateUserDocument = gql`
    mutation updateUser($params: IUserUpdate!) {
  updateUser(params: $params) {
    id
    firstName
    lastName
    email
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateUserGQL extends Apollo.Mutation<UpdateUserMutation, UpdateUserMutationVariables> {
    document = UpdateUserDocument;
    
  }
export const CreateJobDocument = gql`
    mutation createJob($params: IJobCreate!) {
  createJob(params: $params) {
    id
    description
    employmentType
    experienceRequired
    industry {
      id
      name
    }
    location
    salaryRange
    skillsRequired
    title
    status
    createdAt
    updatedAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateJobGQL extends Apollo.Mutation<CreateJobMutation, CreateJobMutationVariables> {
    document = CreateJobDocument;
    
  }
export const UpdateJobDocument = gql`
    mutation updateJob($id: String!, $params: IJobUpdate!) {
  updateJob(id: $id, params: $params) {
    id
    description
    employmentType
    experienceRequired
    industry {
      id
      name
    }
    location
    salaryRange
    skillsRequired
    title
    status
    createdAt
    updatedAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateJobGQL extends Apollo.Mutation<UpdateJobMutation, UpdateJobMutationVariables> {
    document = UpdateJobDocument;
    
  }
export const DeleteJobDocument = gql`
    mutation deleteJob($id: String!) {
  deleteJob(id: $id) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DeleteJobGQL extends Apollo.Mutation<DeleteJobMutation, DeleteJobMutationVariables> {
    document = DeleteJobDocument;
    
  }
export const GetEnumValuesDocument = gql`
    query getEnumValues($name: String!) {
  __type(name: $name) {
    enumValues {
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetEnumValuesGQL extends Apollo.Query<GetEnumValuesQuery, GetEnumValuesQueryVariables> {
    document = GetEnumValuesDocument;
    
  }
export const GetFaqsDocument = gql`
    query getFaqs {
  faqs {
    id
    question
    answer
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetFaqsGQL extends Apollo.Query<GetFaqsQuery, GetFaqsQueryVariables> {
    document = GetFaqsDocument;
    
  }
export const GetIndustriesDocument = gql`
    query getIndustries {
  industries {
    id
    name
    sectors {
      id
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetIndustriesGQL extends Apollo.Query<GetIndustriesQuery, GetIndustriesQueryVariables> {
    document = GetIndustriesDocument;
    
  }
export const GetUserDocument = gql`
    query getUser {
  user {
    id
    firstName
    lastName
    email
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetUserGQL extends Apollo.Query<GetUserQuery, GetUserQueryVariables> {
    document = GetUserDocument;
    
  }
export const GetUserCompanyDocument = gql`
    query getUserCompany {
  company {
    id
    name
    email
    phoneNumber
    description
    totalNumberOfEmployees
    websiteUrl
    linkedinUrl
    twitterUrl
    facebookUrl
    field {
      industry {
        id
      }
      sector {
        id
      }
    }
    startup {
      id
      totalFunding
      lastFundingDate
      lastFundingRound
    }
    employer {
      id
      phoneNumber
      title
      biography
      websiteUrl
      linkedinUrl
      twitterUrl
      facebookUrl
      isVerified
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetUserCompanyGQL extends Apollo.Query<GetUserCompanyQuery, GetUserCompanyQueryVariables> {
    document = GetUserCompanyDocument;
    
  }
export const GetJobPostingsDocument = gql`
    query getJobPostings($filter: String) {
  jobs(filter: $filter) {
    id
    description
    employmentType
    experienceRequired
    industry {
      id
      name
    }
    location
    salaryRange
    skillsRequired
    title
    status
    createdAt
    updatedAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetJobPostingsGQL extends Apollo.Query<GetJobPostingsQuery, GetJobPostingsQueryVariables> {
    document = GetJobPostingsDocument;
    
  }
export const GetNewJobPostingsDocument = gql`
    subscription getNewJobPostings {
  newJob {
    mutation
    node {
      id
      description
      employmentType
      experienceRequired
      industry {
        id
        name
      }
      location
      salaryRange
      skillsRequired
      title
      status
      createdAt
      updatedAt
    }
    previousValues {
      id
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetNewJobPostingsGQL extends Apollo.Subscription<GetNewJobPostingsSubscription, GetNewJobPostingsSubscriptionVariables> {
    document = GetNewJobPostingsDocument;
    
  }