import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AboutComponent } from '@company/about/about.component';
import { CompanyRoutingModule } from '@company/company-routing.module';
import { LegalPrivacyPolicyComponent } from '@company/legal-privacy-policy/legal-privacy-policy.component';
import { LegalTermsOfUseComponent } from '@company/legal-terms-of-use/legal-terms-of-use.component';
import { SupportFaqComponent } from '@company/support-faq/support-faq.component';
import { ContentModule } from '@content/content.module';

@NgModule({
  declarations: [
    AboutComponent,
    SupportFaqComponent,
    LegalTermsOfUseComponent,
    LegalPrivacyPolicyComponent
  ],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    ContentModule
  ]
})
export class CompanyModule {}
