import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { LegalPrivacyPolicyComponent } from './legal-privacy-policy/legal-privacy-policy.component';
import { LegalTermsOfUseComponent } from './legal-terms-of-use/legal-terms-of-use.component';
import { SupportFaqComponent } from './support-faq/support-faq.component';

const routes: Routes = [
  { path: '', component: AboutComponent },
  { path: 'support/faq', component: SupportFaqComponent },
  { path: 'legal/terms-of-use', component: LegalTermsOfUseComponent },
  { path: 'legal/privacy-policy', component: LegalPrivacyPolicyComponent },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class CompanyRoutingModule {}
