export interface Faq {
  question: string;
  answer: string;
}

/* tslint:disable:max-line-length */
export const FAQS: Faq[] = [
  {
    question: 'What is Plugged?',
    answer: 'PLUGGED is the most simple solution with a complex algorithm created to solve the diversity issue in Corporate America.'
  },
  {
    question: 'How much does Plugged cost for businesses? ',
    answer: 'We have several different job subscriptions to purchase from after your 7 day free trial starting at $100/mo. We also offer yearly subscriptions as well. If you are looking to post more jobs than what we offer in our basic packages please contact sales@GETPLUG.com to get a subscription that is right for your business. If you\'re looking to open multiple accounts and would like to partner to receive discounted pricing, please contact support@getplug.com .'
  },
  {
    question: 'How can I upgrade or downgrade my subscription?',
    answer: 'To upgrade or downgrade your subscription, select the "Account" tab then select "SUBSCRIPTION" In the sub-category. You can choose between the following packages: 3-Jobs - $100/month 5-Jobs - $150/month 10-jobs - $300/month.'
  },
  {
    question: 'How does Plugged recommend candidates?',
    answer: 'The beauty really happens behind the scenes, where PLUGGED powerful algorithm \'IKENNA\' analyzes users\' behavior and look-alike matching, for super accurate results - but that\'s the complicated stuff. The easiest answer is that we match your job description and keywords with the candidate\'s years of experience, background, skills and location.'
  },
  {
    question: 'Why is creating a profile important? What do you do with the information?',
    answer: 'PLUGGED wants to make recruiting more human -- and put names and faces behind the companies who are hiring. Ensure you include your company’s email address, first and last name, headline (or title), location and job function when creating your account. We make sure that your profile won\'t be accessible to the candidate until there is a match and you give us the permission.'
  },
  {
    question: 'What happens when I match with a candidate?',
    answer: 'You get instantly connected! A push notification followed by an email introduction is triggered after the match, notifying you and the candidate. At this point, you can view additional information about the candidate and get in touch with him or her directly - via email or chat. Just the two of you talking, no one else.'
  },
  {
    question: 'Do you have candidates from all over the U.S.?',
    answer: 'Technically, YES! Most of our talents are based in the U.S. but with very diverse backgrounds. Our job seekers are concentrated in major urban areas like Washington DC, New York, San Francisco and the Bay Area, Los Angeles, Boston and Chicago, Atlanta, Dallas, Seattle, and so on. But have candidates all over the U.S.!'
  },
];
