import { Component, OnInit } from '@angular/core';
import { Faq, FaqService } from '@api/generated';
import { BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-support-faq',
  templateUrl: './support-faq.component.html',
  styleUrls: [ './support-faq.component.scss' ]
})
export class SupportFaqComponent implements OnInit {

  dataStore: { faqs: Array<Faq> } = { faqs: [] };
  _faqs = <BehaviorSubject<Array<Faq>>>new BehaviorSubject([]);

  get faqs() { return this._faqs.asObservable(); }

  constructor(
    private api: FaqService
  ) {}

  ngOnInit() {
    this.api.apiFaqGet()
      .pipe(take(1))
      .subscribe(
        data => {
          this.dataStore.faqs = data.faqList;
          this._faqs.next({ ...this.dataStore }.faqs);
        },
        err => console.error('[SupportFaqComponent] ERROR - ', err)
      );
  }

}
