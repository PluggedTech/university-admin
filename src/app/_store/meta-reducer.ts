import { ActionReducer, MetaReducer } from '@ngrx/store';
import { LocalStorageConfig, localStorageSync } from 'ngrx-store-localstorage';

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  const config: LocalStorageConfig = {
    keys: [ 'auth' ],
    rehydrate: true
  };
  return localStorageSync(config)(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [ localStorageSyncReducer ];
