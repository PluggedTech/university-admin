import { Injectable } from '@angular/core';
import { AuthService } from '@api/generated';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  AuthActionTypes,
  Login,
  LoginFailure,
  LoginSuccess,
  LoginVerificationFailure,
  LoginVerificationSuccess,
  LogoutFailure,
  LogoutSuccess,
  Signup,
  SignupFailure,
  SignupSuccess,
  Update,
  UpdateFailure,
  UpdateSuccess,
} from '@store/actions/auth.actions';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class AuthEffects {
  @Effect()
  Login: Observable<any> = this.actions
    .pipe(
      ofType(AuthActionTypes.LOGIN),
      map((action: Login) => action.payload),
      switchMap(payload => {
        return this.authService
          .authLoginPost({ email: payload.email, password: payload.password })
          .pipe(
            map(({ user }) => new LoginSuccess(user)),
            catchError(error => of(new LoginFailure({ error })))
          );
      })
    );

  @Effect()
  LoginVerification: Observable<any> = this.actions
    .pipe(
      ofType(AuthActionTypes.LOGIN_VERIFICATION),
      switchMap(() => {
        return this.authService
          .authVerifyLoginGet()
          .pipe(
            map(({ user }) => new LoginVerificationSuccess(user)),
            catchError(error => of(new LoginVerificationFailure({ error })))
          );
      })
    );

  @Effect()
  Signup: Observable<any> = this.actions
    .pipe(
      ofType(AuthActionTypes.SIGNUP),
      map((action: Signup) => action.payload),
      switchMap(payload => {
        return this.authService
          .authRegisterPost({ ...payload })
          .pipe(
            map(({ user }) => new SignupSuccess(user)),
            catchError(error => of(new SignupFailure({ error })))
          );
      })
    );

  @Effect()
  Update: Observable<any> = this.actions
    .pipe(
      ofType(AuthActionTypes.UPDATE),
      map((action: Update) => action.payload),
      switchMap(payload => {
        return this.authService
          .authUpdatePost({ ...payload })
          .pipe(
            map(() => new UpdateSuccess(payload)),
            catchError(error => of(new UpdateFailure({ error })))
          );
      })
    );

  @Effect()
  Logout: Observable<any> = this.actions
    .pipe(
      ofType(AuthActionTypes.LOGOUT),
      switchMap(() => {
        return this.authService
          .authLogoutPost()
          .pipe(
            map(session => new LogoutSuccess()),
            catchError(error => of(new LogoutFailure({ error })))
          );
      })
    );

  @Effect({ dispatch: false })
  AuthSuccess: Observable<any> = this.actions
    .pipe(
      ofType(
        AuthActionTypes.LOGIN_SUCCESS,
        AuthActionTypes.LOGIN_VERIFICATION_SUCCESS,
        AuthActionTypes.SIGNUP_SUCCESS
      )
    );

  @Effect({ dispatch: false })
  AuthFailure: Observable<any> = this.actions
    .pipe(
      ofType(
        AuthActionTypes.LOGIN_FAILURE,
        AuthActionTypes.LOGIN_VERIFICATION_FAILURE,
        AuthActionTypes.SIGNUP_FAILURE,
        AuthActionTypes.LOGOUT_FAILURE
      )
    );

  @Effect({ dispatch: false })
  LogoutSuccess: Observable<any> = this.actions
    .pipe(
      ofType(AuthActionTypes.LOGOUT_SUCCESS)
    );

  @Effect({ dispatch: false })
  UpdateSuccess: Observable<any> = this.actions
    .pipe(
      ofType(AuthActionTypes.UPDATE_SUCCESS),
    );

  @Effect({ dispatch: false })
  UpdateFailure: Observable<any> = this.actions
    .pipe(
      ofType(AuthActionTypes.UPDATE_FAILURE),
    );

  constructor(
    private actions: Actions,
    private authService: AuthService,
  ) {}
}
