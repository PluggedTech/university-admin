import * as fromRouter from '@ngrx/router-store';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import { RouterStateUrl } from './reducers/router/custom-route-serializer';

export interface AppState {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<AppState> = {
  router: fromRouter.routerReducer
};

export const selectRouterState = createFeatureSelector<fromRouter.RouterReducerState<RouterStateUrl>>('router');
