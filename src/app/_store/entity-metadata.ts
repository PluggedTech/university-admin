import { environment } from '@environments/environment';
import { DefaultDataServiceConfig, EntityMetadataMap } from '@ngrx/data';

const API_URL = environment.apiUrl;

const entityMetadata: EntityMetadataMap = {
  CampusPartner: {},
  Classification: {},
  Company: {},
  Contact: {},
  Event: {},
  Industry: {},
  Job: {},
  Major: {},
  Match: {},
  PermittedEventType: {},
  School: {},
  Student: {},
  University: {},
};

/**
 * The entity configuration object.
 *
 * > *Note:* At the time of writing this, the current backend api does not support a pluralized naming convention.
 * The default ngrx pluralization behavior is detracted at the HttpUrlGenerator service level with
 * a custom CamelcaseHttpUrlGeneratorService.
 *
 * @see {@link HttpUrlGenerator}
 * @see {@link @store/services/camelcase-http-url-generator.service.ts CamelcaseHttpUrlGeneratorService}
 * */
export const entityConfig = { entityMetadata };

export const defaultDataServiceConfig: DefaultDataServiceConfig = { root: API_URL + '/api', timeout: 60000 };
