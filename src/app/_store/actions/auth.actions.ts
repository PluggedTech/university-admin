import { Action } from '@ngrx/store';

export enum AuthActionTypes {
  LOGIN = '[AUTH] Login',
  LOGIN_SUCCESS = '[AUTH] Login Success',
  LOGIN_FAILURE = '[AUTH] Login Failure',
  LOGIN_VERIFICATION = '[AUTH] Login Verification',
  LOGIN_VERIFICATION_SUCCESS = '[AUTH] Login Verification Success',
  LOGIN_VERIFICATION_FAILURE = '[AUTH] Login Verification Failure',
  SIGNUP = '[AUTH] Signup',
  SIGNUP_SUCCESS = '[AUTH] Signup Success',
  SIGNUP_FAILURE = '[AUTH] Signup Failure',
  LOGOUT = '[AUTH] Logout',
  LOGOUT_SUCCESS = '[AUTH] Logout Success',
  LOGOUT_FAILURE = '[AUTH] Logout Failure',
  UPDATE = '[AUTH] Update',
  UPDATE_SUCCESS = '[AUTH] Update Success',
  UPDATE_FAILURE = '[AUTH] Update Failure',
}

export class Login implements Action {
  readonly type = AuthActionTypes.LOGIN;

  constructor(public payload: any) {}
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;

  constructor(public payload: any) {}
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILURE;

  constructor(public payload: any) {}
}

export class LoginVerification implements Action {
  readonly type = AuthActionTypes.LOGIN_VERIFICATION;

  constructor() {}
}

export class LoginVerificationSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_VERIFICATION_SUCCESS;

  constructor(public payload: any) {}
}

export class LoginVerificationFailure implements Action {
  readonly type = AuthActionTypes.LOGIN_VERIFICATION_FAILURE;

  constructor(public payload: any) {}
}

export class Signup implements Action {
  readonly type = AuthActionTypes.SIGNUP;

  constructor(public payload: any) {}
}

export class SignupSuccess implements Action {
  readonly type = AuthActionTypes.SIGNUP_SUCCESS;

  constructor(public payload: any) {
  }
}

export class SignupFailure implements Action {
  readonly type = AuthActionTypes.SIGNUP_FAILURE;

  constructor(public payload: any) {}
}

export class Logout implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export class LogoutSuccess implements Action {
  readonly type = AuthActionTypes.LOGOUT_SUCCESS;
}

export class LogoutFailure implements Action {
  readonly type = AuthActionTypes.LOGOUT_FAILURE;

  constructor(public payload: any) {}
}

export class Update implements Action {
  readonly type = AuthActionTypes.UPDATE;

  constructor(public payload: any) {}
}

export class UpdateSuccess implements Action {
  readonly type = AuthActionTypes.UPDATE_SUCCESS;

  constructor(public payload: any) {}
}

export class UpdateFailure implements Action {
  readonly type = AuthActionTypes.UPDATE_FAILURE;

  constructor(public payload: any) {}
}

export type All =
  | Login | LoginSuccess | LoginFailure
  | LoginVerification | LoginVerificationSuccess | LoginVerificationFailure
  | Signup | SignupSuccess | SignupFailure
  | Logout | LogoutSuccess | LogoutFailure
  | Update | UpdateSuccess | UpdateFailure;
