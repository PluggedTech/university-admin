import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DefaultDataService, DefaultDataServiceConfig, HttpUrlGenerator, QueryParams } from '@ngrx/data';
import { Update } from '@ngrx/entity';
import { RouterReducerState } from '@ngrx/router-store';
import { select, Store } from '@ngrx/store';
import { AppState, selectRouterState } from '@store/app.states';
import { RouterStateUrl } from '@store/reducers/router/custom-route-serializer';
import * as camelcase from 'camelcase';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class DynamicDataService<T> extends DefaultDataService<T> {

  protected modelName: string;
  protected store: Store<AppState>;

  constructor(
    entityName: string,
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super(entityName, http, httpUrlGenerator, config);
    this.modelName = camelcase(entityName);
    this.store = store;
  }

  add(entity: T): Observable<T> {
    return super.add(entity).pipe(map((res: any) => res[ this.modelName ]));
  }

  delete(id: number | string): Observable<number | string> {
    return super.delete(id);
  }

  getAll(): Observable<T[]> {
    return super.getAll().pipe(map((res: any) => res[ this.modelName + 'List' ]));
  }

  getById(id: number | string): Observable<T> {
    return super.getById(id).pipe(map((res: any) => res[ this.modelName ]));
  }

  getWithQuery(queryParams: QueryParams | string): Observable<T[]> {
    return super.getWithQuery(queryParams).pipe(map((res: any) => res[ this.modelName + 'List' ]));
  }

  update(update: Update<T>): Observable<T> {
    return super.update(update).pipe(map((res: any) => res[ this.modelName ]));
  }

  deleteByParamId(): Observable<number | string> {
    return this.store
      .pipe(
        select(selectRouterState),
        switchMap(({ state }: RouterReducerState<RouterStateUrl>) => {
          const { id } = state.params;
          return this.delete(id);
        })
      );
  }

  updateByParamId(changes: T): Observable<T> {
    return this.store
      .pipe(
        select(selectRouterState),
        switchMap(({ state }: RouterReducerState<RouterStateUrl>) => {
          const { id } = state.params;
          const update: Update<T> = { id, changes };
          return this.update(update);
        })
      );
  }

  getByParamId(): Observable<T> {
    return this.store
      .pipe(
        select(selectRouterState),
        switchMap(({ state }: RouterReducerState<RouterStateUrl>) => {
          const { id } = state.params;
          return this.getById(id);
        })
      );
  }
}
