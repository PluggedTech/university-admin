import { Injectable } from '@angular/core';
import { DefaultHttpUrlGenerator, HttpResourceUrls, normalizeRoot, Pluralizer } from '@ngrx/data';
import camelcase from 'camelcase';

@Injectable({
  providedIn: 'root'
})
export class CamelcaseHttpUrlGeneratorService extends DefaultHttpUrlGenerator {

  constructor(private mPluralizer: Pluralizer) {
    super(mPluralizer);
  }

  protected getResourceUrls(entityName: string, root: string): HttpResourceUrls {
    let rsrcUrls = this.knownHttpResourceUrls[ entityName ];
    if (!rsrcUrls) {
      const nRoot = normalizeRoot(root);
      const url = `${nRoot}/${camelcase(entityName)}/`;

      rsrcUrls = {
        entityResourceUrl: url,
        collectionResourceUrl: url
      };

      this.registerHttpResourceUrls({ [ entityName ]: rsrcUrls });
    }

    return rsrcUrls;
  }
}
