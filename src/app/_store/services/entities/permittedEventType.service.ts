import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PermittedEventType } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class PermittedEventTypeService extends EntityCollectionServiceBase<PermittedEventType> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('PermittedEventType', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class PermittedEventTypeDataService extends DynamicDataService<PermittedEventType> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('PermittedEventType', http, httpUrlGenerator, store, config);
  }
}
