import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Classification } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class ClassificationService extends EntityCollectionServiceBase<Classification> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Classification', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class ClassificationDataService extends DynamicDataService<Classification> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('Classification', http, httpUrlGenerator, store, config);
  }
}
