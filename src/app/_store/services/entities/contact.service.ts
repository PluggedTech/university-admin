import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Contact } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class ContactService extends EntityCollectionServiceBase<Contact> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Contact', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class ContactDataService extends DynamicDataService<Contact> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('Contact', http, httpUrlGenerator, store, config);
  }
}
