import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Industry } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class IndustryService extends EntityCollectionServiceBase<Industry> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Industry', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class IndustryDataService extends DynamicDataService<Industry> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('Industry', http, httpUrlGenerator, store, config);
  }
}
