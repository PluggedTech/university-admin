import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Company } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class CompanyService extends EntityCollectionServiceBase<Company> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Company', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class CompanyDataService extends DynamicDataService<Company> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('Company', http, httpUrlGenerator, store, config);
  }
}
