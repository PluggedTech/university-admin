import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { University } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class UniversityService extends EntityCollectionServiceBase<University> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('University', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class UniversityDataService extends DynamicDataService<University> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('University', http, httpUrlGenerator, store, config);
  }
}
