import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Event } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class EventService extends EntityCollectionServiceBase<Event> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Event', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class EventDataService extends DynamicDataService<Event> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('Event', http, httpUrlGenerator, store, config);
  }
}
