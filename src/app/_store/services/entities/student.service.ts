import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Student } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class StudentService extends EntityCollectionServiceBase<Student> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Student', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class StudentDataService extends DynamicDataService<Student> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('Student', http, httpUrlGenerator, store, config);
  }
}
