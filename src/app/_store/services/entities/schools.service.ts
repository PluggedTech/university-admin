import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { School } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class SchoolService extends EntityCollectionServiceBase<School> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('School', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class SchoolDataService extends DynamicDataService<School> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('School', http, httpUrlGenerator, store, config);
  }
}
