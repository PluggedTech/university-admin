import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Major } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class MajorService extends EntityCollectionServiceBase<Major> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Major', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class MajorDataService extends DynamicDataService<Major> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('Major', http, httpUrlGenerator, store, config);
  }
}
