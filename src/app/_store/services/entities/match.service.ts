import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Match } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class MatchService extends EntityCollectionServiceBase<Match> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Match', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class MatchDataService extends DynamicDataService<Match> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('Match', http, httpUrlGenerator, store, config);
  }
}
