import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CampusPartner } from '@api/generated';
import {
  DefaultDataServiceConfig,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  HttpUrlGenerator
} from '@ngrx/data';
import { Store } from '@ngrx/store';
import { AppState } from '@store/app.states';
import { DynamicDataService } from '@store/services/dynamic-data.service';

@Injectable({ providedIn: 'root' })
export class CampusPartnerService extends EntityCollectionServiceBase<CampusPartner> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('CampusPartner', factory);
  }
}

@Injectable({ providedIn: 'root' })
export class CampusPartnerDataService extends DynamicDataService<CampusPartner> {
  constructor(
    http: HttpClient,
    httpUrlGenerator: HttpUrlGenerator,
    store: Store<AppState>,
    config: DefaultDataServiceConfig,
  ) {
    super('CampusPartner', http, httpUrlGenerator, store, config);
  }
}
