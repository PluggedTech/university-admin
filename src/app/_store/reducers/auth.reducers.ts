import { Recruiter } from '@api/generated';
import { All, AuthActionTypes } from '@store/actions/auth.actions';

export interface AuthState {
  isAuthenticated: boolean;
  isVerified: boolean;
  user: Recruiter | null;
  errorMessage: string | null;
}

export const initialState: AuthState = {
  isAuthenticated: false,
  isVerified: false,
  user: null,
  errorMessage: null
};

export function reducer(state = initialState, action: All): AuthState {
  switch (action.type) {
    case AuthActionTypes.LOGIN_SUCCESS:
    case AuthActionTypes.SIGNUP_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        isVerified: false,
        user: {
          ...action.payload,
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.LOGIN_VERIFICATION_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        isVerified: true,
        user: {
          ...action.payload,
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.UPDATE_SUCCESS: {
      return {
        ...state,
        user: {
          ...action.payload,
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.LOGIN_FAILURE: {
      return {
        ...state,
        errorMessage: 'Incorrect email and/or password.'
      };
    }
    case AuthActionTypes.LOGIN_VERIFICATION_FAILURE: {
      return {
        ...state,
        isAuthenticated: false,
        isVerified: true,
        errorMessage: 'Unable to verify authenticated user session.'
      };
    }
    case AuthActionTypes.SIGNUP_FAILURE: {
      return {
        ...state,
        errorMessage: 'Unable to create new user account; that email is already in use.'
      };
    }
    case AuthActionTypes.UPDATE_FAILURE: {
      return {
        ...state,
        errorMessage: 'Something went wrong, unable to update user.'
      };
    }
    case AuthActionTypes.LOGOUT_SUCCESS: {
      return initialState;
    }
    case AuthActionTypes.LOGOUT_FAILURE: {
      return {
        ...state,
        errorMessage: 'Unable to terminate the user session.'
      };
    }
    default: {
      return state;
    }
  }
}
