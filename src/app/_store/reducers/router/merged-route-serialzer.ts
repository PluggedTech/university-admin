import { ActivatedRouteSnapshot, Data, Params, RouterStateSnapshot } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';
import { MergedRoute } from './merged-route.reducer';

export class MergedRouterReducerStateSerializer implements RouterStateSerializer<MergedRoute> {
  serialize(state: RouterStateSnapshot): MergedRoute {
    return {
      url: state.url,
      params: this.mergeRouteParams(state.root, r => r.params),
      queryParams: this.mergeRouteParams(state.root, r => r.queryParams),
      data: this.mergeRouteData(state.root)
    }
  }

  private mergeRouteParams(route: ActivatedRouteSnapshot, getter: (r: ActivatedRouteSnapshot) => Params): Params {
    if (!route) return {};

    const currentParams = getter(route);
    const primaryChild = route.children.find(c => c.outlet === 'primary') || route.firstChild;

    return {...currentParams, ...this.mergeRouteParams(primaryChild, getter)};
  }

  private mergeRouteData(route: ActivatedRouteSnapshot): Data {
    if (!route) return {};

    const currentData = route.data;
    const primaryChild = route.children.find(c => c.outlet === 'primary') || route.firstChild;

    return {...currentData, ...this.mergeRouteData(primaryChild)};
  }
}
