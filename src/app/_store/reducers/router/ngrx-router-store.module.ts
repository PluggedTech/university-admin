import { NgModule, Optional, Self } from '@angular/core';
import { Router } from '@angular/router';
import { routerReducer, RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { MergedRouterReducerStateSerializer } from './merged-route-serialzer';

export const routerStateConfig = {
  stateKey: 'router', // state-slice name for routing state
};

@NgModule({
  imports: [
    StoreModule.forFeature(routerStateConfig.stateKey, routerReducer),
    StoreRouterConnectingModule.forRoot(routerStateConfig)
  ],
  exports: [
    StoreModule,
    StoreRouterConnectingModule
  ],
  providers: [
    { provide: RouterStateSerializer, useClass: MergedRouterReducerStateSerializer }
  ],
})
export class NgrxRouterStoreModule {
  constructor(
    @Self() @Optional() router: Router
  ) {
    if (router) {
      console.log('[NgrxRouterStoreModule] was successfully loaded.');
    } else {
      console.error('[NgrxRouterStoreModule] must be imported at the same level as RouterModule.');
    }
  }
}
