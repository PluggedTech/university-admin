import { createFeatureSelector, createSelector } from "@ngrx/store";
import { MergedRouterReducerState } from "./merged-route.reducer";
import { routerStateConfig } from "./ngrx-router-store.module";


export const getRouterReducerState = createFeatureSelector<MergedRouterReducerState>(routerStateConfig.stateKey);

export const getMergedRoute = createSelector(getRouterReducerState, (routerReducerState) => routerReducerState.state);
