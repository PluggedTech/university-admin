import { NgModule } from '@angular/core';
import { DefaultDataServiceConfig, EntityDataModule, EntityDataService, HttpUrlGenerator } from '@ngrx/data';
import { CamelcaseHttpUrlGeneratorService } from '@store/services/camelcase-http-url-generator.service';
import { CampusPartnerDataService, CampusPartnerService } from '@store/services/entities/campus-partner.service';
import { ClassificationDataService, ClassificationService } from '@store/services/entities/classification.service';
import { CompanyDataService, CompanyService } from '@store/services/entities/company.service';
import { ContactDataService, ContactService } from '@store/services/entities/contact.service';
import { EventDataService, EventService } from '@store/services/entities/event.service';
import { IndustryDataService, IndustryService } from '@store/services/entities/industry.service';
import { JobDataService, JobService } from '@store/services/entities/job.service';
import { MajorDataService, MajorService } from '@store/services/entities/major.service';
import { MatchDataService, MatchService } from '@store/services/entities/match.service';
import { PermittedEventTypeDataService, PermittedEventTypeService } from '@store/services/entities/permittedEventType.service';
import { SchoolDataService, SchoolService } from '@store/services/entities/schools.service';
import { StudentDataService, StudentService } from '@store/services/entities/student.service';
import { UniversityDataService, UniversityService } from '@store/services/entities/university.service';
import { defaultDataServiceConfig, entityConfig } from './entity-metadata';

const providers = [
  CampusPartnerService, CampusPartnerDataService,
  ClassificationService, ClassificationDataService,
  CompanyService, CompanyDataService,
  ContactService, ContactDataService,
  EventService, EventDataService,
  IndustryService, IndustryDataService,
  JobService, JobDataService,
  MajorService, MajorDataService,
  MatchService, MatchDataService,
  PermittedEventTypeService, PermittedEventTypeDataService,
  SchoolService, SchoolDataService,
  StudentService, StudentDataService,
  UniversityService, UniversityDataService,
];

@NgModule({
  imports: [
    EntityDataModule.forRoot(entityConfig),
  ],
  providers: [
    ...providers,
    { provide: DefaultDataServiceConfig, useValue: defaultDataServiceConfig },
    { provide: HttpUrlGenerator, useClass: CamelcaseHttpUrlGeneratorService },
  ]
})
export class EntityStoreModule {
  constructor(
    campusPartnerDataService: CampusPartnerDataService,
    classificationDataService: ClassificationDataService,
    companyDataService: CompanyDataService,
    contactDataService: ContactDataService,
    entityDataService: EntityDataService,
    eventDataService: EventDataService,
    industryDataService: IndustryDataService,
    jobDataService: JobDataService,
    majorDataService: MajorDataService,
    matchDataService: MatchDataService,
    permittedEventTypeDataService: PermittedEventTypeDataService,
    schoolDataService: SchoolDataService,
    studentDataService: StudentDataService,
    universityDataService: UniversityDataService,
  ) {
    entityDataService.registerServices({
      'CampusPartner': campusPartnerDataService,
      'Classification': classificationDataService,
      'Company': companyDataService,
      'Contact': contactDataService,
      'Event': eventDataService,
      'Industry': industryDataService,
      'Job': jobDataService,
      'Major': majorDataService,
      'Match': matchDataService,
      'PermittedEventType': permittedEventTypeDataService,
      'School': schoolDataService,
      'Student': studentDataService,
      'University': universityDataService,
    });
  }
}
