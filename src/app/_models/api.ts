interface BaseInterfaceA {
  id?: number | string;
  title: string;
}

interface BaseInterfaceB {
  id?: number | string;
  status?: string;
  dateCreated?: string;
  dateUpdated?: string;
}

export interface Job extends BaseInterfaceA, BaseInterfaceB {
  companyName: string;
  jobType: string;
  jobEmploymentType: string;
  duration: string;
  description: string;
  jobFunction: string;
  location: string;
  locationRemoteAllowed: boolean;
  salaryRange: string;
  skills: string;
  experience: string;
}

export interface JobProvider extends BaseInterfaceB {
  firstName: string;
  lastName: string;
  email: string;
  emailVerified: boolean;
  emailVerificationID: number | null;
  phoneNumber: string;
  title: string;
  imgCover?: string;
  imgProfile?: string;
  bio?: string;
  webSiteURL?: string;
  webLinkedID?: string;
  webLinkedURL?: string;
  webTwitterID?: string;
  webTwitterURL?: string;
  webFacebookID?: string;
  webFacebookURL?: string;
  profileCompleted: boolean;
}

export interface Company extends BaseInterfaceB {
  jobProviderID: number | string;
  name: string;
  email: string;
  emailVerified: boolean;
  emailVerificationID: number | null;
  phoneNumber: string;
  industryID: number;
  description: string;
  totalNumOfEmployees: number;
  totalFunding: number;
  isStartup: boolean;
  lastFundingDate: string;
  lastFundingRound: string;
  webSiteURL?: string;
  webLinkedID?: string;
  webLinkedURL?: string;
  webTwitterID?: string;
  webTwitterURL?: string;
  webFacebookID?: string;
  webFacebookURL?: string;
}
