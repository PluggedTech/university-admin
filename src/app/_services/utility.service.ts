import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';

export type Platform = 'android' | 'ios' | 'facebook' | 'twitter';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor() { }

  getExternalPlatformUrl(platform: Platform): string {
    let link;

    if (platform === 'android') {
      link = environment.platforms.playStore;
    }

    if (platform === 'ios') {
      link = environment.platforms.appStore;
    }

    if (platform === 'facebook') {
      link = environment.platforms.facebook;
    }

    if (platform === 'twitter') {
      link = environment.platforms.twitter;
    }

    return link;
  }
}
