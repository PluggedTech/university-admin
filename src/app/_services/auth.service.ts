import { AuthGuard } from './../_guards/auth.guard';
import { AlertService } from './alert.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService as ApiAuthService, Recruiter, RecruiterCreate, User, UserLogin } from '@api/generated';
import { merge, pick } from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { QBConfig } from './../QBConfig';
import { UserChatService } from './user-chat.service';
import { QBHelper } from '@app/qbHelper';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup } from '@angular/forms';

declare var QB: any;

QB.init(
  QBConfig.credentials.appId,
  QBConfig.credentials.authKey,
  QBConfig.credentials.authSecret,
  QBConfig.appConfig
);


interface AuthState {
  isAuthenticated: boolean;
  user: Recruiter | null | any;
  token: string | null;
  errorMessage: string | null;
}

const initState: AuthState = {
  isAuthenticated: false,
  user: null,
  token: null,
  errorMessage: null,
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private state: AuthState = { ...initState };
  private keys = Object.keys(this.state);

  constructor(
    private snackBar: MatSnackBar,
    private auth: ApiAuthService,
    private router: Router,
    private alert: AlertService,
    private dialog: MatDialog,
    private qbHelper: QBHelper,
  ) {
    const rehydrate = JSON.parse(localStorage.getItem('auth')) || this.state;
    let state = merge(this.state, rehydrate);
    state = pick(state, this.keys);
    this.setState(state);
  }

  private _state$ = new BehaviorSubject<AuthState>(this.state);

  get state$(): Observable<AuthState> { return this._state$.asObservable(); }

  get isAuthenticated(): boolean { return this.state.isAuthenticated; }

  get errorMessage(): string | null { return this.state.errorMessage; }

  get user(): Recruiter | null { return this.state.user; }

  login(payload: UserLogin) {
    return this.auth
      .authLoginPost(payload)
      .pipe(
        tap(async ({ user, token }) => {
          this.state = {
            ...this.state,
            isAuthenticated: true,
            user,
            token
          };

          this._state$.next(this.state);

          localStorage.setItem('auth', JSON.stringify(this.state));
          const auth = JSON.parse(localStorage.getItem('auth'));

          this.qbHelper.connectChat(auth.user.qbCredential)

          await this.router.navigate(['e', 'dashboard']);
        }),
        catchError(err => {
          this.state = {
            ...this.state,
            errorMessage: 'Incorrect email and/or password.'
          };

          this.snackBar.open(this.state.errorMessage, 'Error', { duration : 2000 })

          this._state$.next(this.state);

          localStorage.setItem('auth', JSON.stringify(this.state));

          return err;
        })
      );
  }

  logout(): void {
    this.auth
      .authLogoutPost()
      .pipe(
        tap(async () => {
          this.clearState();
          await this.router.navigate(['login']);
        }),
        catchError(async () => {
          this.clearState();
          await this.router.navigate(['login']);
        })
      )
      .subscribe();
  }

  registerUser(payload: RecruiterCreate, form : any): void {
    this.auth
      .authRegisterPost(payload)
      .subscribe((registerRes) => {
          const state = {
            user: {
              ...registerRes,
            },
            errorMessage: null
          };

          const alert = this.alert.showAlert({
            html: 'Please check your email to activate your account',
            position: 'top-right'
          });

          setTimeout(() => {
            this.dialog.closeAll();
            location.href = '/welcome';
          }, 5000);

          this.setState(state);

          form.reset()

          return registerRes;
      }, registerErr => {
          const parseMessage = registerErr.split('-')

          const state = {
            ...this.state,
            errorMessage: parseMessage[1]
          };

          this.snackBar.open(state.errorMessage, 'Error', { duration : 2000 })

          this._state$.next(state)

          return registerErr;
      })
  }

  updateUser(payload: User): void {
    this.auth
      .authUpdatePost(payload)
      .pipe(
        tap((res) => {
          const state = {
            user: {
              ...res.user,
            },
            errorMessage: null
          };

          this.setState(state);
        }),
        catchError(err => {
          const state = {
            ...this.state,
            errorMessage: 'Unable to update profile.'
          };

          this.setState(state);

          return err;
        })
      )
      .subscribe();
  }

  private setState(state): void {
    this.state = {
      ...this.state,
      ...state
    };

    this._state$.next(this.state);

    localStorage.setItem('auth', JSON.stringify(this.state));
  }

  private clearState(): void {
    this.setState(initState);
  }
}
