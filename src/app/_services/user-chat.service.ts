import { Injectable } from '@angular/core';
import { QBConfig } from './../QBConfig';
import { QBHelper } from './../qbHelper';
import { StudentService } from '@api/generated';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

declare var QB: any;

@Injectable({
  providedIn: 'root'
})
export class UserChatService {

  constructor(
    private qbHelper: QBHelper,
    private http : HttpClient
  ) { }

  async startChatSession(user) {
    QB.init(
      QBConfig.credentials.appId,
      QBConfig.credentials.authKey,
      QBConfig.credentials.authSecret,
      QBConfig.appConfig
    );

    this.createChatSession(user);
  }

  getQBUser(id): any {
    const ids = [];
    ids.push(id);

    const searchParams = { filter: { field: 'id', param: 'in', value: ids } };

    return new Promise((resolve, reject) => {
      QB.users.listUsers(searchParams, function(error, result) {
        if (error) {
          reject(error);
        }

        resolve(result.items[ 0 ].user);
      });
    });
  }

  getStudentFromQB(email){
    return this.http.get<any>(`${environment.apiUrl}/api/user?email=${email}`).toPromise();
  }

  private createChatSession(user) {
    this.qbHelper
      .qbCreateConnection(user)
      .then(result => {
        this.setConnection(result.id, user.password);
        localStorage.setItem('chat', JSON.stringify(result));
      })
      .catch(err => console.error('[ChatComponent] Create Connection Error', err));
  }

  private setConnection(userId, password) {
    this.qbHelper
      .qbChatConnection(userId, password)
      .then(() => {
      })
      .catch(err => console.error('[ChatComponent] Set Connection Error', err));
  }
}
