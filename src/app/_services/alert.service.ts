import { Injectable } from '@angular/core';
import SweetAlert2Module, { SweetAlertIcon, SweetAlertPosition } from 'sweetalert2';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  showAlert(config: {
    title?: string,
    html?: string,
    icon?: SweetAlertIcon,
    position?: SweetAlertPosition
  }) {

    return Swal.fire({
      title: config.title || '',
      html: config.html || '',
      icon: config.icon || 'success',
      position: config.position || 'center'
    });
  }

  closeAlert(alert) {
    Swal.close(alert);
  }
}
