import { Injectable } from '@angular/core';
import { Sector, SectorService as Api } from '@api/generated';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SectorService {

  private dataStore: { sectors: Sector[] } = { sectors: [] };
  private _sectors = <BehaviorSubject<Sector[]>>new BehaviorSubject([]);

  get sectors() { return this._sectors.asObservable(); }

  constructor(
    private api: Api,
  ) { }

  loadAll() {
    this.api.apiSectorGet()
      .subscribe(
        res => {
          this.dataStore.sectors = res.sectorList;
          this._sectors.next({ ...this.dataStore }.sectors);
        },
        err => console.error('[SectorService] Loading Error - ', err)
      );
  }
}
