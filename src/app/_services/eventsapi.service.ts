import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventsapiService {

  api = 'http://localhost:2020/api/event'

  constructor(private http : HttpClient) { }

  submitEvent(data){
    return this.http.post<any>(this.api, data)
  }
}
