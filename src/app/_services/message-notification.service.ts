import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'apollo-link';

@Injectable({
  providedIn: 'root'
})
export class MessageNotificationService {

  private _unreadMessagesSource = new BehaviorSubject<any>('');
  _unreadMessages = this._unreadMessagesSource.asObservable();

  constructor() { }

  updateUnreadMessages(number){
    this._unreadMessagesSource.next(number)
  }
}
