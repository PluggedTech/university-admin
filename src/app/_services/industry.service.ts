import { Injectable } from '@angular/core';
import { Industry, IndustryService as Api } from '@api/generated';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IndustryService {
  private dataStore: { industries: Industry[] } = { industries: [] };
  private _industries = <BehaviorSubject<Industry[]>>new BehaviorSubject([]);

  get industries() { return this._industries.asObservable(); }

  constructor(
    private api: Api,
  ) {}

  loadAll() {
    this.api.apiIndustryGet()
      .subscribe(
        res => {
          this.dataStore.industries = res.industryList;
          this._industries.next({ ...this.dataStore }.industries);
        },
        err => console.error('[IndustryService] Loading Error - ', err)
      );
  }
}
