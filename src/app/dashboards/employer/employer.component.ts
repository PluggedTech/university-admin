import { Component, OnInit } from '@angular/core';
import { Event, Job, JobService, EventService } from '@api/generated';
import { SimplePanelOptions } from '@app/_components/panels/simple-panel/simple-panel.component';
import { faBriefcase, faCalendar, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-employer',
  templateUrl: './employer.component.html',
  styleUrls: [ './employer.component.scss' ]
})
export class EmployerComponent implements OnInit {

  jobsPanelOptions: SimplePanelOptions = {
    leftIcon: { title: 'View Jobs', routerLink: '/e/jobs', faIcon: faBriefcase },
    rightIcon: { title: 'Create New Job', routerLink: '/e/jobs/create', faIcon: faPlusCircle }
  };

  eventsPanelOptions: SimplePanelOptions = {
    leftIcon: { title: 'View Events', routerLink: '/e/events', faIcon: faCalendar },
    rightIcon: { title: 'Create New Event', routerLink: '/e/events/create', faIcon: faPlusCircle }
  };


  private _jobs$ = new BehaviorSubject<any>('');
  get jobs$(): Observable<any> { return this._jobs$.asObservable(); }

  private _events$ = new BehaviorSubject<any>('');
  get events$(): Observable<any> { return this._events$.asObservable(); }

  user : any;

  constructor(
    private jobService: JobService,
    private eventService: EventService,
  ) {
  }

  async ngOnInit() {
    const recruiter = JSON.parse(localStorage.getItem('auth'))

    this.jobService.apiJobQueryPost({ recruiterId : recruiter.user.id })
    .subscribe(data => {
      this._jobs$.next(data.jobList)
    }, error => {
      console.log(error)
    })

    this.eventService.apiEventQueryPost({ recruiterId : recruiter.user.id })
    .subscribe(data => {
      this._events$.next(data.eventList)
    }, error => {
      console.log(error)
    })
  }
}
