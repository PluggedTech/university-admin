import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobService, Match, MatchService, StudentService } from '@api/generated';
//import { MatchService } from '@store/services/entities/match.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { subscribeOn } from 'rxjs/operators';


@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: [ './candidates.component.scss' ]
})
export class CandidatesComponent implements OnInit {

  DEFAULT_AVATAR = "assets/images/avtar/3.jpg";

  jobId: string;
  job: any;
  studentIds : any;

  unsubscribe$ = new Subject<void>();

  columnsToDisplay: string[] = [
    'student',
    'actions'
  ];

  constructor(
    private matchService: MatchService,
    private studentService: StudentService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private jobService: JobService
  ) {
  }

  private _job$ = new BehaviorSubject<any>('');
  get job$(): Observable<any> { return this._job$.asObservable(); }

  private _matches$ = new BehaviorSubject<any>('');
  get matches$(): Observable<any> { return this._matches$.asObservable(); }

  private _studentIds$ = new BehaviorSubject<any>('');
  get studentIds$(): Observable<any> { return this._studentIds$.asObservable(); }

  private _students$ = new BehaviorSubject<any>('');
  get students$(): Observable<any> { return this._students$.asObservable(); }

  ngOnInit(): void {
    this.jobId = this.activeRoute.snapshot.paramMap.get('id');

    this.jobService.apiJobIdGet(Number(this.jobId))
    .subscribe(data => {
      this._job$.next(data.job);
    }, err => {
      console.log(err);
    });

    this.matchService.apiMatchQueryPost({ jobId: Number(this.jobId) })
    .subscribe(data => {
      this._matches$.next(data.matchList)
      this.studentIds = data.matchList.map((match) => {
        return match.studentId;
      })
      this._studentIds$.next(this.studentIds)
    }, error => {
      console.log(error)
    })

    this.studentIds$.subscribe(studentIds => {
      this.studentService.apiStudentQueryPost({ id : studentIds })
      .subscribe(data => {
        this._students$.next(data.studentList) 
      }, error => {
        console.log(error)
      })
    }, error => {
      console.log(error)
    })


  }
}
