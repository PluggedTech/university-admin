import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student, StudentService } from '@api/generated';
//import { StudentService } from '@store/services/entities/student.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { QBHelper } from '@app/qbHelper';

@Component({
  selector: 'app-opportunities-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: [ './student-profile.component.scss' ]
})
export class StudentProfileComponent implements OnInit {

  DEFAULT_AVATAR = "assets/images/avtar/3.jpg";

  jobId: string;
  studentId: string;
  student : any;

  private _student$ =  new BehaviorSubject<any>('');
  eventID: string;
  get student$() : Observable<any> { return this._student$.asObservable() };

  constructor(
    private router: ActivatedRoute,
    private studentService : StudentService,
    private route : Router,
    private qbHelper : QBHelper
  ) {}

  ngOnInit() {
    this.studentId = this.router.snapshot.paramMap.get('studentid');
    this.jobId = this.router.snapshot.paramMap.get('jobid');
    this.eventID = this.router.snapshot.paramMap.get('id');
    this.studentService.apiStudentIdGet(Number(this.studentId))
    .subscribe(data => {
      this._student$.next(data.student)
      this.student = data.student;
    }, error => {
      console.log(error)
    })
  }

  initiateChat(){
    this.qbHelper.createDialog(this.student.qbCredential.id)

    if(this.jobId){
      this.route.navigateByUrl(`/e/job/${this.jobId}/student/${this.studentId}/chat`)
    }

    if(this.eventID){
      this.route.navigateByUrl(`/e/events/${this.eventID}/student/${this.studentId}/chat`)
    }
  }

}
