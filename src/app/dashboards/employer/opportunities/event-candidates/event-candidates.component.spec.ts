import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCandidatesComponent } from './event-candidates.component';

describe('EventCandidatesComponent', () => {
  let component: EventCandidatesComponent;
  let fixture: ComponentFixture<EventCandidatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCandidatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCandidatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
