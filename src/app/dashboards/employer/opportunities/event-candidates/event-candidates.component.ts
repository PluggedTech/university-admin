import { EventService, StudentService, MatchEventService } from '@api/generated';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-event-candidates',
  templateUrl: './event-candidates.component.html',
  styleUrls: ['./event-candidates.component.scss']
})
export class EventCandidatesComponent implements OnInit {


  DEFAULT_AVATAR = 'assets/images/avtar/3.jpg';

  private _events$ = new BehaviorSubject<any>('');
  eventID: string;
  studentIds = [];
  get events$(): Observable<any> { return this._events$.asObservable(); }

  private _studentIds$ = new BehaviorSubject<any>('');
  get studentIds$(): Observable<any> { return this._studentIds$.asObservable(); }

  private _students$ = new BehaviorSubject<any>('');
  get students$(): Observable<any> { return this._students$.asObservable(); }

  constructor(
    private eventService: EventService,
    private activeRoute: ActivatedRoute,
    private studentService: StudentService,
    private matchEvent : MatchEventService
  ) { }

  ngOnInit(): void {
    this.eventID = this.activeRoute.snapshot.paramMap.get('id');
    this.eventService.apiEventIdGet(Number(this.eventID))
    .subscribe(data => {
      this._events$.next(data.event);
    }, err => {
      console.log(err);
    });

    this.matchEvent.apiMatchEventQueryPost({ eventId : Number(this.eventID) })
    .subscribe(result => {

      this._studentIds$.next(result.matchEventList.map((matchEvent) => {
        return matchEvent.student.id
      }));

    }, error => {
      console.log(error)
    })

    this.studentIds$.subscribe(studentIds => {
      this.studentService.apiStudentQueryPost({ id : studentIds })
      .subscribe(data => {
        this._students$.next(data.studentList);
      }, error => {
        console.log(error);
      });
    }, error => {
      console.log(error);
    });

  }

}
