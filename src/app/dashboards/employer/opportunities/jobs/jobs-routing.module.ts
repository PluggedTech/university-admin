import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobCreateComponent } from '@dashboards/employer/opportunities/jobs/crud/job-create.component';
import { JobReadComponent } from '@dashboards/employer/opportunities/jobs/crud/job-read/job-read.component';
import { JobUpdateComponent } from '@dashboards/employer/opportunities/jobs/crud/job-update.component';
import { JobsComponent } from '@dashboards/employer/opportunities/jobs/jobs.component';

const routes: Routes = [
  {
    path: 'jobs',
    children: [
      { path: '', component: JobsComponent },
      { path: 'create', component: JobCreateComponent },
      { path: ':id/update', component: JobUpdateComponent },
      { path: ':id', component: JobReadComponent },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class JobsRoutingModule {}
