import { Component, OnInit } from '@angular/core';
import { Job, JobService } from '@api/generated';
import { Observable, BehaviorSubject } from 'rxjs';

import { ShepherdService } from 'angular-shepherd';
import steps from './steps';

@Component({
  selector: 'app-opportunities-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
})
export class JobsComponent implements OnInit {

  private _jobs$ = new BehaviorSubject<any>('');
  get jobs$(): Observable<any> { return this._jobs$.asObservable(); }

  columnsToDisplay: string[] = [
    // toggle column by un/commenting array item below
    // 'id',
    'title',
    'candidates',    
    'majors',
    'employmentType',    
    'applicationDeadline',   
    'actions',
  ];

  constructor(
    private jobService: JobService, private shepherdService: ShepherdService
  ) {
  }

  ngOnInit() {
    const recruiter = JSON.parse(localStorage.getItem('auth'))
    this.jobService.apiJobQueryPost({ recruiterId : recruiter.user.id})
    .subscribe(data => {
      this._jobs$.next(data.jobList)

      //Show hints on the firstTimeLoad
      if (localStorage['firstTimeLoad'] != 'TRUE') {

        this.shepherdService.defaultStepOptions = {
          classes: '.mat-column-candidates',
          scrollTo: false,
          cancelIcon: {
            enabled: true
          }
        };

        this.shepherdService.requiredElements = [
          {
            selector: '.mat-column-candidates',
            message: 'No search results found. Please execute another search, and try to start the tour again.',
            title: 'No results'
          },
        ];

        this.shepherdService.modal = true;
        this.shepherdService.confirmCancel = false;

        if (data.jobList.length == 0){
          //if no jobs then point to header
          steps[0].attachTo.element = 'thead tr:first-child .mat-column-candidates';
          steps[1].attachTo.element = 'thead tr:first-child .mat-column-actions';
        }

        this.shepherdService.addSteps(steps);
        this.shepherdService.start();

        localStorage['firstTimeLoad'] = 'TRUE';
      }
    }, error => {
      console.log(error)
    })
  }

  isInterested(match) {
    return match.isInterested === true;
  }
}
