const steps = 
[
    {
      id: 'intro',
      attachTo: { 
        element: 'tbody tr:first-child .mat-column-candidates', 
        on: 'right'
      },
      beforeShowPromise: function() {
        return new Promise(function(resolve) {
          setTimeout(function() {
            window.scrollTo(0, 0);
            resolve();
          }, 500);
        });
      },
      buttons: [
        {
          classes: 'shepherd-button-secondary',
          text: 'Exit',
          type: 'cancel'
        },
     /*    {
          classes: 'shepherd-button-primary',
          text: 'Back',
          type: 'back'
        }, */
        {
          classes: 'shepherd-button-primary',
          text: 'Next',
          type: 'next'
        }
      ],
      cancelIcon: {
        enabled: false
      },
      classes: 'custom-class-name-1 custom-class-name-2',
      highlightClass: 'highlight',
      scrollTo: false,
      title: '',
      text: ['Click here to view the list of students who applied for this role'],
      when: {
        show: () => {
          console.log('show step');
        },
        hide: () => {
          console.log('hide step');
        }
      }
    },
    {
      id: 'intro',
      attachTo: { 
        element: 'tbody tr:first-child .mat-column-actions', 
        on: 'left'
      },
      beforeShowPromise: function() {
        return new Promise(function(resolve) {
          setTimeout(function() {
            window.scrollTo(0, 0);
            resolve();
          }, 500);
        });
      },
      buttons: [
        {
          classes: 'shepherd-button-primary',
          text: 'Back',
          type: 'back'
        },        
        {
          classes: 'shepherd-button-secondary',
          text: 'Exit',
          type: 'cancel'
        },
        /* {
          classes: 'shepherd-button-primary',
          text: 'Next',
          type: 'next'
        } */
      ],
      cancelIcon: {
        enabled: false
      },
      classes: 'custom-class-name-1 custom-class-name-2',
      highlightClass: 'highlight',
      scrollTo: false,
      title: '',
      text: ['Click here to edit and/or review your job'],
      when: {
        show: () => {
          console.log('show step');
        },
        hide: () => {
          console.log('hide step');
        }
      }
    },
  ];

  export default steps;