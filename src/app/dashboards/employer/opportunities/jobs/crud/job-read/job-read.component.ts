import { Location } from '@angular/common';
import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Job } from '@api/generated';
import { JobBaseComponent } from '@dashboards/employer/opportunities/jobs/crud/job-base.component';
import { JobDataService } from '@store/services/entities/job.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-opportunities-jobs-update',
  templateUrl: './job-read.component.html',
  styleUrls: [ '../job-base.component.scss' ],
})
export class JobReadComponent extends JobBaseComponent implements OnInit {

  job$: Observable<Job>;

  constructor(
    protected injector: Injector,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private jobDataService: JobDataService,
  ) {
    super(injector);

    this.submitButtonText = 'Edit';
    this.showDeleteButton = false;
  }

  ngOnInit(): void {
    this.job$ = this.jobDataService.getByParamId();
  }

  onCancelButtonClick(): void {
    this.location.back();
  }

  async onSubmitButtonClick(): Promise<void> {
    await this.router.navigate([ 'update' ], { relativeTo: this.route, queryParamsHandling: 'preserve' });
  }

}
