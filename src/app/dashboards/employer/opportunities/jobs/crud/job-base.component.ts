import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDatepicker } from '@angular/material/datepicker';
import { Classification, EnumYesOrNo, Industry, Job, Major, School, University } from '@api/generated';
import { CustomValidators } from '@app/custom-validators';
import { ClassificationService } from '@store/services/entities/classification.service';
import { IndustryService } from '@store/services/entities/industry.service';
import { MajorService } from '@store/services/entities/major.service';
import { SchoolService } from '@store/services/entities/schools.service';
import { UniversityService } from '@store/services/entities/university.service';
import moment, { Moment } from 'moment';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { Observable, Subject } from 'rxjs';
import EmploymentTypeEnum = Job.EmploymentTypeEnum;
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  template: ``,
})
export class JobBaseComponent implements OnInit, OnDestroy {

  formGroup: FormGroup;

  /** @usage The text applied to the panel's header. */
  title: string;

  industries$: Observable<Industry[]>;
  classifications$: Observable<Classification[]>;
  universities$: Observable<University[]>;
  schools$: Observable<School[]>;
  majors$: Observable<Major[]>;

  locRemoteWorkChecked = false;
  formIsSubmitted = false;
  showDeleteButton = false;
  submitButtonText = 'Submit';
  cancelButtonText = 'Cancel';

  /** Options for Google Places Autocomplete API */
  lcOptions: Options = {
    bounds: undefined,
    componentRestrictions: {
      country: 'US'
    },
    fields: [ 'formatted_address', 'place_id' ],
    strictBounds: false,
    types: []
  };

  /**
   * Human-readable string mapping of `EmploymentTypeEnum`.
   * */
  employmentTypes = new Map<string, string>([
    [ EmploymentTypeEnum.FULLTIME, 'Full-Time' ],
    [ EmploymentTypeEnum.PARTTIME, 'Part-Time' ],
    [ EmploymentTypeEnum.TEMPORARY, 'Temporary' ],
    [ EmploymentTypeEnum.SEASONAL, 'Seasonal' ],
    [ EmploymentTypeEnum.FREELANCE, 'Freelance' ],
    [ EmploymentTypeEnum.CONSULTANT, 'Consultant' ],
    [ EmploymentTypeEnum.INTERN, 'Intern' ],
  ]);

  /**
   * Human-readable string mapping of `EnumYesOrNo`.
   * */
  yesOrNo = new Map<string, string>([
    [ EnumYesOrNo.YES, 'Yes' ],
    [ EnumYesOrNo.NO, 'No' ],
  ]);

  skills: string[] = [];

  Editor = ClassicEditor;
  editorModel = {
    editorData: ''
  };

  readonly separatorKeyCodes: number[] = [ ENTER, COMMA ];
  protected unsubscribe$ = new Subject();
  // INJECTORS
  private formBuilder: FormBuilder;
  // INJECTORS: API SERVICES
  private classificationService: ClassificationService;
  private industryService: IndustryService;
  private majorService: MajorService;
  private schoolService: SchoolService;
  private universityService: UniversityService;

  constructor(
    protected injector: Injector,
  ) {
    // INSTANTIATE INJECTORS
    this.formBuilder = this.injector.get(FormBuilder);
    // INSTANTIATE INJECTORS - API SERVICES
    this.classificationService = this.injector.get(ClassificationService);
    this.industryService = this.injector.get(IndustryService);
    this.majorService = this.injector.get(MajorService);
    this.schoolService = this.injector.get(SchoolService);
    this.universityService = this.injector.get(UniversityService);

    // BINDINGS
    this.industries$ = this.industryService.entities$;
    this.classifications$ = this.classificationService.entities$;
    this.universities$ = this.universityService.entities$;
  }

  /**
   * Returns a map of the controls in this group.
   * @see {@link formGroup.controls}
   * */
  get fc() { return this.formGroup.controls; }

  /**
   * Maps the raw values of this {@link this.formGroup.getRawValue FormGroup} to an object (type Job).
   * */
  get rawFormValue(): Job {
    const values = this.formGroup.getRawValue();

    const data: Job = {
      ...values,
      applicationDeadline: values.applicationDeadline.toDate(),
      employmentEndDate: values.employmentEndDate != null ? values.employmentEndDate.toDate() : null,
      employmentStartDate: values.employmentStartDate.toDate(),
      graduationDate: values.graduationDate.toDate(),
      name: values.title,
      nlIndustryName: 'other', // todo: add in html
      skills: values.skills,
      ynTempAuthorizedWork: <boolean>values.ynTemporaryAuthorizedWork,
      ynWorkVisaSponsor: <boolean>values.ynWorkVisaSponsorship,
    };
    return data;
  }

  /**
   * Wrapper for `FormGroup.patch`. Patches the value of the FormGroup with param job
   * as the accepted object with control names as keys. It does its best to match the
   * values to the correct controls in the group.
   *
   * @param job The object that matches the structure of the group.
   * */
  set patchFormValue(job: Job) {
    this.skills = job.skills;

    const universityId = job.universityId;
    const schoolIds = job.schools.map(item => item.id);
    this.loadSchoolsByUniversityId(String(universityId));
    this.loadMajorsBySchoolId(schoolIds.join(',').split(','));

    this.onRemoteWorkChange(job.locRemoteWork);

    this.formGroup.patchValue({
      ...job,
      employmentEndDate: moment(job.employmentEndDate),
      employmentStartDate: moment(job.employmentStartDate),
      graduationDate: moment(job.graduationDate),
      applicationDeadline: moment(job.applicationDeadline),
      title: job.name,
      ynTemporaryAuthorizedWork: '' + job.ynTempAuthorizedWork,
      ynWorkVisaSponsorship: '' + job.ynWorkVisaSponsor,
      schools: schoolIds,
      majors: job.majors.map(item => item.id),
      classifications: job.classifications.map(item => item.id),
    });
  }

  ngOnInit(): void {
    // (1) initialize formGroup
    this.initFormGroup();

    this.classificationService.getAll();
    this.industryService.getAll();
    this.universityService.getAll();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  delete(btn): void { }

  cancel(btn): void { }

  name(item) {
    if (item && !item.hasOwnProperty('name')) {
      throw new Error(`${item} does not have property 'name'`);
    }

    return item.name;
  }

  onAddSkill({ input, value }: MatChipInputEvent): void {
    const fc = this.fc.skills;

    // push skill
    if ((value || '').trim()) {
      fc.setErrors(null);
      if (fc.valid) {
        this.skills.push(value.trim());
        this.fc.skills.markAsDirty();
      }
    } else {
      fc.updateValueAndValidity();
    }

    // reset the input value
    if (input) {
      input.value = '';
    }
  }

  onEditorChange( event ) {
    throw new Error('Method not implemented.');
  }

  onLocationChange(address: Address) {
    const { formatted_address } = address;
    const fc = this.fc.location;

    // set employmentLocation
    if (formatted_address) {
      fc.setValue(formatted_address);
    }
  }

  onRemoveSkill(skill): void {
    const fc = this.fc.skills;
    const index = this.skills.indexOf(skill);
    if (index >= 0) {
      this.skills.splice(index, 1);
    }
    fc.updateValueAndValidity();
    fc.markAsDirty();
  }

  onSchoolSelectionChange({ value }) {
    this.loadMajorsBySchoolId(value);
  }

  onSelectedMonth(normalizedMonth: Moment, datepicker: MatDatepicker<any>): void {
    const ctrlValue = this.fc.graduationDate.value;
    ctrlValue.month(normalizedMonth.month());
    this.fc.graduationDate.setValue(ctrlValue);
  }

  onSelectedYear(normalizedYear: Moment): void {
    const ctrlValue = this.fc.graduationDate.value;
    ctrlValue.year(normalizedYear.year());
    this.fc.graduationDate.setValue(ctrlValue);
  }

  onUniversityChange({ value }) {
    this.loadSchoolsByUniversityId(value);
  }

  onRemoteWorkChange(evt) {
    const remoteWork = this.locRemoteWorkChecked = !this.locRemoteWorkChecked;
    const location = this.fc.location;

    remoteWork ? location.clearValidators() : location.setValidators([ Validators.required ]);
    location.updateValueAndValidity({ onlySelf: true });
  }

  submit(btn?): void { }

  /**
   * Loads {@link this.majors$} by school id(s)
   * */
  private loadMajorsBySchoolId(id: string | string[]): void {
    this.majors$ = this.majorService.getWithQuery({ schoolId: id });
  }

  /**
   * Loads {@link this.schools$} by university id(s)
   * */
  private loadSchoolsByUniversityId(id: string | string[]): void {
    this.schools$ = this.schoolService.getWithQuery({ universityId: id });
  }

  /**
   * Initializes the `formGroup` with empty data; dates are set to the current date.
   * */
  private initFormGroup(): void {
    this.formGroup = this.formBuilder.group({
      recruiterId:  '',
      applicationDeadline: [ moment(), Validators.required ],
      classifications: [ '', Validators.required ],
      description: [ '', Validators.required ],
      employmentEndDate: [ moment()],
      employmentStartDate: [ moment(), Validators.required ],
      employmentType: [ '', Validators.required ],
      experience: [],
      graduationDate: [ moment(), Validators.required ],
      hiringGoal: [ '', Validators.required ],
      industryId: [ '', Validators.required ],
      location: [ '', Validators.required ],
      locRemoteWork: false,
      majors: [ '', Validators.required ],
      minGPA: [ null, [Validators.min(0), Validators.max(4)] ],
      salary: '',
      schools: [ '', Validators.required ],
      //skills: [ this.skills, CustomValidators.validateRequired ], // mat-chip is non-reactive so we bind to it via an array
      skills: [ this.skills ],
      title: [ '', Validators.required ],
      universityId: [ '', Validators.required ],
      ynTemporaryAuthorizedWork: [ '', Validators.required ],
      ynWorkVisaSponsorship: [ '', Validators.required ],
    });
  }
}
