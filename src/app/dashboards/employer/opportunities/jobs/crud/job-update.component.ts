import { Location } from '@angular/common';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Job } from '@api/generated';
import { JobBaseComponent } from '@dashboards/employer/opportunities/jobs/crud/job-base.component';
import { JobDataService, JobService } from '@store/services/entities/job.service';
import { takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-opportunities-jobs-update',
  templateUrl: './job-base.component.html',
  styleUrls: [ './job-base.component.scss' ]
})
export class JobUpdateComponent extends JobBaseComponent implements OnInit, OnDestroy {

  id: string;
  job: Job;

  constructor(
    protected injector: Injector,
    private location: Location,
    private jobService: JobService,
    private jobDataService: JobDataService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    super(injector);

    this.title = 'Update Job';
    this.submitButtonText = 'Update';
    this.showDeleteButton = true;
  }

  ngOnInit(): void {
    super.ngOnInit();

    // (1) initialize form
    this.jobDataService
      .getByParamId()
      .pipe(
        takeUntil(this.unsubscribe$),
        tap((job: Job) => {
          this.job = job;
          this.patchFormValue = job;
        })
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  delete(btn) {
    this.jobDataService
      .deleteByParamId()
      .pipe(
        takeUntil(this.unsubscribe$),
        tap(() => {
          this.jobService.removeOneFromCache(this.job);
          this.router.navigate([ '../..' ], { relativeTo: this.route });
        })
      )
      .subscribe();
  }

  cancel(btn) {
    this.location.back();
  }

  submit(btn) {
    // (1) set formIsSubmitted to true
    this.formIsSubmitted = true;

    // (2) ensure formGroup is valid
    if (this.formGroup.invalid) {
      throw Error(`invalid form`);
    }

    // (3) update job
    this.jobDataService
      .updateByParamId(this.rawFormValue)
      .pipe(
        takeUntil(this.unsubscribe$),
        tap((job) => {
          this.jobService.updateOneInCache(job);
          this.location.back();
        })
      )
      .subscribe();
  }
}
