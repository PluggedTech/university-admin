import { Location } from '@angular/common';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { JobBaseComponent } from '@dashboards/employer/opportunities/jobs/crud/job-base.component';
import { JobService } from '@store/services/entities/job.service';
import { take, tap } from 'rxjs/operators';
import moment, { Moment } from 'moment';

@Component({
  selector: 'app-opportunities-jobs-create',
  templateUrl: './job-base.component.html',
  styleUrls: [ './job-base.component.scss' ],
})
export class JobCreateComponent extends JobBaseComponent implements OnInit, OnDestroy {

  constructor(
    protected injector: Injector,
    private location: Location,
    private jobService: JobService,
  ) {
    super(injector);

    this.title = 'New Job';
    this.submitButtonText = 'Create';
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  cancel(btn) {
    this.location.back();
  }

  submit(btn) {
    // (1) set formIsSubmitted to true
    this.formIsSubmitted = true;

    // (2) ensure formGroup is valid
    if (this.formGroup.invalid) {
      throw Error(`[JobCreateComponent] Error - Invalid Form`);
    }
    btn.nativeElement.disabled = true;
    const recruiter = JSON.parse(localStorage.getItem('auth'))
    this.formGroup.controls.recruiterId.setValue(recruiter.user.id);

    // (3) create job
    this.jobService.add(this.rawFormValue)
      .pipe(
        take(1),
        tap(() => { this.location.back(); })
      )
      .subscribe(res => {
        btn.nativeElement.disabled = false;
      }, err =>     btn.nativeElement.disabled = false);
  }

  onEditorChange(evt) {
    this.formGroup.controls.description.setValue(evt.editor.getData());
    console.log(evt.editor.getData());
  }

  onEmploymentTypeChange(evt){
    const employmentType =  this.fc.employmentType.value;

    if(employmentType === 'FULL_TIME'){
      this.fc.employmentEndDate.setValue(null)
    }else{
      this.fc.employmentEndDate.setValue(moment())
    }
  }
}
