import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgxMaterialsModule } from '@app/ngx-materials.module';
import { ComponentsModule } from '@components/components.module';
import { ContentModule } from '@content/content.module';
import { JobBaseComponent } from '@dashboards/employer/opportunities/jobs/crud/job-base.component';
import { JobCreateComponent } from '@dashboards/employer/opportunities/jobs/crud/job-create.component';
import { JobReadComponent } from '@dashboards/employer/opportunities/jobs/crud/job-read/job-read.component';
import { JobUpdateComponent } from '@dashboards/employer/opportunities/jobs/crud/job-update.component';
import { JobsComponent } from '@dashboards/employer/opportunities/jobs/jobs.component';
import { DirectivesModule } from '@directives/directives.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PipesModule } from '@pipes/pipes.module';
import { NgJoinPipeModule, NgMapPipeModule, NgWherePipeModule } from 'angular-pipes';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MomentModule } from 'ngx-moment';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { JobsRoutingModule } from './jobs-routing.module';

@NgModule({
  declarations: [
    JobsComponent,
    JobBaseComponent,
    JobCreateComponent,
    JobReadComponent,
    JobUpdateComponent,
  ],
    imports: [
        CommonModule,
        JobsRoutingModule,
        // Others
        FontAwesomeModule,
        MomentModule,
        NgxMaterialsModule,
        PipesModule,
        ReactiveFormsModule,
        ContentModule,
        DirectivesModule,
        NgJoinPipeModule,
        NgMapPipeModule,
        GooglePlaceModule,
        MatSlideToggleModule,
        ComponentsModule,
        NgWherePipeModule,
        CKEditorModule,
        FormsModule
    ]
})
export class JobsModule {}
