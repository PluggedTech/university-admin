import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { JobService, InterviewScheduleService, InterviewSchedule, MatchService } from '@api/generated';
import { takeUntil } from 'rxjs/operators';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { ThemePalette } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-interview-schedule',
  templateUrl: './interview-schedule.component.html',
  styleUrls: ['./interview-schedule.component.scss']
})
export class InterviewScheduleComponent implements OnInit {

  unsubscribe$ = new Subject<void>();

  interviewForm : FormGroup;

  jobs : any;

  public disabled = false;
  public showSpinners = true;
  public showSeconds = false;
  public touchUi = false;
  public enableMeridian = true;
  public minDate: Date;
  public maxDate: Date;
  public stepHour = 1;
  public stepMinute = 1;
  public color: ThemePalette = 'primary';
  public disableMinute = false;
  public hideTime = false;

  public options = [
    { value: true, label: 'True' },
    { value: false, label: 'False' }
  ];

  public listColors = ['primary', 'accent', 'warn'];

  public stepHours = [1, 2, 3, 4, 5];
  public stepMinutes = [1, 5, 10, 15, 20, 25];
  public stepSeconds = [1, 5, 10, 15, 20, 25];

  private _jobs$ = new BehaviorSubject<any>('');
  get jobs$(): Observable<any> { return this._jobs$.asObservable(); }

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData : any,
    private fb : FormBuilder, 
    private jobService : JobService, 
    private matchService : MatchService, 
    private interviewScheduleService: InterviewScheduleService,
    private http : HttpClient,
    private dialogRef : MatDialogRef<InterviewScheduleComponent>
    ) { 
    this.interviewForm = fb.group({
      job: [''],
      recruiterId: [''],
      studentId: [''],
      status: ['pending'],
      date1: [''],
      time1: [''],
      date2 : [''],
      time2: ['']
    })

    console.log(this.dialogData)
  }

  async ngOnInit() {
    const match : any = await this.http.get(
      `${environment.apiUrl}/api/match?job[recruiterId]=${this.dialogData.recruiterId}&student[id]=${this.dialogData.studentId}`
      ).toPromise();

    this._jobs$.next(match.matchList.map(match => {
      return match.job;
    }))
  }

  onSubmit(){
    this.interviewForm.controls.recruiterId.setValue(this.dialogData.recruiterId)
    this.interviewForm.controls.studentId.setValue(this.dialogData.studentId)
    this.interviewScheduleService.apiInterviewSchedulePost(this.interviewForm.value)
    .subscribe((res) => {
      console.log(res)
      this.interviewForm.reset();
      this.dialogRef.close();
    }, err => {
      console.log(err)
      this.interviewForm.reset();
      this.dialogRef.close();
    })
  }

}
