import { Component, OnInit } from '@angular/core';
import { MatDialog,  MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { JobService, MatchService, Recruiter, StudentService, MatchEventService } from '@api/generated';
import { CONSTANTS, QBConfig } from '@app/QBConfig';
import { QBHelper } from '@app/qbHelper';
import { MessageNotificationService } from '@services/message-notification.service';
import moment from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { InterviewScheduleComponent } from './interview-schedule/interview-schedule.component';
import { map } from 'rxjs/operators';
import { UserChatService } from '@app/_services/user-chat.service';
import { AuthService } from '@app/_services/auth.service';

declare var QB: any;

interface Profile {
  firstName: string,
  lastName: string
}

interface QBCredential {
  qbId: number;
  qbLogin: string;
  qbFullName: string;
  qbPassword: string;
}

interface ChatDialog {
  id: string;
  name: string;
  type: number;
  last_message: string;
  occupantIds: Array<number>;
  unreadMessagesCount: number;
  createdAt: string;
  updatedAt: string;
}

interface Message {
  senderId: number;
  recipientId: number;
  body: string;
  createdAt: string;
  _id: string;
  attachments: Array<any>;
  created_at: string;
  date_sent: string;
  delivered_ids: Array<number>;
  message: string;
  read_ids: Array<number>;
  sender_id: number;
  chat_dialog_id: string;
  selfReaded: boolean;
  read: number;
}

@Component({
  selector: 'app-opportunities-chat',
  templateUrl: './chat.component.html',
  styleUrls: [ './chat.component.scss' ]
})
export class ChatComponent implements OnInit {

  DEFAULT_AVATAR= 'assets/images/user/user.png';

  auth:any;
  jobId: any;
  eventId: any;
  studentProfileId: any;
  recruiter: any;
  recruiterProfile: any;
  currentUser: any;
  currentUserId: number;
  dialogId: any;
  message: string;
  occupantIds: number[];
  recipientId: number;
  recipientName = '';
  unread = 0;
  attachments: any = [];
  sentMessage: any;
  dialogRef: MatDialogRef<any>;

  qbCredential$ : Observable<QBCredential>
  profile$ : Observable<Profile>

  private chatDialogs: ChatDialog[] = [];
  private storeMessages: Message[] = [];

  constructor(
    private qbHelper: QBHelper,
    private messageNotification: MessageNotificationService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private jobService: JobService,
    private matchService: MatchService,
    private matchEventService: MatchEventService,
    private studentService: StudentService,
    private userChatService: UserChatService,
    private authService: AuthService,
  ) {
    const auth = JSON.parse(localStorage.getItem('auth'));
    const currentUserQBId = auth.user.qbCredential.id;  

    this.currentUserId = currentUserQBId;
    this.recruiter = auth.user;

    this.recruiterProfile = auth.user.profilePicture != "" ? auth.user.profilePicture : this.DEFAULT_AVATAR;


    QB.chat.onMessageListener = this.onMessageListener.bind(this);
    QB.chat.onSentMessageCallback = this.onSentMessageCallback.bind(this);
  }

  private _messages$ = new BehaviorSubject<Message[]>([]);
  get messages$(): Observable<Message[]> { return this._messages$.asObservable(); }

  private _chatDialogs$ = new BehaviorSubject<ChatDialog[]>([]);
  get chatDialogs$(): Observable<ChatDialog[]> { return this._chatDialogs$.asObservable(); }

  private _match$ = new BehaviorSubject<any>('');
  get match$(): Observable<any> { return this._match$.asObservable(); }

  private _student$ = new BehaviorSubject<any>('');
  get student$(): Observable<any> { return this._student$.asObservable(); }

  async ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem('auth')); 
    this.jobId = this.route.snapshot.paramMap.get('jobid');
    this.eventId = this.route.snapshot.paramMap.get('eventid');
    this.studentProfileId = this.route.snapshot.paramMap.get('studentid');

    this.loadDialogs();
  }

  customTB(index, item) { return `${index}-${item._id}`;
  }

  fillNewMessageParams(userId, msg) {
    const self = this,
      message = {
        _id: msg.id,
        attachments: [],
        created_at: +msg.extension.date_sent || Date.now(),
        date_sent: +msg.extension.date_sent || Date.now(),
        delivered_ids: [ userId ],
        message: msg.body,
        read_ids: [ userId ],
        sender_id: userId,
        chat_dialog_id: msg.extension.dialog_id,
        selfReaded: userId === this.currentUserId,
        read: 0
      };

    if (msg.extension.attachments) {
      message.attachments = msg.extension.attachments;
    }

    if (msg.extension.notification_type) {
      message[ 'notification_type' ] = msg.extension.notification_type;
    }

    if (msg.extension.new_occupants_ids) {
      message[ 'new_occupants_ids' ] = msg.extension.new_occupants_ids;
    }

    message[ 'status' ] = (userId !== this.currentUser) ? self.getMessageStatus(message) : undefined;

    return message;
  }

  getMessageStatus(message) {
    if (message.sender_id !== this.currentUserId) {
      return undefined;
    }
    const
      self = this,
      deleveredToOcuupants = message.delivered_ids.some(function(id) {
        return id !== self.currentUserId;
      }),
      readedByOccupants = message.read_ids.some(function(id) {
        return id !== self.currentUserId;
      });
    return !deleveredToOcuupants ? 'sent' :
      readedByOccupants ? 'read' : 'delivered';
  }

  /**
   * Load dialog messages
   * @param dialogId The id of the dialog to be loaded
   * @param occupantIds The ids corresponding to the occupants of the dialog
   * */
  async loadMessages(dialogId: number, occupantIds: number[], recipientName: string) {
    const auth = JSON.parse(localStorage.getItem('auth'));
    const currentUserQBId = auth.user.qbCredential.id;  

    this.occupantIds = occupantIds;
    this.recipientName = recipientName;
    const index = occupantIds.indexOf(currentUserQBId);

    if (index > -1) {
      occupantIds.splice(index, 1);
    }


    this.recipientId = occupantIds[ 0 ];
    this.dialogId = dialogId;

    const params = {
      chat_dialog_id: dialogId,
      sort_asc: 'date_sent',
      limit: 200,
      mark_as_read: 0
    };

    const QBUser = await this.userChatService.getQBUser(this.recipientId) 
    const QBCustomData = JSON.parse(QBUser.custom_data);
    const findStudent = await this.studentService.apiStudentIdGet(QBCustomData.userAppId).toPromise();

    this._student$.next(findStudent.student)

    QB.chat.message.list(params, (err, res) => {
      if (err) {
        return console.error('[ChatComponent] Error - ', err);
      }

      this.storeMessages = res.items.map(message => {
        if (message.message === '[attachment]') {
          if (message.attachments.length > 0) {
            message.attachments.forEach(attachment => {
              attachment.src = QB.content.privateUrl(attachment.id);
            });
          }
        }

        return <Message>{
          senderId: message.sender_id,
          body: message.message,
          createdAt: moment().toString(),
          _id: message._id,
          attachments: message.attachments,
          created_at: moment().toString(),
          date_sent: moment().toString(),
          delivered_ids: message.delivered_ids,
          message: message.message,
          read_ids: message.read_ids,
          sender_id: message.sender_id,
          read: message.read
        };
      });
      this._messages$.next([ ...this.storeMessages ]);
    });
  }

  sendMessage(msg, attachments: any = false) {
    const message = {
      type: 'chat',
      body: msg,
      extension: {
        save_to_history: 1,
        dialog_id: this.dialogId
      },
      markable: 1
    };

    if (attachments) {
      message.extension[ 'attachments' ] = attachments;
    }

    this.sentMessage = message;
    QB.chat.send(this.recipientId, message);

    this.message = '';

  }

  onSentMessageCallback(messageLost, messageSent) {

    if (this.sentMessage.extension.attachments !== undefined) {
      if (this.sentMessage.extension.attachments.length > 0) {
        this.sentMessage.extension.attachments.forEach(attachment => {
          attachment.src = QB.content.privateUrl(attachment.id);
        });
      }
    }

    this.storeMessages.push(<Message>{
      senderId: this.currentUserId,
      recipientId: this.recipientId,
      body: this.sentMessage.body,
      createdAt: moment().toString(),
      _id: this.sentMessage.id,
      attachments: this.sentMessage.extension.attachments,
      created_at: moment().toString(),
      date_sent: moment().toString(),
    });

    this._messages$.next([ ...this.storeMessages ]);
  }

  onMessageListener(userId, msg) {
    this.setRecepientChat(userId, msg);
  }

  setRecepientChat(userId, msg) {
    const message = this.fillNewMessageParams(userId, msg);

    if (message.message === '[attachment]') {
      if (message.attachments.length > 0) {
        message.attachments.forEach(attachment => {
          attachment.src = QB.content.privateUrl(attachment.id);
        });
      }
    }

    this.storeMessages.push(<Message>{
      senderId: message.sender_id,
      body: message.message,
      createdAt: moment().toString(),
      _id: message._id,
      attachments: message.attachments,
      created_at: moment().toString(),
      date_sent: moment().toString(),
      delivered_ids: message.delivered_ids,
      message: message.message,
      read_ids: message.read_ids,
      sender_id: message.sender_id,
      selfReaded: userId === this.currentUserId,
      read: message.read
    });

    this._messages$.next([ ...this.storeMessages ]);
  }

  checkUnreadCount(dialogId) {
    QB.chat.message.unreadCount(dialogId, (err, result) => {
      if (err) {
        console.log(err);
      }

      this.messageNotification.updateUnreadMessages(result);
    });
  }

  markRead() {
    const element = document.querySelector('#chatbox');
    element.addEventListener('scroll', () => {
      const position = element.getBoundingClientRect();

      // checking whether fully visible
      if (position.top >= 0 && position.bottom <= window.innerHeight) {
        console.log('Element is fully visible in screen');
      }

      // checking for partial visibility
      if (position.top < window.innerHeight && position.bottom >= 0) {
        console.log('Element is partially visible in screen');
      }

    });
  }

  loadImagesEvent(e) {
    let img: any, container: Element, imgPos: number, scrollHeight: number;
    img = e.target;
    container = document.querySelector('#chatbox');
    // @ts-ignore
    imgPos = container.offsetHeight + container.scrollTop - img.offsetTop;
    scrollHeight = container.scrollTop + img.offsetHeight;

    img.classList.add('loaded');

    if (imgPos >= 0) {
      container.scrollTop = scrollHeight + 5;
    }
  }

  prepareToUpload(e) {
    const self = this,
      files = e.currentTarget.files,
      dialogId = this.dialogId;
    for (let i = 0; i < files.length; i++) {
      self.uploadFilesAndGetIds(files[ i ], dialogId);
    }
    e.currentTarget.value = null;
  }

  uploadFilesAndGetIds(file, dialogId) {
    if (file.size >= CONSTANTS.ATTACHMENT.MAXSIZE) {
      return alert(CONSTANTS.ATTACHMENT.MAXSIZEMESSAGE);
    }
    this.attachments = [ {
      id: 'isLoading',
      src: URL.createObjectURL(file)
    } ];
    const self = this;
    this.qbHelper.abCreateAndUpload(file)
      .then(response => {
        self.attachments = [];
        const attachments = [ { id: response.uid, type: CONSTANTS.ATTACHMENT.TYPE } ];
        self.sendMessage(CONSTANTS.ATTACHMENT.BODY, attachments);
      }).catch(err => {
      self.attachments = [];
      alert('ERROR: ' + err.detail);
    });
  }

  openDialog(event: MouseEvent, studentId : Number, recruiterId : Number) {
    this.dialogRef = this.dialog.open(InterviewScheduleComponent, {
      panelClass: 'mat-dialog-auth-panel',
      backdropClass: 'mat-dialog-auth-backdrop',
      data : {
        studentId : studentId,
        recruiterId : recruiterId
      }
    });
  }

  private createChatSession(credential) {
    const userCredential = {
      login: credential.login, 
      password: credential.password,
    };

    this.qbHelper
      .qbCreateConnection(userCredential)
      .then(user => {
        this.currentUser = user;
        this.currentUserId = user.id;
        this.setConnection(user.id, userCredential.password);
      })
      .catch(err => console.error('[ChatComponent] Create Connection Error', err));
  }

  private setConnection(userId, password) {
    this.qbHelper
      .qbChatConnection(userId, password)
      .then(() => {
        this.loadDialogs();
      })
      .catch(err => console.error('[ChatComponent] Set Connection Error', err));
  }

  private loadDialogs() {
    const filter = {
      'limit': 200,
      'sort_desc': 'updated_at',
      'type': {
        'in': [ 3, 2 ]
      }
    };

    QB.chat.dialog.list(filter, (err, res) => {
      if (err) {
        return console.error('[ChatComponent] Error - ', err);
      }

      this.chatDialogs = res.items.map(item => {
        return <ChatDialog>{
          createdAt: item.created_at,
          id: item._id,
          name: item.name,
          last_message: item.last_message,
          occupantIds: item.occupants_ids,
          type: item.type,
          unreadMessagesCount: item.unread_messages_count,
          updatedAt: item.updated_at,
        };
      });
      this._chatDialogs$.next([ ...this.chatDialogs ]);
    });

    this.matchedDialog();
  }

  private createDialog(partnerId){
    const params = {
      type: 3,
      occupants_ids : [partnerId]
    }

    QB.chat.dialog.create(params, (error, dialog) => {
      if(error) console.log(error)

      this.chatDialogs.push(dialog)
      this._chatDialogs$.next([ ...this.chatDialogs ]);
    })
  }
    
  private matchedDialog(){
    this.match$.subscribe(data => {
      if(data){
        this.createDialog(data.student.qbCredential.id)
      }
    }, error => {
      console.log(error)
    })
  }

}
