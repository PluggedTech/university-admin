import { Location } from '@angular/common';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Event } from '@api/generated';
import { EventBaseComponent } from '@dashboards/employer/opportunities/events/crud/event-base.component';
import { EventDataService, EventService } from '@store/services/entities/event.service';
import { take, tap } from 'rxjs/operators';

@Component({
  selector: 'app-events-update',
  templateUrl: './event-base.component.html',
  styleUrls: [ './event-base.component.scss' ]
})
export class EventUpdateComponent extends EventBaseComponent implements OnInit, OnDestroy {

  event: Event;

  constructor(
    protected injector: Injector,
    private eventDataService: EventDataService,
    private eventService: EventService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    super(injector);

    this.headerTitle = 'Update Event';
    this.submitButtonText = 'Update';
    this.showDeleteButton = true;
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  ngOnInit(): void {
    super.ngOnInit();

    // (1) initialize form
    this.patchFormValue = this.eventDataService.getByParamId().pipe(tap(event => this.event = event));
  }

  onCancelButtonClick(btn) {
    this.location.back();
  }

  onDeleteButtonClick(btn) {
    this.eventDataService
      .deleteByParamId()
      .pipe(
        take(1),
        tap(event => {
          this.eventService.removeOneFromCache(event);
          this.router.navigate([ '../..' ], { relativeTo: this.route });
        })
      )
      .subscribe();
  }

  onSubmitButtonClick(btn) {
    // (1) check if the form is valid
    if (this.formGroup.invalid) {
      throw Error(`[EventUpdateComponent] Error - Unable to submit! The form is invalid.`);
    }

    // (3) update event
    this.eventDataService
      .updateByParamId(this.rawFormValue)
      .pipe(
        take(1),
        tap((event) => {
          this.eventService.updateOneInCache(event);
          this.location.back();
        })
      )
      .subscribe();
  }

}
