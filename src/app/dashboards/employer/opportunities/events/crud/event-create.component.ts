import { AlertService } from './../../../../../_services/alert.service';
import { Location } from '@angular/common';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { EventBaseComponent } from '@dashboards/employer/opportunities/events/crud/event-base.component';
import { EventDataService, EventService } from '@store/services/entities/event.service';
import { takeUntil, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Component({
  templateUrl: './event-base.component.html',
  styleUrls: ['./event-base.component.scss'],
})
export class EventCreateComponent extends EventBaseComponent implements OnInit, OnDestroy {

  constructor(
    protected injector: Injector,
    private eventService: EventService,
    private eventDataService: EventDataService,
    private location: Location,
    private http: HttpClient,
    private alert: AlertService
  ) {
    super(injector);

    this.headerTitle = 'New Event';
    this.submitButtonText = 'Submit';
    this.cancelButtonText = 'Cancel';
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  async onSubmitButtonClick(btn) {
    // (1) check if form is valid
    if (this.formGroup.invalid) {
      throw new Error('[EventCreateComponent] Error - Unable to submit! The form is invalid.');
    }
    btn.nativeElement.disabled = true;
    const recruiter = JSON.parse(localStorage.getItem('auth'))
    this.formGroup.controls.recruiterId.setValue(recruiter.user.id);


    // (2) submit the form
    this.eventDataService
      .add(this.rawFormValue)
      .pipe(
        takeUntil(this.unsubscribe$),
        tap(async (event) => {
          const eventEmailStatus = await this.http.get(`${environment.apiUrl}/event/${event.id}/sendApprovalEmail`).toPromise();
          this.eventService.addOneToCache(event);
          this.location.back();
        })).subscribe(res => {
          btn.nativeElement.disabled = false;
          const alert = this.alert.showAlert({
            html: 'Event has been submitted and pending Approval from CCE. Please check your email for notification of approval',
            position: 'top-right'
          });
          setTimeout(() => {
            this.alert.closeAlert(alert);
          }, 5000);
        }, err => {
          btn.nativeElement.disabled = false;
        });
  }

  onEditorChange(evt) {
    this.formGroup.controls.description.setValue(evt.editor.getData());
    console.log(evt.editor.getData());
  }

  onCancelButtonClick(btn) {
    this.location.back();
  }
}
