import { Location } from '@angular/common';
import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Event } from '@api/generated';
import { EventBaseComponent } from '@dashboards/employer/opportunities/events/crud/event-base.component';
import { EventDataService } from '@store/services/entities/event.service';
import { Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-event-read',
  templateUrl: './event-read.component.html',
  styleUrls: [ '../event-base.component.scss' ]
})
export class EventReadComponent extends EventBaseComponent implements OnInit {

  private _university$ = new BehaviorSubject<any>('');
  get university$(): Observable<any> { return this._university$.asObservable(); }

  private _permittedEventType$ = new BehaviorSubject<any>('');
  get permittedEventType$(): Observable<any> { return this._permittedEventType$.asObservable(); }

  event$: Observable<Event>;

  constructor(
    protected injector: Injector,
    private eventDataService: EventDataService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.event$ = this.eventDataService.getByParamId();

    this.event$.subscribe(data => {
      this.uniService.apiUniversityIdGet(data.schools[0].universityId)
      .subscribe(uni => {
        this._university$.next(uni.university)
      }, error => {
        console.log(error)
      })

      this.permittedService.apiPermittedEventTypeIdGet(data.eventTimeslot.permittedEventTypeId)
      .subscribe(permitted => {
        this._permittedEventType$.next(permitted.permittedEventType)
      }, error => {
        console.log(error)
      })

    }, error => {
      console.log(error)
    })

  }

  onCancelButtonClick(): void {
    this.location.back();
  }

  async onSubmitButtonClick(): Promise<void> {
    await this.router.navigate([ './update' ], { relativeTo: this.route });
  }
}
