import { Component, Injector, OnDestroy, OnInit, Pipe, PipeTransform, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSelectionList, MatSelectionListChange } from '@angular/material/list';
import {
  CampusPartner,
  Classification,
  Contact,
  Event,
  Major,
  PermittedEventTimeslot,
  PermittedEventType,
  School,
  University,
  UniversityService as UniService,
  ContactService as ContactServiceAPI,
  PermittedEventTypeService as PermittedService
} from '@api/generated';
import { ContactFieldComponent } from '@components/contact/contact-field/contact-field.component';
import { SimpleFormControls } from '@components/simple-form/simple-form-controls.abstract';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { CampusPartnerService } from '@store/services/entities/campus-partner.service';
import { ClassificationService } from '@store/services/entities/classification.service';
import { ContactService } from '@store/services/entities/contact.service';
import { MajorService } from '@store/services/entities/major.service';
import { PermittedEventTypeService } from '@store/services/entities/permittedEventType.service';
import { SchoolService } from '@store/services/entities/schools.service';
import { UniversityService } from '@store/services/entities/university.service';
import { Moment } from 'moment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import PartnerTypeEnum = CampusPartner.PartnerTypeEnum;

@Pipe({
  name: 'cpFilter'
})
export class CpFilterPipe implements PipeTransform {
  transform(array: CampusPartner[], type: string): CampusPartner[] {
    if (array == null || type == null) { return array; }
    return array.filter(item => item.partnerType === type);
  }
}

@Pipe({
  name: 'cpSearch'
})
export class CpSearchPipe implements PipeTransform {
  transform(array: CampusPartner[], search: string): CampusPartner[] {
    if (array == null || search == null) { return array; }
    return array.filter(value => value.name.toLowerCase().indexOf(search.toLowerCase()) !== -1);
  }
}

@Component({
  template: ``,
})
export class EventBaseComponent implements SimpleFormControls, OnInit, OnDestroy {

  @ViewChild('mslCEP') mslCEP: MatSelectionList;
  @ViewChild('primaryContact') primaryContact: ContactFieldComponent;
  @ViewChild('secondaryContact') secondaryContact: ContactFieldComponent;

  Editor = ClassicEditor;
  editorModel = {
    editorData: ''
};
  formGroup: FormGroup;
  cepSelection = [];

  events = [];
  icons = {
    faCalendar: faCalendar
  };
  campusPartnerTypes = new Map<string, string>([
    [ PartnerTypeEnum.FRATERNITYSORORITYORSIMILAR, 'Fraternity, Sorority or Similar' ],
    [ PartnerTypeEnum.SUPPORTANDLEADERSHIP, 'Support & Leadership' ],
    [ PartnerTypeEnum.INDUSTRYFOCUSED, 'Industry Focused' ],
    [ PartnerTypeEnum.ACADEMICDEPARTMENT, 'Academic Department' ],
  ]);
  campusPartners$: Observable<CampusPartner[]>;
  cancelButtonText = 'Cancel';
  headerTitle: string;
  showDeleteButton = false;
  submitButtonText = 'Submit';
  eventTimeslots$ = new BehaviorSubject<PermittedEventTimeslot[]>([]);
  universities$: Observable<University[]>;
  schools$: Observable<School[]>;
  majors$: Observable<Major[]>;
  classifications$: Observable<Classification[]>;
  contacts$: Observable<Contact[]>;
  eventTypes$: Observable<PermittedEventType[]>;
  eventDateMonthSelected = [ false, false, false ];
  selectedEventTimeslot: PermittedEventTimeslot;
  protected unsubscribe$ = new Subject<void>();
  // INJECTORS
  private formBuilder: FormBuilder;
  // INJECTORS: API SERVICES
  private universityService: UniversityService;
  public uniService: UniService;
  public permittedService: PermittedService;
  private schoolService: SchoolService;
  private majorService: MajorService;
  private classificationService: ClassificationService;
  private campusPartnerService: CampusPartnerService;
  private contactService: ContactService;
  private contactServiceAPI: ContactServiceAPI;
  private eventTypeService: PermittedEventTypeService;

  constructor(
    protected injector: Injector,
  ) {
    // INSTANTIATE INJECTORS
    this.formBuilder = this.injector.get(FormBuilder);
    // INSTANTIATE INJECTORS - API SERVICES
    this.uniService = this.injector.get(UniService);
    this.permittedService = this.injector.get(PermittedService);
    this.universityService = this.injector.get(UniversityService);
    this.schoolService = this.injector.get(SchoolService);
    this.majorService = this.injector.get(MajorService);
    this.classificationService = this.injector.get(ClassificationService);
    this.campusPartnerService = this.injector.get(CampusPartnerService);
    this.contactService = this.injector.get(ContactService);
    this.contactServiceAPI = this.injector.get(ContactServiceAPI);
    this.eventTypeService = this.injector.get(PermittedEventTypeService);

    this.universities$ = this.universityService.entities$;
    this.classifications$ = this.classificationService.entities$;
    this.campusPartners$ = this.campusPartnerService.entities$
      .pipe(
        map(items => {
          return items.map(item => {
            return { ...item, selected: false };
          });
        })
      );

    const auth = JSON.parse(localStorage.getItem('auth'))
    this.contacts$ = this.contactServiceAPI.apiContactQueryPost({ companyId : Number(auth.user.company.id) })
    .pipe(
      map(contacts => {
        return contacts.contactList.map(contact => {
          return contact
        }) 
      })
    )


    //this.contactService.entities$;
    this.eventTypes$ = this.eventTypeService.entities$;
  }

  /**
   * Returns a map of the controls in this group.
   * @see {@link formGroup.controls}
   * */
  get fc() { return this.formGroup.controls; }

  /**
   * Maps the raw values of this {@link this.formGroup.getRawValue FormGroup} to an object (type Job).
   * */
  get rawFormValue(): Event {
    const values = this.formGroup.getRawValue();

    const other = {
      eventTypeId: values.type.id,
      eventTimeslotId: values.timeslot.id,
      campusPartners: values.campusPartners.map(item => item.id)
    };

    delete values.timeslot;
    delete values.type;

    return <Event>{
      ...values,
      ...other
    };
  }

  /**
   * Wrapper for `FormGroup.patch`. Patches the value of the FormGroup with param job
   * as the accepted object with control names as keys. It does its best to match the
   * values to the correct controls in the group.
   *
   * @param event The object that matches the structure of the group.
   * */
  set patchFormValue(event: Observable<Event>) {
    console.log(event)
    event
      .pipe(
        take(1),
        concatMap(res => this.eventTypes$.pipe(map(arr => {
            const type = arr.find(e => e.id === res.eventTimeslot.permittedEventTypeId);
            this.eventTimeslots$.next([ ...type.permittedEventTimeslots ]);
            return { ...res, type };
          }))
        ),
        concatMap(res => this.eventTimeslots$.pipe(map(arr => {
          const timeslot = arr.find(e => e.id === res.eventTimeslotId);
          this.selectedEventTimeslot = timeslot;
          return { ...res, timeslot };
        })))
      )
      .subscribe(res => {
        this.primaryContact.fill(res.primaryContact);
        this.secondaryContact.fill(res.secondaryContact);

        this.loadSchoolsByUniversityId(res.schools.map(e => e.universityId + ''));
        this.loadMajorsBySchoolId(res.schools.map(e => e.id + ''));

        res.campusPartners.forEach(item => this.toggleCEPSelection({ ...item, selected: false }));

        this.cepSelection.forEach(selection => {
          const option = this.mslCEP.options.find(item => item.value.name === selection.name);
          this.mslCEP.selectedOptions.select(option);
        });

        delete res.campusPartners;

        this.formGroup.patchValue({
          ...res,
          universities: res.schools.map(e => e.universityId),
          schools: res.schools.map(e => e.id),
          majors: res.majors.map(e => e.id),
          classifications: res.classifications.map(e => e.id),
        });
      });
  }

  onCancelButtonClick(btn?): void {
    throw new Error('Method not implemented.');
  }

  onDeleteButtonClick(btn?): void {
    throw new Error('Method not implemented.');
  }

  onHeaderIconClick(): void {
    throw new Error('Method not implemented.');
  }

  onSubmitButtonClick(btn?): void {
    throw new Error('Method not implemented.');
  }

  onEditorChange( event ) {
    throw new Error('Method not implemented.');
}

  ngOnInit(): void {
    // (1) initialize formGroup
    this.initFormGroup();

    this.universityService.getAll();
    this.classificationService.getAll();
    this.campusPartnerService.getAll();
    this.contactService.getAll();
    this.eventTypeService.getAll();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onCEPSelectionChange(event: MatSelectionListChange) {
    this.toggleCEPSelection(event.option.value);
  }

  onCEPRemoved(name) {
    this.toggleCEPSelection(name);
  }

  onUniversityChange({ value }) {
    this.loadSchoolsByUniversityId(value);
  }

  onSchoolSelectionChange({ value }) {
    this.loadMajorsBySchoolId(value);
    this.loadEventTypesBySchoolId(value);
  }

  onEventTypeSelectionChange({ value }) {
    const timeslots = (value as PermittedEventType).permittedEventTimeslots;
    this.eventTimeslots$.next([ ...timeslots ]);
  }

  onEventTimeslotSelectionChange({ value }) {
    this.selectedEventTimeslot = { ...value };
  }

  setEventDateMonthSelected(index: number, value: boolean) {
    this.eventDateMonthSelected[ index ] = value;
  }

  onContactSelected(contact: Contact, type: 'PRIMARY' | 'SECONDARY'): void {
    if (type === 'PRIMARY') { this.fc.primaryContactId.setValue(contact.id); }
    if (type === 'SECONDARY') { this.fc.secondaryContactId.setValue(contact.id); }

    const auth = JSON.parse(localStorage.getItem('auth'))
    this.contacts$ = this.contactServiceAPI.apiContactQueryPost({ companyId : Number(auth.user.company.id) })
    .pipe(
      map(contacts => {
        return contacts.contactList.map(contact => {
          return contact
        }) 
      })
    )
  }

  uniName(id){
    this.uniService.apiUniversityIdGet(id)
    .subscribe(university => {
      console.log(university)
    }, error => {
      console.log(error)
    })
  }

  timeslot(timeslot: PermittedEventTimeslot): string {
    const { startTime, endTime, timezone } = timeslot;
    return `${this.timeTransform(startTime)} - ${this.timeTransform(endTime)} ${timezone}`;
  }

  timeslotFilter(index: number): object {
    const self = this;
    return function(date: Moment) {
      if (self.eventDateMonthSelected[ index ]) {
        const { days } = self.selectedEventTimeslot;
        const dd = date.format('dd');
        return days.includes(dd);
      }

      return true;
    };
  }

  private timeTransform(time : any): any {
    let hour = (time.split(':'))[0]
    let min = (time.split(':'))[1]
    let part = hour > 12 ? 'pm' : 'am';
    min = (min+'').length == 1 ? `0${min}` : min;
    hour = hour > 12 ? hour - 12 : hour;
    hour = (hour+'').length == 1 ? `0${hour}` : hour;

    return `${hour}:${min} ${part}`
  }

  private toggleCEPSelection(item: any): void {
    const index = this.cepSelection.findIndex(mItem => mItem.id === item.id);
    if (index === -1) {
      item.selected = true;
      this.cepSelection.push(item);
    } else {
      item.selected = false;
      this.cepSelection.splice(index, 1);
    }

    this.formGroup.patchValue({
      campusPartners: this.cepSelection
    });
  }

  /**
   * Loads {@link this.majors$} by school id(s)
   * */
  private loadMajorsBySchoolId(id: string | string[]): void {
    this.majors$ = this.majorService.getWithQuery({ schoolId: id });
  }

  /**
   * Loads {@link this.schools$} by university id(s)
   * */
  private loadSchoolsByUniversityId(id: string | string[]): void {
    this.schools$ = this.schoolService.getWithQuery({ universityId: id });
  }

  /**
   * Loads {@link this.eventTypes$} by school id(s)
   * */
  private loadEventTypesBySchoolId(id: string | string[]): void {
    this.eventTypes$ = this.eventTypeService.getWithQuery({ schoolId: id });
  }

  /**
   * Initializes the `formGroup` with empty data; dates are set to the current date.
   * */
  private initFormGroup(): void {
    this.formGroup = this.formBuilder.group({
      recruiterId: '',
      campusPartners: [ this.cepSelection, Validators.required ],
      meetingInviteLink: [''],
      classifications: [ '', Validators.required ],
      description: [ '', Validators.required ],
      majors: [ '', Validators.required ],
      schools: [ '', Validators.required ],
      title: [ '', Validators.required ],
      universities: [ '', Validators.required ],
      primaryContactId: [ '', Validators.required ],
      secondaryContactId: [ '', Validators.required ],
      type: [ '', Validators.required ],
      timeslot: [ '', Validators.required ],
      firstChoiceDate: [ { value: '', disabled: true }, Validators.required ],
      secondChoiceDate: [ { value: '', disabled: true } ],
      thirdChoiceDate: [ { value: '', disabled: true } ],
    });
  }
}

