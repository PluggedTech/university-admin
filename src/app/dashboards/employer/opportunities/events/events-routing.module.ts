import { StudentProfileComponent } from '@dashboards/employer/opportunities/student-profile';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventReadComponent } from '@dashboards/employer/opportunities/events/crud/event-read/event-read.component';
import { EventCreateComponent } from '@dashboards/employer/opportunities/events/crud/event-create.component';
import { EventUpdateComponent } from '@dashboards/employer/opportunities/events/crud/event-update.component';
import { EventsComponent } from '@dashboards/employer/opportunities/events/events.component';

const routes: Routes = [
  {
    path: 'events',
    children: [
      { path: '', component: EventsComponent },
      { path: 'create', component: EventCreateComponent },
      { path: ':id/update', component: EventUpdateComponent },
      { path: ':id', component: EventReadComponent },
      { path: ':id/student-profile/:studentid', component: StudentProfileComponent },

    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class EventsRoutingModule {}
