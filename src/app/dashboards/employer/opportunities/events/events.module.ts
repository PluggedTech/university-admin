import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxMaterialsModule } from '@app/ngx-materials.module';
import { ComponentsModule } from '@components/components.module';
import { ContactModule } from '@components/contact/contact.module';
import { ContentModule } from '@content/content.module';
import { CpFilterPipe, CpSearchPipe, EventBaseComponent, } from '@dashboards/employer/opportunities/events/crud/event-base.component';
import { EventCreateComponent } from '@dashboards/employer/opportunities/events/crud/event-create.component';
import { EventReadComponent } from '@dashboards/employer/opportunities/events/crud/event-read/event-read.component';
import { EventUpdateComponent } from '@dashboards/employer/opportunities/events/crud/event-update.component';
import { EventsRoutingModule } from '@dashboards/employer/opportunities/events/events-routing.module';
import { EventsComponent } from '@dashboards/employer/opportunities/events/events.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgJoinPipeModule, NgMapPipeModule, NgWherePipeModule } from 'angular-pipes';
import { MomentModule } from 'ngx-moment';
import { EventsApplicationComponent } from './events-application/events-application.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';


@NgModule({
  declarations: [
    CpFilterPipe,
    CpSearchPipe,
    EventBaseComponent,
    EventCreateComponent,
    EventReadComponent,
    EventsComponent,
    EventUpdateComponent,
    EventsApplicationComponent,
  ],
  imports: [
    CommonModule,
    CKEditorModule,
    EventsRoutingModule,
    NgxMaterialsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule,
    ContentModule,
    NgMapPipeModule,
    NgJoinPipeModule,
    MomentModule,
    ContactModule,
    NgWherePipeModule
  ]
})
export class EventsModule {}
