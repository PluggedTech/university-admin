import { Component, OnInit } from '@angular/core';
import { EventService, PermittedEventTypeService } from '@api/generated';
import { Observable, BehaviorSubject, forkJoin } from 'rxjs';

@Component({
  selector: 'app-opportunities-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  private _events$ = new BehaviorSubject<any>('');
  get events$(): Observable<any> { return this._events$.asObservable(); }

  private _permittedEvents$ = new BehaviorSubject<any>(null);
  get permittedEvents$(): Observable<any> { return this._permittedEvents$.asObservable(); }

  // uncomment string in array to enable column
  count = [];
  columnsToDisplay: string[] = [
    // 'id',
    'title',
    'candidates',
    'schools',
    'primaryContact',
    'phoneNumber',
    'eventType',
    'eventDate',
    'eventTimeSlot',
    'campusEventPartner',
    'actions',
  ];

  constructor(
    private eventService: EventService,
    private permittedEvent: PermittedEventTypeService
  ) {
  }

  ngOnInit() {
    const recruiter = JSON.parse(localStorage.getItem('auth'))
    this.eventService.apiEventQueryPost({ recruiterId: recruiter.user.id })
      .subscribe(data => {
        this.getEventTypes(data.eventList.map(el => el.eventTimeslot.id))
        this.permittedEvents$.subscribe((elem) => {
          data.eventList.map(el => {
            if(elem && el.eventTimeslot.id === elem.permittedEventType.id) {
              return el['type'] = elem;
            }
        });
        this._events$.next(data.eventList);
      }, error => {
        console.log(error);
      });
    });
  }

  /**
   * Used with `NgMapPipeModule` to return the `name` property of any object.
   * @param object The object to be mapped
   * @returns The name property of the provide object
   * @see {@link https://github.com/fknop/angular-pipes/blob/master/docs/array.md#map NgMapPipeModule}
   * @example
   * ```
   * <div [innerHTML]="elm.universities | map:name"></div>
   * <div>{{ elm.schools | map:name  }}</div>
   * ```
   * */
  name(object: { name: string | any }) {
    if (!object.hasOwnProperty('name')) { throw new Error('property \'name\' is required'); }
    return object.name;
  }

  getTime(timeString: string) {
    const timeString12hr
    = new Date('1970-01-01T' + timeString + 'Z').toLocaleTimeString('EST', { timeZone: 'UTC', hour12: true, hour: 'numeric', minute: 'numeric' })
    return timeString12hr;
  }

  getEventTypes(id: number[]) {
    id.forEach(idNo => {
      this.permittedEvent.apiPermittedEventTypeIdGet(idNo)
      .subscribe(data => this._permittedEvents$.next(data));
    });
  }

  isInterested(match) {
    return match.isInterested === true;
  }

}
