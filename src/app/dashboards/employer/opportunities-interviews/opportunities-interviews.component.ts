import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InterviewScheduleService } from '@api/generated';

@Component({
  selector: 'app-opportunities-interviews',
  templateUrl: './opportunities-interviews.component.html',
  styleUrls: [ './opportunities-interviews.component.scss' ]
})
export class OpportunitiesInterviewsComponent implements OnInit {

  interviewSchedules: any;
  interviews: any; 
  pending: any = 0;
  upcoming: any = 0;
  completed: any = 0;

  constructor(private interviewSchedule: InterviewScheduleService, private route: Router) { }

  async ngOnInit() {
    const auth = JSON.parse(localStorage.getItem('auth'))
    const interviewSchedule = await this.interviewSchedule.apiInterviewScheduleQueryPost({ recruiterId : auth.id }).toPromise();

    this.pending = this.filterByStatus('pending', interviewSchedule.interviewScheduleList)
    this.upcoming = this.filterByStatus('upcoming', interviewSchedule.interviewScheduleList)
    this.completed = this.filterByStatus('completed', interviewSchedule.interviewScheduleList)
  }

  private filterByStatus(status, array){
    return array.filter(interviewSchedule => {
      return interviewSchedule.status === status 
    }).reduce((acc, interviewSchedule) => {
      return ++acc;
    }, 0)
  }

  routeToPage(status) {
    this.route.navigateByUrl('/e/interview/' + status);
  }

}
