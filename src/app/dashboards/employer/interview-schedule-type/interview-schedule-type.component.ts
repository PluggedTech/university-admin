import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InterviewScheduleService, InterviewSchedule } from '@api/generated';
import { map } from 'jquery';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-interview-schedule-type',
  templateUrl: './interview-schedule-type.component.html',
  styleUrls: ['./interview-schedule-type.component.scss']
})
export class InterviewScheduleTypeComponent implements OnInit {

  interviewScheduleForm : FormGroup;
  interviewSchedule : any
  status : string;
  id : unknown;

  constructor(private activeRoute : ActivatedRoute, private interviewScheduleService: InterviewScheduleService, private fb : FormBuilder, private router : Router) {
    this.interviewScheduleForm = this.fb.group({
      status: ['']
    })
   }

  ngOnInit(): void {
    this.id =this.activeRoute.snapshot.paramMap.get('id');
    this.status = this.activeRoute.snapshot.paramMap.get('status');
    this.interviewScheduleService.apiInterviewScheduleIdGet(<number>this.id)
    .subscribe(res => {
      this.interviewSchedule = res.interviewSchedule
    }, err => {
      console.log(err)
    })
  }

  onSubmit(){
    this.interviewSchedule.status = this.interviewScheduleForm.value.status;

    this.interviewScheduleService.apiInterviewScheduleIdPut(<number>this.id, this.interviewSchedule)
    .subscribe(res => {
      this.router.navigateByUrl('/e/interview/' + this.status)
    }, err => {
      console.log(err)
    })
  }

}
