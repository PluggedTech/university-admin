import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyComponent } from '@dashboards/employer/account-settings/company/company.component';
import { EmployerComponent } from '@dashboards/employer/account-settings/employer/employer.component';
import { SecurityComponent } from '@dashboards/employer/account-settings/security/security.component';

const routes: Routes = [
  { path: '', component: EmployerComponent },
  { path: 'company', component: CompanyComponent },
  { path: 'security-and-privacy', component: SecurityComponent },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AccountSettingsRoutingModule {}
