import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaterialsModule } from '@app/ngx-materials.module';
import { CompanyComponent } from '@dashboards/employer/account-settings/company/company.component';
import { EmployerComponent } from '@dashboards/employer/account-settings/employer/employer.component';
import { SecurityComponent } from '@dashboards/employer/account-settings/security/security.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxMaskModule } from 'ngx-mask';
import { AccountSettingsRoutingModule } from './account-settings-routing.module';

@NgModule({
  declarations: [
    EmployerComponent,
    CompanyComponent,
    SecurityComponent,
  ],
  imports: [
    CommonModule,
    AccountSettingsRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot({}),
    NgxMaterialsModule
  ]
})
export class AccountSettingsModule {}
