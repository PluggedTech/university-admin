import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: [ './security.component.scss' ]
})
export class SecurityComponent implements OnInit, OnDestroy {
  passwordRestForm: FormGroup;

  user : any;
  resetStatus : any;
  isVerified : any = false;
  passwordChanged : any = { success: false, message: ''}; 

  private unsubscribe$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private http : HttpClient,
    private route : ActivatedRoute
  ) {}

  async ngOnInit() {
    // Create Password Reset Form
    this.passwordRestForm = this.formBuilder.group({
        newPassword: [ '', [ Validators.required ] ],
        confirmPassword: [ '', [ Validators.required ] ]
      },
      {
        validator: this.matchPasswordValidator
      });

    this.checkIfVerified();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  submit() {
    if (this.passwordRestForm.invalid) {
      throw Error('the passwordRestForm is invalid');
    }

    const data = this.passwordRestForm.getRawValue();
    const { oldPassword, newPassword } = data;

    this.changePassword(newPassword);
    this.passwordRestForm.reset();
  }

  private matchPasswordValidator(control: AbstractControl) {
    const password = control.get('newPassword').value;
    const confirmPassword = control.get('confirmPassword').value;

    if (password !== confirmPassword) {
      control.get('confirmPassword').setErrors({ ConfirmPasswordUnmatch: true });
    } else {
      return null;
    }
  }

  async sendResetRequest(){
    const { user: { user : { email } } } = JSON.parse(localStorage.getItem('auth'))
    try {
      const requestResult = await this.http.post<any>(`${environment.apiUrl}/auth/reset`, { email : email }).toPromise();
      this.resetStatus = { success : true, message: requestResult.SUCCESS };
    } catch (error) {
      this.resetStatus = { success : false, message: error };
    }
  }

  private async checkIfVerified(){
    try {
      const secret = this.route.snapshot.queryParamMap.get('s');
      const result = await this.http.post<any>(`${environment.apiUrl}/auth/verify-reset`, { s : secret }).toPromise();
      this.user = result;
      this.isVerified = true;
    } catch (error) {
      console.log(error);  
      this.isVerified = false; 
    }
  }

  private async changePassword(password){
    try {
      const result = await this.http.post<any>(`${environment.apiUrl}/auth/change-password`, { userId: this.user.userId, secret: this.user.secret, password: password }).toPromise();
      this.passwordChanged = { success: true, message: 'Successfully Changed Password'};
    } catch (error) {
      console.log(error)      
      this.passwordChanged = { success: false, message: error};
    }
  }
}
