import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Recruiter } from '@api/generated';
import { AuthService } from '@services/auth.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

const DEFAULT_PROFILE_PICTURE = 'assets/images/user/user.png';
const DEFAULT_COVER_PICTURE = 'assets/images/user-card/1.jpg';

interface Banner {
  coverPicture?: string;
  profilePicture?: string;
}

interface Profile {
  firstName: string;
  lastName: string;
  profilePicture: string;
  coverPicture: string;
}

@Component({
  selector: 'app-employer',
  templateUrl: './employer.component.html',
  styleUrls: [ './employer.component.scss' ]
})
export class EmployerComponent implements OnInit, OnDestroy {
  imageForm: FormGroup;
  myProfileForm: FormGroup;
  banner$: BehaviorSubject<Banner>;
  profile$ : Observable<Profile>
  private unsubscribe$ = new Subject();
  private banner: Banner = {
    coverPicture: DEFAULT_COVER_PICTURE,
    profilePicture: DEFAULT_PROFILE_PICTURE
  };

  constructor(
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private sanitization: DomSanitizer,
    private authService: AuthService
  ) {
    this.banner$ = new BehaviorSubject<Banner>(this.banner);
  }

  private _imgProfile$ = new BehaviorSubject<any>('assets/images/user/user.png');

  get imgProfile$(): Observable<any> { return this._imgProfile$.asObservable(); }

  private _imgCover$ = new BehaviorSubject<any>('assets/images/user-card/1.jpg');

  get imgCover$(): Observable<any> { return this._imgCover$.asObservable(); }

  get fc() { return this.myProfileForm.controls; }

  private static formResetValidity(form: FormGroup) {
    form.markAsPristine();
    form.markAsUntouched();
    form.updateValueAndValidity();
  }

  ngOnInit() {
    this.profile$ = this.authService.state$
      .pipe(
        map(({ user }) => {
          if (!user) { return; }

          const { id, user: { firstName, lastName } } = user;

          let { profilePicture, coverPicture } = user;
          if ((profilePicture || '').trim() === '') {
            profilePicture = DEFAULT_PROFILE_PICTURE;
          }

          if ((coverPicture || '').trim() === '') {
            coverPicture = DEFAULT_COVER_PICTURE;
          }

          return <Profile>{
            id,
            firstName,
            lastName,
            profilePicture,
            coverPicture
          };
        })
      )
    

    // (1) create imageForm
    this.imageForm = this.formBuilder.group({
      imgCover: [ null ],
      imgProfile: [ null ],
    });

    // (2) create myProfileForm
    this.myProfileForm = this.formBuilder.group({
      id: '',
      firstName: '',
      lastName: '',
      email: [ '', [ Validators.email, Validators.nullValidator ] ],
      phone: '',
      title: '',
      bio: '',
      websiteUrl: '',
      linkedinUrl: '',
      twitterUrl: '',
      facebookUrl: '',
      user: {
        password : ''
      }
    });


    // (3) Initialize myProfileForm
    this.initMyProfileForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  async submit() {
    // (1) ensure that myProfileForm is valid
    if (this.myProfileForm.invalid) {
      throw Error('Invalid Form: (form MyProfileForm');
    }

    // (2) get raw values from myProfileForm
    const data = this.myProfileForm.getRawValue();
    const { user: { user: { id, type } } } = JSON.parse(localStorage.getItem('auth'));

    // (3) map raw values to profile (type Recruiter)
    const profile: Recruiter = {
      ...this.banner,
      title: data.title,
      biography: data.bio,
      user: {
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        phone: data.phone,
        id: id,
        type: type
      },
      socialMedia: {
        websiteUrl: data.websiteUrl,
        linkedinUrl: data.linkedinUrl,
        twitterUrl: data.twitterUrl,
        facebookUrl: data.facebookUrl
      }
    };

    // (4) dispatch update to api
    this.authService.updateUser(profile);

    // (5) reset myProfileForm validity
    EmployerComponent.formResetValidity(this.myProfileForm);
  }

  onFileChange({ target }) {
    const reader = new FileReader();

    if (target.files && target.files.length > 0) {
      if (target.name !== 'coverPicture' && target.name !== 'profilePicture') {
        throw new Error(`Invalid target.name (${target.name}), must be 'coverPicture' or 'profilePicture'`);
      }

      const key = target.name;
      const [ file ] = target.files;

      reader.readAsDataURL(file);
      reader.onload = () => {
        const data = {};
        data[ key ] = reader.result;
        this.setBanner(data);
        this.myProfileForm.markAsDirty();
      };
    }
  }

  private initMyProfileForm() {
    const profile = this.authService.user;
    if (!profile) {
      throw new Error('Unable to initialize.');
    }

    // set banner
    const banner: Banner = {
      coverPicture: profile.coverPicture || DEFAULT_COVER_PICTURE,
      profilePicture: profile.profilePicture || DEFAULT_PROFILE_PICTURE
    };
    this.setBanner(banner);

    // set everything else
    const { user, socialMedia } = profile;
    const values = {
      firstName: user.firstName,
      lastName: user.lastName,
      title: profile.title,
      email: user.email,
      phone: user.phone,
      bio: profile.biography,
      ...socialMedia
    };
    this.myProfileForm.patchValue(values);
  }

  private setBanner(banner: Banner) {
    this.banner = {
      ...this.banner,
      ...banner
    };

    this.banner$.next({ ...this.banner });
  }
}
