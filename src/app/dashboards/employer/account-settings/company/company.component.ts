import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Industry, Sector,  Recruiter } from '@api/generated';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { Company, ICompanyUpdate, User } from '@generated/graphql';
import { IndustryService } from '@services/industry.service';
import { SectorService } from '@services/sector.service';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { flatMap, map, skipWhile, tap } from 'rxjs/operators';
import { AuthService } from '@app/_services/auth.service';

// FontAwesome Icons
const icons = {
  faGlobe: faGlobe,
};

interface CompanyFormFields {
  name?: string;
  email?: string;
  phoneNumber?: string;
  field?: {
    industryId: string;
    sectorId: string;
  };
  description?: string;
  totalNumberOfEmployees?: number;
  startup?: {
    totalFunding?: number;
    lastFundingDate?: any;
    lastFundingRound?: any;
  };
  websiteUrl?: string;
  linkedinUrl?: string;
  twitterUrl?: string;
  facebookUrl?: string;
}

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: [ './company.component.scss' ]
})
export class CompanyComponent implements OnInit, OnDestroy {
  companyProfileForm: FormGroup;
  companyProfileFormSubmit = false;
  isStartup = false;

  industries: Observable<Industry[]>;
  sectors: Observable<Sector[]>;

  icons = icons;
  private company: Company;
  private unsubscribe$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private industryService: IndustryService,
    private sectorService: SectorService,
    private authService : AuthService
    // private userSessionService: UserSessionService,
  ) {
    this._sectors = <BehaviorSubject<Sector[]>>new BehaviorSubject([]);
  }

  _sectors: BehaviorSubject<Sector[]>;

  //get sectors() { return this._sectors.asObservable(); }

  get fc() { return this.companyProfileForm.controls; }

  ngOnInit() {
    // (1) Create companyProfileForm
    this.companyProfileForm = this.formBuilder.group({
      socialMediaId: '',
      name: '',
      email: [ '', [ Validators.email, Validators.nullValidator ] ],
      phone: '',
      industryId: '',
      sectorId: '',
      description: '',
      employeeCount: '',
      startup: this.formBuilder.group({
        totalFunding: '',
        lastFundingDate: '',
        lastFundingRound: '',
      }),
      socialMedia: this.formBuilder.group({
        websiteUrl: '',
        linkedinUrl: '',
        twitterUrl: '',
        facebookUrl: '',
      })
    });

    const { user : { company, socialMedia }} = JSON.parse(localStorage.getItem('auth'))

    this.companyProfileForm.patchValue({
      socialMediaId: socialMedia.id,
      name: company.name,
      email: company.email,
      phone: company.phone,
      description: company.description,
      employeeCount: company.employeeCount,
      socialMedia: {
        websiteUrl: socialMedia.websiteUrl,
        linkedinUrl: socialMedia.linkedinUrl,
        twitterUrl: socialMedia.twitterUrl,
        facebookUrl: socialMedia.facebookUrl,
      },
      industryId : company.industryId,
      sectorId : company.sectorId
    })

    this.companyProfileForm.get('industryId').valueChanges
      .pipe(
        //flatMap(industryId => this.loadSectors(industryId).pipe(map(sectors => ({ industryId, sectors })))),
      )
      .subscribe(({ industryId, sectors }) => {
        // ensure that user selects a new 'sector' when they select a new 'industry'
        const sectorId = (this.fc.field as FormGroup).controls.sectorId;
        if (sectors.length > 0) {
          sectorId.setValidators([ Validators.required, Validators.nullValidator ]);
          sectorId.enable();
        } else {
          sectorId.clearValidators();
          sectorId.disable();
        }

        // if selected industry matches the current industry in the users company profile
        // assume the user is attempting to revert his/her changes
        const { industry, sector } = this.company.field;
        if (industry.id === industryId) {
          sectorId.setValue(sector.id);

          // mark as pristine to prevent the data from being submitted
          this.fc.field.markAsPristine();
        } else {
          sectorId.setValue(null);
        }

        sectorId.updateValueAndValidity();
      });

    // (2) load user
    // this.loadCompany();

    // (3) load industries
    this.loadIndustries();

    this.loadSectors();

    // (4) initialize companyProfileForm
    this.initCompanyProfileForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  submit() {
    this.companyProfileFormSubmit = true;

    // (1) ensure that the companyProfileForm is valid...
    if (this.companyProfileForm.invalid) {
      throw Error('Invalid Form: (form CompanyProfileForm)');
    }

    const { user : {id,  company, socialMedia }} = JSON.parse(localStorage.getItem('auth'))

    const profile: Recruiter = {
      socialMedia : this.companyProfileForm.value.socialMedia,
      company: this.companyProfileForm.value
    };

    console.log(profile)

    // (4) dispatch update to api
    this.authService.updateUser(profile);

    this.resetFormValidity(this.companyProfileForm);
  }

  //private loadSectors(industryId: string): Observable<Sector[]> {
  //  return this.industries.pipe(
  //    skipWhile(data => data.length === 0),
  //    // map(data => data.find(value => value.id === industryId).sectors),
  //    tap(sectors => this._sectors.next(sectors)),
  //  );
  //}

  private formatDate(date: any): string {
    return date.format('YYYY-MM-DD');
  }

  private loadIndustries() {
    this.industries = this.industryService.industries
      .pipe(tap(x => console.log('industries', x)));
    this.industryService.loadAll();
  }

  private loadSectors() {
    this.sectors = this.sectorService.sectors
      .pipe(tap(x => console.log('sectors', x)));
    this.sectorService.loadAll();
  }
  
  // private loadCompany(): void {
  //   this.userSessionService.user
  //     .pipe(skipWhile(user => !user || !user.company))
  //     .subscribe(user => this.company = user.company);
  // }

  private initCompanyProfileForm() {
    const fc = this.fc;
    const opt = { onlySelf: true, emitEvent: false };

    const init = (user: User) => {
      const { company } = user;
      const { field, startup } = company;
      const { industry, sector } = field;

      // (1) map form controls to company (data)
      const fcMap = new Map();

      fcMap.set('name', company.name);
      fcMap.set('email', company.email);
      fcMap.set('phoneNumber', company.phoneNumber);
      fcMap.set('description', company.description);
      fcMap.set('totalNumberOfEmployees', company.totalNumberOfEmployees);
      fcMap.set('websiteUrl', company.websiteUrl);
      fcMap.set('linkedinUrl', company.linkedinUrl);
      fcMap.set('twitterUrl', company.twitterUrl);
      fcMap.set('facebookUrl', company.facebookUrl);

      // (2) initialize from map
      for (const [ key, value ] of fcMap.entries()) {
        fc[ key ].setValue(value, opt);
      }

      // (3) initialize field
      const _field = (this.fc.field as FormGroup).controls;
      _field.industryId.setValue(industry.id, opt);
      _field.sectorId.setValue(sector.id, opt);

      // (4) initialize startup
      if (startup) {
        this.isStartup = true;

        const _startup = (this.fc.startup as FormGroup).controls;
        _startup.totalFunding.setValue(startup.totalFunding, opt);
        _startup.lastFundingDate.setValue(startup.lastFundingDate, opt);
        _startup.lastFundingRound.setValue(startup.lastFundingRound, opt);
      }

      return of(industry.id);
    };

    // this.userSessionService.user
    //   .pipe(
    //     skipWhile(user => !user || !user.company),
    //     flatMap(user => init(user)),
    //     flatMap(industryId => this.loadSectors(industryId))
    //   )
    //   .subscribe(() => {
    //     // reset companyProfileForm validity
    //     this.resetFormValidity(this.companyProfileForm);
    //   });
  }

  private resetFormValidity(form: FormGroup): void {
    form.markAsPristine();
    form.markAsUntouched();
    form.updateValueAndValidity();
  }
}
