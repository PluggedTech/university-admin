import { NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { PanelsModule } from '@app/_components/panels/panels.module';
import { AccountSettingsComponent } from '@app/dashboards/employer/account-settings/account-settings.component';
import { AccountSubscriptionComponent } from '@app/dashboards/employer/account-subscription/account-subscription.component';
import { NgxMaterialsModule } from '@app/ngx-materials.module';
import { ContentModule } from '@content/content.module';
import { EmployerRoutingModule } from '@dashboards/employer/employer-routing.module';
import { EmployerComponent } from '@dashboards/employer/employer.component';
import { InterviewScheduleTypeComponent } from '@dashboards/employer/interview-schedule-type/interview-schedule-type.component';
import { InterviewComponent } from '@dashboards/employer/interview/interview.component';
import { OpportunitiesInterviewsComponent } from '@dashboards/employer/opportunities-interviews/opportunities-interviews.component';
import { ChatComponent } from '@dashboards/employer/opportunities/chat';
import { InterviewScheduleComponent } from '@dashboards/employer/opportunities/chat/interview-schedule/interview-schedule.component';
import { StudentProfileComponent } from '@dashboards/employer/opportunities/student-profile';
import { DirectivesModule } from '@directives/directives.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PipesModule } from '@pipes/pipes.module';
import { ServiceModule } from '@services/service.module';
import { MomentModule } from 'ngx-moment';
import { CandidatesComponent } from './opportunities/candidates/candidates.component';
import { EventCandidatesComponent } from './opportunities/event-candidates/event-candidates.component';

@NgModule({
  declarations: [
    // Components
    AccountSettingsComponent,
    AccountSubscriptionComponent,
    EmployerComponent,
    OpportunitiesInterviewsComponent,
    StudentProfileComponent,
    ChatComponent,
    InterviewScheduleComponent,
    InterviewComponent,
    InterviewScheduleTypeComponent,
    CandidatesComponent,
    EventCandidatesComponent
  ],
  imports: [
    CommonModule,
    ContentModule,
    DirectivesModule,
    EmployerRoutingModule,
    FontAwesomeModule,
    MomentModule,
    NgxMaterialsModule,
    PanelsModule,
    PipesModule,
    ReactiveFormsModule,
    ServiceModule,
    FormsModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    MatIconModule
  ]
})
export class EmployerModule {}
