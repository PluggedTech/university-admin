import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactBookComponent } from '@components/contact/contact-book/contact-book.component';
import { AccountSettingsComponent } from '@dashboards/employer/account-settings/account-settings.component';
import { AccountSubscriptionComponent } from '@dashboards/employer/account-subscription/account-subscription.component';
import { EmployerComponent } from '@dashboards/employer/employer.component';
import { InterviewScheduleTypeComponent } from '@dashboards/employer/interview-schedule-type/interview-schedule-type.component';
import { InterviewComponent } from '@dashboards/employer/interview/interview.component';
import { OpportunitiesInterviewsComponent } from '@dashboards/employer/opportunities-interviews/opportunities-interviews.component';
import { CandidatesComponent } from '@dashboards/employer/opportunities/candidates/candidates.component';
import { ChatComponent } from '@dashboards/employer/opportunities/chat';
import { StudentProfileComponent } from '@dashboards/employer/opportunities/student-profile';
import { EventCandidatesComponent } from './opportunities/event-candidates/event-candidates.component';

const routes: Routes = [
  { path: 'dashboard', component: EmployerComponent },
  { path: '', loadChildren: () => import('./opportunities/jobs/jobs.module').then(m => m.JobsModule) },
  { path: '', loadChildren: () => import('./opportunities/events/events.module').then(m => m.EventsModule) },
  { path: 'interviews', component: OpportunitiesInterviewsComponent },
  { path: 'account/subscription', component: AccountSubscriptionComponent },
  { path: 'student-profile/:id', component: StudentProfileComponent },
  { path: 'job/:jobid/student-profile/:studentid', component: StudentProfileComponent },
  { path: 'job/:jobid/student/:studentid/chat', component: ChatComponent },
  { path: 'events/:eventid/student-profile/:studentid', component: StudentProfileComponent },
  { path: 'events/:eventid/student/:studentid/chat', component: ChatComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'interview/:status', component: InterviewComponent },
  { path: 'interview-type/:status/:id', component: InterviewScheduleTypeComponent },
  { path: 'job/:id/applicants', component: CandidatesComponent },
  { path: 'events/:id/applicants', component: EventCandidatesComponent },
  {
    path: 'account/settings',
    component: AccountSettingsComponent,
    loadChildren: () => import('./account-settings/account-settings.module').then(m => m.AccountSettingsModule)
  },
  { path: 'contacts', component: ContactBookComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class EmployerRoutingModule {}
