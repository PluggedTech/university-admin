import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InterviewScheduleService, InterviewSchedule } from '@api/generated';
import { Subject, BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss']
})
export class InterviewComponent implements OnInit {

  unsubscribe$ = new Subject<void>();

  dataStore: {
    interviewSchedules: InterviewSchedule[]
  } = {
    interviewSchedules: []
  };

  columnsToDisplay: string[] = [
    'job',
    'date1',
    'date2',
    'student',
    'actions',

  ];

  status : string = '';
  list : any = [];

  private _interviewSchedules = <BehaviorSubject<InterviewSchedule[]>>new BehaviorSubject([]);
  get interviewSchedules() { return this._interviewSchedules.asObservable(); }

  constructor(private activeRoute : ActivatedRoute, private interviewScheduleService : InterviewScheduleService) { }

  ngOnInit(): void {
    this.status = this.activeRoute.snapshot.paramMap.get('status');
    this.interviewScheduleService.apiInterviewScheduleGet()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(res => {
      console.log(res)
      this.dataStore.interviewSchedules  = res.interviewScheduleList;
      this.interviewByStatus(this.status);
      console.log(this.dataStore)
      this._interviewSchedules.next({ ...this.dataStore}.interviewSchedules)
    }, err => {
      console.log(err)
    });

  }

  interviewByStatus(status){
    switch(status) {
      case "upcoming" : 
        this.dataStore.interviewSchedules = this.dataStore.interviewSchedules.filter(interviewSchedule => {
          return interviewSchedule.status == "upcoming"
        })
        break;
      case "completed" : 
        this.dataStore.interviewSchedules = this.dataStore.interviewSchedules.filter(interviewSchedule => {
          return interviewSchedule.status == "completed"
        })
        break;
      default : 
        this.dataStore.interviewSchedules = this.dataStore.interviewSchedules.filter(interviewSchedule => {
          return interviewSchedule.status == "pending"
        })
        break;
    }
  }

}
