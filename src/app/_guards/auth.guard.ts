import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '@services/auth.service';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService,
    private dialog: MatDialog,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    // if environment is non-production, turn AuthGuard off (canActivate = true)
    // if (!environment.production) {
    //   console.log('[AuthGuard] Deactivated - The current environment is not production!');
    //   this.closeDialog();
    //   return true;
    // }

    if (this.authService.isAuthenticated) {
      this.closeDialog();
      return true;
    }

    this.router.navigate([ 'login' ]).then(() => false);
  }

  closeDialog() {
    const id = localStorage.getItem('authDialogID');
    if (id) {
      const dialogRef = this.dialog.getDialogById(id);
      if (dialogRef) {
        dialogRef.close();
      }

      localStorage.removeItem('authDialogID');
    }
  }
}
