import { Component, OnInit } from '@angular/core';
import { faFacebookF, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons';
import { faCircle, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { Platform, UtilityService } from '@services/utility.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: [ './footer.component.scss' ]
})
export class FooterComponent implements OnInit {
  faCircle = faCircle;
  faYoutube = faYoutube;
  faTwitter = faTwitter;
  faFacebookF = faFacebookF;
  faQuestionCircle = faQuestionCircle;

  logo: any = '/assets/images/company/plugged-logo-a.png';

  constructor(
    private utilityService: UtilityService
  ) {}

  ngOnInit() {}

  navigateToExternalPlatform(platform: Platform) {
    window.open(this.utilityService.getExternalPlatformUrl(platform), '_blank');
  }

}
