import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from '@components/components.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { InlineSVGModule } from 'ng-inline-svg';
import { EmployerPartnersComponent } from './employer-partners/employer-partners.component';
import { EmployerComponent } from './employer/employer.component';
import { FooterComponent } from './footer/footer.component';
import { HeroComponent } from './hero/hero.component';
import { StudentsComponent } from './students/students.component';
import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';

@NgModule({
  declarations: [
    LandingComponent,
    HeroComponent,
    EmployerComponent,
    EmployerPartnersComponent,
    FooterComponent,
    StudentsComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FontAwesomeModule,
    InlineSVGModule.forRoot(),
    LandingRoutingModule,
    NgbCollapseModule,
  ]
})
export class LandingModule {}
