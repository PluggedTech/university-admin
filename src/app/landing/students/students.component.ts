import { Component, OnInit } from '@angular/core';
import { Platform, UtilityService } from '@services/utility.service';

interface Image {
  src: any;
  alt: string;
}

interface ListItem {
  icon: Image;
  header: string;
  body: string;
}

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: [ './students.component.scss' ]
})
export class StudentsComponent implements OnInit {

  graduateIcon: any = '/assets/images/welcome/sections/students/icons/graduate-icon.png';

  listItems: ListItem[] = [
    {
      icon: { src: '/assets/images/welcome/sections/students/icons/briefcase-icon.png', alt: 'Briefcase Icon Icon' },
      header: 'Download The App',
      body: 'Download the PLUGGED app and create a profile.'
    },
    {
      icon: { src: '/assets/images/welcome/sections/students/icons/calendar-icon.png', alt: 'Calendar Icon' },
      header: 'Explore Opportunities',
      body: 'Sift through opportunities (jobs and events)  from various companies by swiping right to apply and left to skip.'
    },
    {
      icon: { src: '/assets/images/welcome/sections/students/icons/lock-icon.png', alt: 'Lock Icon' },
      header: 'Connect With Hiring Managers',
      // tslint:disable-next-line:max-line-length
      body: 'Communicate directly with recruiting/event managers via the chat and confirm campus hire interviews to land the perfect opportunity!'
    },
    {
      icon: { src: '/assets/images/welcome/sections/students/icons/phone-icon.png', alt: 'Phone Icon' },
      header: 'Update & Manage Profile',
      // tslint:disable-next-line:max-line-length
      body: 'Keep your profile current with new jobs, extracurriculars, etc. and adjust profile settings (i.e. push notifications) to maximize your experience!'
    },
  ];

  constructor(
    private utilityService: UtilityService
  ) {}

  ngOnInit() {}

  navigateToDeviceAppStore(platform: Platform) {
    window.open(this.utilityService.getExternalPlatformUrl(platform), '_blank');
  }

}
