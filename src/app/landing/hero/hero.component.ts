import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { LoginComponent } from '@auth/login/login.component';
import { EmployerSignupComponent } from '@auth/signup/employer/employer.signup.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Feature {
  icon: any;
  header: string;
  company: string;
  student: string;
}

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: [ './hero.component.scss' ]
})
export class HeroComponent implements OnInit, OnDestroy {

  unsubscribe$ = new Subject<void>();

  logo: any = '/assets/images/company/plugged-logo-a.png';
  machineLearningImage: any = '/assets/images/welcome/sections/hero/plugged-machine-learning.png';
  navbarMenuIsCollapsed = true;

  features: Feature[][] = [
    [
      {
        icon: '/assets/images/welcome/icons/feature/pipeline-icon.png',
        header: 'Pipeline',
        company: 'Easily access Howard University School of Business top talent using HUSB PLUGGED.',
        student: 'Stay aware of what companies are doing on-campus without having to.'
      },
      {
        icon: '/assets/images/welcome/icons/feature/simple-user-interface-icon.png',
        header: 'Simple User Interface',
        company: 'Easy to use web app for the “not-so-tech-savvy” recruiters to post opportunities.',
        student: 'Modernized mobile app functionality for you to engage a large opportunity pool.'
      }
    ],
    [
      {
        icon: '/assets/images/welcome/icons/feature/employer-branding-icon.png',
        header: 'Employer Branding',
        company: 'Position your company brand so students are aware of your company and what you offer.',
        student: 'Learn about companies you may not have known about and the opportunities they offer.'
      },
      {
        icon: '/assets/images/welcome/icons/feature/advanced-filtration-system-icon.png',
        header: 'Advanced Filtration System',
        company: 'Market opportunities directly to students that meet the baseline requirements.',
        student: 'Prioritized opportunities based on the alignment with your background/qualifications.'
      }
    ]
  ];

  constructor(
    private dialog: MatDialog,
  ) {}

  ngOnInit() {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  openDialog(event: MouseEvent, dialog: 'SIGNIN' | 'SIGNUP'): void {
    const config: MatDialogConfig = {
      panelClass: 'mat-dialog-auth-panel',
      backdropClass: 'mat-dialog-auth-backdrop',
    };

    let dialogRef: MatDialogRef<any>;
    if (dialog === 'SIGNIN') {
      dialogRef = this.dialog.open(LoginComponent, { ...config, id: 'auth-dialog-signin' });
    } else if (dialog === 'SIGNUP') {
      dialogRef = this.dialog.open(EmployerSignupComponent, { ...config, id: 'auth-dialog-signup' });
    }

    if (dialogRef) {
      // store `dialogRef.id` so that we may close the dialog on successful `SIGNIN` or `SIGNUP`
      // Note: We close the dialog globally via ** src/app/_guards/auth.guard.ts **
      localStorage.setItem('authDialogID', dialogRef.id);
      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(() => localStorage.removeItem('authDialogID'));
    }
  }
}
