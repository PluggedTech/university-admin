import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { LoginComponent } from '@app/_auth/login/login.component';
import { EmployerSignupComponent } from '@app/_auth/signup/employer/employer.signup.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-employer-partners',
  templateUrl: './employer-partners.component.html',
  styleUrls: [ './employer-partners.component.scss' ]
})
export class EmployerPartnersComponent implements OnInit {
  unsubscribe$ = new Subject<void>();

  brands: string[] = [];

  constructor(private dialog : MatDialog) {
    const assetPath = '/assets/images/welcome/sections/employer-partners';

    for (let i = 1; i <= 11; i++) {
      const asset = (i < 10) ? `brand-0${i}.png` : `brand-${i}.png`;

      this.brands.push(`${assetPath}/${asset}`);
    }
  }

  ngOnInit() {
  }

  openDialog(dialog: 'SIGNIN' | 'SIGNUP'): void {
    const config: MatDialogConfig = {
      panelClass: 'mat-dialog-auth-panel',
      backdropClass: 'mat-dialog-auth-backdrop',
    };

    let dialogRef: MatDialogRef<any>;
    if (dialog === 'SIGNIN') {
      dialogRef = this.dialog.open(LoginComponent, { ...config, id: 'auth-dialog-signin' });
    } else if (dialog === 'SIGNUP') {
      dialogRef = this.dialog.open(EmployerSignupComponent, { ...config, id: 'auth-dialog-signup' });
    }

    if (dialogRef) {
      // store `dialogRef.id` so that we may close the dialog on successful `SIGNIN` or `SIGNUP`
      // Note: We close the dialog globally via ** src/app/_guards/auth.guard.ts **
      localStorage.setItem('authDialogID', dialogRef.id);
      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(() => localStorage.removeItem('authDialogID'));
    }
  }

}
