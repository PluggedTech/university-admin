import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { LoginComponent } from '@app/_auth/login/login.component';
import { EmployerSignupComponent } from '@app/_auth/signup/employer/employer.signup.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Image {
  src: any;
  alt: string;
}

interface ListItem {
  icon: Image;
  header: string;
  body: string;
}

@Component({
  selector: 'app-employer',
  templateUrl: './employer.component.html',
  styleUrls: [ './employer.component.scss' ]
})
export class EmployerComponent implements OnInit {
  unsubscribe$ = new Subject<void>();

  corporateOfficeIcon = {
    src: '/assets/images/welcome/sections/employer/icons/corporate-office-icon.png',
    srcset: '/assets/images/welcome/sections/employer/icons/corporate-office-icon@2x.png, ' +
      '/assets/images/welcome/sections/employer/icons/corporate-office-icon@3x.png'
  };

  listItems: ListItem[] = [
    {
      icon: { src: '/assets/images/welcome/sections/employer/icons/shield-icon.png', alt: 'Shield Icon' },
      header: 'Post Unlimited Jobs & Events',
      body: 'Complete job and/or event requisition form. Reserve on-campus rooms for events.'
    },
    {
      icon: { src: '/assets/images/welcome/sections/employer/icons/bar-graph-icon.png', alt: 'Bar Graph Icon' },
      header: 'Review Applicants',
      body: 'Review filtered list of students that meet the requirements.'
    },
    {
      icon: { src: '/assets/images/welcome/sections/employer/icons/customer-service-icon.png', alt: 'Customer Service Icon' },
      header: 'Contact Target Students',
      body: 'Initiate chat and interview scheduling using our in-app messaging tool.'
    },
    {
      icon: { src: '/assets/images/welcome/sections/employer/icons/interview-icon.png', alt: 'Interview Icon' },
      header: 'Hire & Debrief',
      body: 'Hire students that fits your opportunities and review student engagement metrics.'
    },
  ];

  constructor(
    private dialog : MatDialog
  ) { }

  ngOnInit() {
  }

  openDialog(dialog: 'SIGNIN' | 'SIGNUP'): void {
    const config: MatDialogConfig = {
      panelClass: 'mat-dialog-auth-panel',
      backdropClass: 'mat-dialog-auth-backdrop',
    };

    let dialogRef: MatDialogRef<any>;
    if (dialog === 'SIGNIN') {
      dialogRef = this.dialog.open(LoginComponent, { ...config, id: 'auth-dialog-signin' });
    } else if (dialog === 'SIGNUP') {
      dialogRef = this.dialog.open(EmployerSignupComponent, { ...config, id: 'auth-dialog-signup' });
    }

    if (dialogRef) {
      // store `dialogRef.id` so that we may close the dialog on successful `SIGNIN` or `SIGNUP`
      // Note: We close the dialog globally via ** src/app/_guards/auth.guard.ts **
      localStorage.setItem('authDialogID', dialogRef.id);
      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(() => localStorage.removeItem('authDialogID'));
    }
  }

}
