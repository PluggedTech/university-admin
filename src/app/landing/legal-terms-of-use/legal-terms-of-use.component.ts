import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-legal-terms-of-use',
  templateUrl: './legal-terms-of-use.component.html',
  styleUrls: [ './legal-terms-of-use.component.scss' ]
})
export class LegalTermsOfUseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $.getScript('./assets/js/legal-terms-list.js');
  }

}
