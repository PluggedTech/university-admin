import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: [ './activate.component.scss' ]
})
export class ActivateComponent implements OnInit {

  isDone = false;

  message = 'Please wait while we activate your account';

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  async ngOnInit() {
    const secret = this.route.snapshot.queryParamMap.get('s');

    try {
      const activate = await this.http.get<any>(`${environment.apiUrl}/auth/account-activation/${secret}`).toPromise();
      this.message = activate.message;
      this.isDone = true;
    } catch (error) {
      this.message = error;
      this.isDone = false;
    }

    setTimeout(() => {
      window.location.replace('/');
    }, 8000);
  }

}
