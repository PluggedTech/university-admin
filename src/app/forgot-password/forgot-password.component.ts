import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from '@environments/environment';
import { MatDialogRef, MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  resetStatus = { success : false, message: '' };
  private unsubscribe$ = new Subject<void>();

  resetForm = this.formBuilder.group({
    email: [ '', [ Validators.required ] ],
  });

  @ViewChild('submitBtn') submitBtn;

  constructor(
    private formBuilder: FormBuilder,
    private http : HttpClient,
    private dialog : MatDialog,
    private dialogRef : MatDialogRef<ForgotPasswordComponent>
  ) { }

  get fc() { return this.resetForm.controls; }

  ngOnInit() {
  }

  async submit(){
    try {
      this.resetForm.disable();
      this.submitBtn.nativeElement.disabled = true;
      const requestResult = await this.http.post<any>(`${environment.apiUrl}/auth/reset`, { email : this.resetForm.value.email }).toPromise();
      this.resetStatus = { success : true, message: requestResult.SUCCESS };
    } catch (error) {
      this.resetStatus = { success : false, message: error };
    }
    this.resetForm.enable();
    this.submitBtn.nativeElement.disabled = false;
    this.resetForm.reset();
  }

}
