import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EmploymentTypeToStringPipe } from '@pipes/employment-type-to-string.pipe';
import { PhonePipe } from '@pipes/phone.pipe';
import { RemoveEmptyStringPipe } from '@pipes/remove-empty-string.pipe';

@NgModule({
  declarations: [
    EmploymentTypeToStringPipe,
    RemoveEmptyStringPipe,
    PhonePipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    EmploymentTypeToStringPipe,
    RemoveEmptyStringPipe,
    PhonePipe,
  ]
})
export class PipesModule {}
