import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'employmentTypeToString'
})
export class EmploymentTypeToStringPipe implements PipeTransform {
  transform(enumValue: string): string {
    if (enumValue.search('_') > 0) {
      return enumValue.split('_')
        .map(value => this.capitalize(value))
        .join('-');
    } else {
      return this.capitalize(enumValue);
    }
  }

  private capitalize(value: string): string {
    return value.charAt(0).toUpperCase() + value.substring(1, value.length).toLowerCase();
  }
}
