import { Pipe, PipeTransform } from '@angular/core';
import { CountryCode, parsePhoneNumber } from 'libphonenumber-js/min';

@Pipe({
  name: 'phone'
})
export class PhonePipe implements PipeTransform {

  transform(phoneNumber: number | string, countryCode: CountryCode): any {
    try {
      const result = parsePhoneNumber('' + phoneNumber, countryCode);
      return result.formatNational();
    } catch (e) {
      return phoneNumber;
    }
  }

}
