import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeEmptyString'
})
export class RemoveEmptyStringPipe implements PipeTransform {
  transform(array: Array<any>): Array<any> {
    return array.filter((str) => (str.trim() !== ('')));
  }
}
