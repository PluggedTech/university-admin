import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-event-approval',
  templateUrl: './event-approval.component.html',
  styleUrls: ['./event-approval.component.scss']
})
export class EventApprovalComponent implements OnInit {

  message : any = "Processing request, please wait."
  isDone : boolean = false;

  constructor(private route : ActivatedRoute, private http : HttpClient, private router : Router) { }

  async ngOnInit() {
   const id = this.route.snapshot.paramMap.get('id') 
   const type = this.route.snapshot.paramMap.get('type') 
   const date = this.route.snapshot.queryParamMap.get('date')

   try {
    const result = await this.http.get<any>(`${environment.apiUrl}/event/${id}/${type}?date=${date}`).toPromise();
    this.message = result.message;
    this.isDone = true;
   } catch (error) {
    this.message = error;
    this.isDone = true;
   }

   setTimeout(() => {
    window.location.replace('/')
   }, 8000)
  }

}
