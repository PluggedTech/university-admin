"use strict";
$.legalTermsList = function (list) {
  console.log('legalTermsList');
  var termsWrapper = $(list).find('.row > div:first-child h6');
  var terms = $(termsWrapper).children();
  var contentTable = {
    table: $('#legal-terms-content-table'),
    offsetY: $('.page-main-header').outerHeight() + 16,
  };

  $.each(termsWrapper, (index, elm) => {
    $(elm).attr('id', `legal-term-${index + 1}`);
  });

  $.each(terms, (index, elm) => {
    // add term to content table
    var contentTableItem = $(`<h6>${index + 1}. <a href="javascript:void(0)">${elm.innerHTML}</a></h6>`);

    contentTableItem.on('click', () => {
      $([document.documentElement, document.body]).animate({
        scrollTop: $(elm).offset().top - contentTable.offsetY
      }, 2000)
    });

    contentTable.table.append(contentTableItem);

    // number terms
    elm.innerHTML = `${index + 1}. ${elm.innerHTML}`;
  });

};
$.legalTermsList($('#legal-terms-list'));
