import * as StudentsBackground from './students-bg.png';
import * as StudentsBackgroundCurve from './students-bg-curve.png';

export {
  StudentsBackground,
  StudentsBackgroundCurve,
};
