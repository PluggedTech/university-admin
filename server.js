/* server.js */

'use strict';

if (process.env.ENABLE_NEW_RELIC) {
  require('newrelic');
}

const
  express = require('express'),
  http = require('http'),
  cors = require('cors'),
  helmet = require('helmet'),
  compression = require('compression');

// ============================================================================
// GENERAL MIDDLEWARE
// ============================================================================

let app = express();
app.set('port', process.env.PORT || 2021);
app.set('env', process.env.NODE_ENV);
app.set('startTime', Date.now());

// CORS options
let corsOptions = {};
app.use(cors(corsOptions));

app.use(helmet());

const shouldCompress = function (req, res) {
  if (req.headers['x-no-compression']) {
    return false
  }

  return true;
};

app.use(compression({level: 7, memLevel: 9, filter: shouldCompress}));


// ============================================================================
// PAGE ROUTES
// ============================================================================

app.use(express.static('dist'));

app.use('*',
  (req, res, next) => {
    if (process.env.NODE_LOCAL === true) {
      return next();
    }

     
    console.log('env', app.get('env'));
    console.log('X-Forwarded-Proto', req.get('X-Forwarded-Proto'));
    
    //FORCING SSL
    if ((app.get('env') === 'prod' || app.get('env') === 'dev' || app.get('env') === 'develop') && (req.get('X-Forwarded-Proto') !== 'https')) {
      console.log('redirecting to https');
      res.redirect('https://' + req.get('Host') + req.url);
    } else {
      console.log('not redirecting to https');
      next();
    }
  },
  (req, res) => {
  res.sendFile('./dist/index.html', { root: __dirname });
});

// ============================================================================
// Handle Uncaught Exception
// ============================================================================

function handleUncaughtException(error) {
  logger.error('UNCAUGHT EXCEPTION', error);
}

process.on('uncaughtException', handleUncaughtException);

// ============================================================================
// *** START SERVER ***
// ============================================================================
http.globalAgent.keepAlive = true;

const server = http.createServer(app).listen(app.get('port'), () => {
  console.log('NODE_ENV: ' + app.get('env'));
  console.log('Node App listening on port: ' + app.get('port'));
});

server.keepAliveTimeout = 61 * 1000;
server.headersTimeout = 65 * 1000;

module.exports = app;
